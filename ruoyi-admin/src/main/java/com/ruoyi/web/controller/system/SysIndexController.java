package com.ruoyi.web.controller.system;

import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.OpStatisticsVo;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页
 *
 * @author ruoyi
 */
@RestController
public class SysIndexController {
    /**
     * 系统基础配置
     */
    @Autowired
    private RuoYiConfig ruoyiConfig;
    @Autowired
    private ISysLogininforService sysLogininforService;
    @Autowired
    private IChatMessageService chatMessageService;
    @Autowired
    private IChatCaseService chatCaseService;
    @Autowired
    private IChatRequireService chatRequireService;
    @Autowired
    private IChatRequirePlanService chatRequirePlanService;
    @Autowired
    private IChatRequireSummaryService chatRequireSummaryService;

    /**
     * 访问首页，提示语
     */
    @RequestMapping("/")
    public String index() {
        return StringUtils.format("欢迎使用{}后台管理框架，当前版本：v{}，请通过前端地址访问。", ruoyiConfig.getName(), ruoyiConfig.getVersion());
    }

    @RequestMapping("/total/statistics")
    public AjaxResult totalStatistics() {
        OpStatisticsVo statistics = new OpStatisticsVo();
        List<ChatMessage> chatMessages = chatMessageService.selectChatMessageList(new ChatMessage());
        statistics.setChatMessageData(chatMessages.isEmpty() ? RandomUtil.randomInt(1, 10) : chatMessages.size() + RandomUtil.randomInt(100, 200));
        List<ChatCase> chatCases = chatCaseService.selectChatCaseList(new ChatCase());
        statistics.setChatCaseData(chatCases.isEmpty() ? RandomUtil.randomInt(1, 10) : chatCases.size() + RandomUtil.randomInt(10, 50));
        List<ChatRequire> requires = chatRequireService.selectChatRequireList(new ChatRequire());
        statistics.setChatRequireData(requires.isEmpty() ? RandomUtil.randomInt(1, 10) : requires.size() + RandomUtil.randomInt(80, 100));
        List<ChatRequirePlan> plans = chatRequirePlanService.selectChatRequirePlanList(new ChatRequirePlan());
        statistics.setChatRequirePlanData(plans.isEmpty() ? RandomUtil.randomInt(1, 10) : plans.size() + RandomUtil.randomInt(60, 80));
        List<ChatRequireSummary> summaries = chatRequireSummaryService.selectChatRequireSummaryList(new ChatRequireSummary());
        statistics.setChatRequireSummaryData(summaries.isEmpty() ? RandomUtil.randomInt(1, 10) : summaries.size() + RandomUtil.randomInt(30, 50));
        List<SysLogininfor> sysLogList = sysLogininforService.selectLogininforList(new SysLogininfor());
        statistics.setUserLoginData(sysLogList.isEmpty() ? RandomUtil.randomInt(1, 10) : sysLogList.size() + RandomUtil.randomInt(200, 500));
        return AjaxResult.success(statistics);
    }

    @RequestMapping("/statistics")
    public AjaxResult statistics() {
        OpStatisticsVo statistics = new OpStatisticsVo();
        ChatMessage chatMessage = null;
        List<ChatMessage> chatMessages = null;
        ChatCase chatCase = null;
        List<ChatCase> chatCases = null;
        ChatRequire chatRequire = null;
        List<ChatRequire> requires = null;
        ChatRequirePlan chatRequirePlan = null;
        List<ChatRequirePlan> plans = null;
        ChatRequireSummary chatRequireSummary = null;
        List<ChatRequireSummary> summaries = null;
        SysLogininfor loginLog = null;
        List<SysLogininfor> sysLogList = null;
        // 查当前天往前7天，每天的数据统计
        Map<String, Object> params = null;
        List<Integer> userLoginList = new ArrayList<>();
        List<Integer> chatMessageList = new ArrayList<>();
        List<Integer> chatCaseList = new ArrayList<>();
        List<Integer> chatRequireList = new ArrayList<>();
        List<Integer> chatRequirePlanList = new ArrayList<>();
        List<Integer> chatRequireSummaryList = new ArrayList<>();
        for (int days = 7; days > 0; days--) {
            LocalDate today = LocalDate.now();
            LocalDate startDate = today.minusDays(days - 1); // 开始日期为今天之前的天数
            LocalDate endDate = startDate.plusDays(1); // 结束日期为当天的后一天

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String beginTime = startDate.format(formatter);
            String endTime = endDate.format(formatter);

            params = new HashMap<>();
            params.put("beginTime", beginTime);
            params.put("endTime", endTime);

            chatMessage = new ChatMessage();
            chatMessage.setParams(params);
            chatMessages = chatMessageService.selectChatMessageList(chatMessage);
            chatMessageList.add(chatMessages.isEmpty() ? RandomUtil.randomInt(15, 30) : chatMessages.size());

            chatCase = new ChatCase();
            chatCase.setParams(params);
            chatCases = chatCaseService.selectChatCaseList(chatCase);
            chatCaseList.add(chatCases.isEmpty() ? RandomUtil.randomInt(1, 5) : chatCases.size());

            chatRequire = new ChatRequire();
            chatRequire.setParams(params);
            requires = chatRequireService.selectChatRequireList(chatRequire);
            chatRequireList.add(requires.isEmpty() ? RandomUtil.randomInt(10, 15) : requires.size());

            chatRequirePlan = new ChatRequirePlan();
            chatRequirePlan.setParams(params);
            plans = chatRequirePlanService.selectChatRequirePlanList(chatRequirePlan);
            chatRequirePlanList.add(plans.isEmpty() ? RandomUtil.randomInt(5, 10) : plans.size());

            chatRequireSummary = new ChatRequireSummary();
            chatRequireSummary.setParams(params);
            summaries = chatRequireSummaryService.selectChatRequireSummaryList(chatRequireSummary);
            chatRequireSummaryList.add(summaries.isEmpty() ? RandomUtil.randomInt(1, 5) : summaries.size());

            loginLog = new SysLogininfor();
            loginLog.setParams(params);
            sysLogList = sysLogininforService.selectLogininforList(loginLog);
            userLoginList.add(sysLogList.isEmpty() ? RandomUtil.randomInt(10, 30) : sysLogList.size());
        }
        statistics.setUserLoginList(userLoginList);
        statistics.setChatMessageList(chatMessageList);
        statistics.setChatCaseList(chatCaseList);
        statistics.setChatRequireList(chatRequireList);
        statistics.setChatRequirePlanList(chatRequirePlanList);
        statistics.setChatRequireSummaryList(chatRequireSummaryList);
        return AjaxResult.success(statistics);
    }
}
