package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.ChatRequire;
import com.ruoyi.system.domain.ChatRequirePlan;
import com.ruoyi.system.service.IChatRequirePlanService;
import com.ruoyi.system.service.IChatRequireService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 需求方案信息Controller
 *
 * @author ruoyi
 * @date 2024-04-06
 */
@RestController
@RequestMapping("/system/plan")
public class ChatRequirePlanController extends BaseController {
    @Autowired
    private IChatRequirePlanService chatRequirePlanService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IChatRequireService chatRequireService;

    /**
     * 查询需求方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:plan:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatRequirePlan chatRequirePlan) {
        startPage();
        List<ChatRequirePlan> list = chatRequirePlanService.selectChatRequirePlanList(chatRequirePlan);
        if (null != list && list.size() > 0) {
            for (ChatRequirePlan plan : list) {
                SysUser sysUser = sysUserService.selectUserById(plan.getUserId());
                plan.setUserName(null != sysUser ? sysUser.getUserName() : String.valueOf(plan.getUserId()));
                ChatRequire chatRequire = chatRequireService.selectChatRequireById(plan.getRequireId());
                plan.setRequireTitle(null != chatRequire ? chatRequire.getTitle() : String.valueOf(plan.getRequireId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出需求方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:plan:export')")
    @Log(title = "需求方案信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatRequirePlan chatRequirePlan) {
        List<ChatRequirePlan> list = chatRequirePlanService.selectChatRequirePlanList(chatRequirePlan);
        ExcelUtil<ChatRequirePlan> util = new ExcelUtil<ChatRequirePlan>(ChatRequirePlan.class);
        util.exportExcel(response, list, "需求方案信息数据");
    }

    /**
     * 获取需求方案信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:plan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        ChatRequirePlan plan = chatRequirePlanService.selectChatRequirePlanById(id);
        if (null != plan) {
            SysUser sysUser = sysUserService.selectUserById(plan.getUserId());
            plan.setUserName(null != sysUser ? sysUser.getUserName() : String.valueOf(plan.getUserId()));
            ChatRequire chatRequire = chatRequireService.selectChatRequireById(plan.getRequireId());
            plan.setRequireTitle(null != chatRequire ? chatRequire.getTitle() : String.valueOf(plan.getRequireId()));
        }
        return success(plan);
    }

    /**
     * 新增需求方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:plan:add')")
    @Log(title = "需求方案信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatRequirePlan chatRequirePlan) {
        chatRequirePlan.setIp(IpUtils.getIpAddr());
        chatRequirePlan.setCreateBy(getUsername());
        return toAjax(chatRequirePlanService.insertChatRequirePlan(chatRequirePlan));
    }

    /**
     * 修改需求方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:plan:edit')")
    @Log(title = "需求方案信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatRequirePlan chatRequirePlan) {
        return toAjax(chatRequirePlanService.updateChatRequirePlan(chatRequirePlan));
    }

    /**
     * 删除需求方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:plan:remove')")
    @Log(title = "需求方案信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(chatRequirePlanService.deleteChatRequirePlanByIds(ids));
    }
}
