package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.ChatMessage;
import com.ruoyi.system.service.IChatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【BankChat会话消息】Controller
 *
 * @author ruoyi
 * @date 2024-03-25
 */
@RestController
@RequestMapping("/system/message")
public class ChatMessageController extends BaseController {
    @Autowired
    private IChatMessageService chatMessageService;

    /**
     * 查询【BankChat会话消息】列表
     */
    @PreAuthorize("@ss.hasPermi('system:message:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatMessage chatMessage) {
        startPage();
        chatMessage.setRole("user");
        List<ChatMessage> list = chatMessageService.selectChatMessageList(chatMessage);
        if (null != list && !list.isEmpty()) {
            for (ChatMessage message : list) {
                ChatMessage query = new ChatMessage();
                query.setParentId(message.getId());
                query.setSessionId(message.getSessionId());
                query.setTopicId(message.getTopicId());
                List<ChatMessage> messageList = chatMessageService.selectChatMessageList(query);
                if (null != messageList && !messageList.isEmpty()) {
                    message.setAiContent(messageList.get(0).getContent());
                }
            }
        }

        return getDataTable(list);
    }

    /**
     * 导出【BankChat会话消息】列表
     */
    @PreAuthorize("@ss.hasPermi('system:message:export')")
    @Log(title = "【BankChat会话消息】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatMessage chatMessage) {
        List<ChatMessage> list = chatMessageService.selectChatMessageList(chatMessage);
        ExcelUtil<ChatMessage> util = new ExcelUtil<ChatMessage>(ChatMessage.class);
        util.exportExcel(response, list, "【BankChat会话消息】数据");
    }

    /**
     * 获取【BankChat会话消息】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:message:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        ChatMessage chatMessage = chatMessageService.selectChatMessageById(id);
        ChatMessage chatMessageTemp = chatMessageService.selectChatMessageByParentId(id);
        if (null != chatMessageTemp) {
            chatMessage.setAiContent(chatMessageTemp.getContent());
        }
        return success(chatMessage);
    }

    /**
     * 新增【BankChat会话消息】
     */
    @PreAuthorize("@ss.hasPermi('system:message:add')")
    @Log(title = "【BankChat会话消息】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatMessage chatMessage) {
        chatMessage.setCreateBy(getUsername());
        return toAjax(chatMessageService.insertChatMessage(chatMessage));
    }

    /**
     * 修改【BankChat会话消息】
     */
    @PreAuthorize("@ss.hasPermi('system:message:edit')")
    @Log(title = "【BankChat会话消息】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatMessage chatMessage) {
        // 回复id的消息，并保持为一条新纪录，parentId=id的新纪录【BankChat会话消息】
        ChatMessage chatMessageTemp = chatMessageService.selectChatMessageByParentId(chatMessage.getId());
        if (null != chatMessageTemp) {
            chatMessageTemp.setContent(chatMessage.getAiContent());
            return toAjax(chatMessageService.updateChatMessage(chatMessageTemp));
        }
        chatMessage.setParentId(chatMessage.getId());
        chatMessage.setId(StringUtils.randomStr(8));
        chatMessage.setRole("system");
        chatMessage.setContent(chatMessage.getAiContent());
        return toAjax(chatMessageService.insertChatMessage(chatMessage));
    }

    /**
     * 删除【BankChat会话消息】
     */
    @PreAuthorize("@ss.hasPermi('system:message:remove')")
    @Log(title = "【BankChat会话消息】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(chatMessageService.deleteChatMessageByIds(ids));
    }
}
