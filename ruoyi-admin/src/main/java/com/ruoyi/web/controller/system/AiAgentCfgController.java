package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AiAgentCfg;
import com.ruoyi.system.service.IAiAgentCfgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 不同类型对话Agent配置信息Controller
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/agent/cfg")
public class AiAgentCfgController extends BaseController
{
    @Autowired
    private IAiAgentCfgService aiAgentCfgService;

    /**
     * 查询不同类型对话Agent配置信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiAgentCfg aiAgentCfg)
    {
        startPage();
        List<AiAgentCfg> list = aiAgentCfgService.selectAiAgentCfgList(aiAgentCfg);
        return getDataTable(list);
    }

    /**
     * 导出不同类型对话Agent配置信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:export')")
    @Log(title = "不同类型对话Agent配置信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiAgentCfg aiAgentCfg)
    {
        List<AiAgentCfg> list = aiAgentCfgService.selectAiAgentCfgList(aiAgentCfg);
        ExcelUtil<AiAgentCfg> util = new ExcelUtil<AiAgentCfg>(AiAgentCfg.class);
        util.exportExcel(response, list, "不同类型对话Agent配置信息数据");
    }

    /**
     * 获取不同类型对话Agent配置信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(aiAgentCfgService.selectAiAgentCfgById(id));
    }

    /**
     * 新增不同类型对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:add')")
    @Log(title = "不同类型对话Agent配置信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiAgentCfg aiAgentCfg)
    {
        return toAjax(aiAgentCfgService.insertAiAgentCfg(aiAgentCfg));
    }

    /**
     * 修改不同类型对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:edit')")
    @Log(title = "不同类型对话Agent配置信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiAgentCfg aiAgentCfg)
    {
        return toAjax(aiAgentCfgService.updateAiAgentCfg(aiAgentCfg));
    }

    /**
     * 删除不同类型对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:cfg:remove')")
    @Log(title = "不同类型对话Agent配置信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(aiAgentCfgService.deleteAiAgentCfgByIds(ids));
    }
}
