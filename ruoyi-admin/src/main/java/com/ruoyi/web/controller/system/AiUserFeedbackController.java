package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiUserFeedback;
import com.ruoyi.system.service.IAiUserFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户意见反馈信息Controller
 *
 * @author ruoyi
 * @date 2024-05-07
 */
@RestController
@RequestMapping("/ai/feedback")
public class AiUserFeedbackController extends BaseController {
    @Autowired
    private IAiUserFeedbackService aiUserFeedbackService;

    /**
     * 查询用户意见反馈信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiUserFeedback aiUserFeedback) {
        startPage();
        List<AiUserFeedback> list = aiUserFeedbackService.selectAiUserFeedbackList(aiUserFeedback);
        return getDataTable(list);
    }

    /**
     * 导出用户意见反馈信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:export')")
    @Log(title = "用户意见反馈信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiUserFeedback aiUserFeedback) {
        List<AiUserFeedback> list = aiUserFeedbackService.selectAiUserFeedbackList(aiUserFeedback);
        ExcelUtil<AiUserFeedback> util = new ExcelUtil<AiUserFeedback>(AiUserFeedback.class);
        util.exportExcel(response, list, "用户意见反馈信息数据");
    }

    /**
     * 获取用户意见反馈信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(aiUserFeedbackService.selectAiUserFeedbackById(id));
    }

    /**
     * 新增用户意见反馈信息
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:add')")
    @Log(title = "用户意见反馈信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiUserFeedback aiUserFeedback) {
        return toAjax(aiUserFeedbackService.insertAiUserFeedback(aiUserFeedback));
    }

    /**
     * 修改用户意见反馈信息
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:edit')")
    @Log(title = "用户意见反馈信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiUserFeedback aiUserFeedback) {
        return toAjax(aiUserFeedbackService.updateAiUserFeedback(aiUserFeedback));
    }

    /**
     * 删除用户意见反馈信息
     */
    @PreAuthorize("@ss.hasPermi('ai:feedback:remove')")
    @Log(title = "用户意见反馈信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(aiUserFeedbackService.deleteAiUserFeedbackByIds(ids));
    }
}
