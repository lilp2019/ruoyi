package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.ChatCase;
import com.ruoyi.system.service.IChatCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 案例信息Controller
 *
 * @author ruoyi
 * @date 2024-04-08
 */
@RestController
@RequestMapping("/system/case")
public class ChatCaseController extends BaseController {
    @Autowired
    private IChatCaseService chatCaseService;

    /**
     * 查询案例信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:case:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatCase chatCase) {
        startPage();
        List<ChatCase> list = chatCaseService.selectChatCaseList(chatCase);
        return getDataTable(list);
    }

    /**
     * 导出案例信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:case:export')")
    @Log(title = "案例信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatCase chatCase) {
        List<ChatCase> list = chatCaseService.selectChatCaseList(chatCase);
        ExcelUtil<ChatCase> util = new ExcelUtil<ChatCase>(ChatCase.class);
        util.exportExcel(response, list, "案例信息数据");
    }

    /**
     * 获取案例信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:case:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return success(chatCaseService.selectChatCaseById(id));
    }

    /**
     * 新增案例信息
     */
    @PreAuthorize("@ss.hasPermi('system:case:add')")
    @Log(title = "案例信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatCase chatCase) {
        chatCase.setCreateBy(getUsername());
        return toAjax(chatCaseService.insertChatCase(chatCase));
    }

    /**
     * 修改案例信息
     */
    @PreAuthorize("@ss.hasPermi('system:case:edit')")
    @Log(title = "案例信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatCase chatCase) {
        return toAjax(chatCaseService.updateChatCase(chatCase));
    }

    /**
     * 删除案例信息
     */
    @PreAuthorize("@ss.hasPermi('system:case:remove')")
    @Log(title = "案例信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(chatCaseService.deleteChatCaseByIds(ids));
    }
}
