package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.ChatRequire;
import com.ruoyi.system.domain.ChatRequireSummary;
import com.ruoyi.system.service.IChatRequireService;
import com.ruoyi.system.service.IChatRequireSummaryService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 需求总结方案信息Controller
 *
 * @author ruoyi
 * @date 2024-04-06
 */
@RestController
@RequestMapping("/system/summary")
public class ChatRequireSummaryController extends BaseController {
    @Autowired
    private IChatRequireSummaryService chatRequireSummaryService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IChatRequireService chatRequireService;

    /**
     * 查询需求总结方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:summary:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatRequireSummary chatRequireSummary) {
        startPage();
        List<ChatRequireSummary> list = chatRequireSummaryService.selectChatRequireSummaryList(chatRequireSummary);
        if (null != list && list.size() > 0) {
            for (ChatRequireSummary summary : list) {
                SysUser sysUser = sysUserService.selectUserById(summary.getUserId());
                summary.setUserName(null != sysUser ? sysUser.getUserName() : String.valueOf(summary.getUserId()));
                ChatRequire chatRequire = chatRequireService.selectChatRequireById(summary.getRequireId());
                summary.setRequireTitle(null != chatRequire ? chatRequire.getTitle() : String.valueOf(summary.getRequireId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出需求总结方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:summary:export')")
    @Log(title = "需求总结方案信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatRequireSummary chatRequireSummary) {
        List<ChatRequireSummary> list = chatRequireSummaryService.selectChatRequireSummaryList(chatRequireSummary);
        ExcelUtil<ChatRequireSummary> util = new ExcelUtil<ChatRequireSummary>(ChatRequireSummary.class);
        util.exportExcel(response, list, "需求总结方案信息数据");
    }

    /**
     * 获取需求总结方案信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:summary:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        ChatRequireSummary summary = chatRequireSummaryService.selectChatRequireSummaryById(id);
        if (null != summary) {
            SysUser sysUser = sysUserService.selectUserById(summary.getUserId());
            summary.setUserName(null != sysUser ? sysUser.getUserName() : String.valueOf(summary.getUserId()));
            ChatRequire chatRequire = chatRequireService.selectChatRequireById(summary.getRequireId());
            summary.setRequireTitle(null != chatRequire ? chatRequire.getTitle() : String.valueOf(summary.getRequireId()));
        }
        return success(summary);
    }

    /**
     * 新增需求总结方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:summary:add')")
    @Log(title = "需求总结方案信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatRequireSummary chatRequireSummary) {
        chatRequireSummary.setCreateBy(getUsername());
        return toAjax(chatRequireSummaryService.insertChatRequireSummary(chatRequireSummary));
    }

    /**
     * 修改需求总结方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:summary:edit')")
    @Log(title = "需求总结方案信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatRequireSummary chatRequireSummary) {
        return toAjax(chatRequireSummaryService.updateChatRequireSummary(chatRequireSummary));
    }

    /**
     * 删除需求总结方案信息
     */
    @PreAuthorize("@ss.hasPermi('system:summary:remove')")
    @Log(title = "需求总结方案信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(chatRequireSummaryService.deleteChatRequireSummaryByIds(ids));
    }
}
