package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiScheme;
import com.ruoyi.system.service.IAiSchemeService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户方案信息Controller
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/scheme")
public class AiSchemeController extends BaseController {
    @Autowired
    private IAiSchemeService aiSchemeService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiScheme aiScheme) {
        startPage();
        List<AiScheme> list = aiSchemeService.selectAiSchemeList(aiScheme);
        if (null != list && list.size() > 0) {
            for (AiScheme scheme : list) {
                SysUser sysUser = sysUserService.selectUserById(scheme.getUserId());
                scheme.setUserName(null != sysUser ? sysUser.getNickName() : String.valueOf(scheme.getUserId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户方案信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:export')")
    @Log(title = "用户方案信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiScheme aiScheme) {
        List<AiScheme> list = aiSchemeService.selectAiSchemeList(aiScheme);
        ExcelUtil<AiScheme> util = new ExcelUtil<AiScheme>(AiScheme.class);
        util.exportExcel(response, list, "用户方案信息数据");
    }

    /**
     * 获取用户方案信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(aiSchemeService.selectAiSchemeById(id));
    }

    /**
     * 新增用户方案信息
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:add')")
    @Log(title = "用户方案信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiScheme aiScheme) {
        return toAjax(aiSchemeService.insertAiScheme(aiScheme));
    }

    /**
     * 修改用户方案信息
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:edit')")
    @Log(title = "用户方案信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiScheme aiScheme) {
        return toAjax(aiSchemeService.updateAiScheme(aiScheme));
    }

    /**
     * 删除用户方案信息
     */
    @PreAuthorize("@ss.hasPermi('ai:scheme:remove')")
    @Log(title = "用户方案信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(aiSchemeService.deleteAiSchemeByIds(ids));
    }
}
