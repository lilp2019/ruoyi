package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiSession;
import com.ruoyi.system.service.IAiSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户对话Session信息Controller
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/session")
public class AiSessionController extends BaseController
{
    @Autowired
    private IAiSessionService aiSessionService;

    /**
     * 查询用户对话Session信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:session:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiSession aiSession)
    {
        startPage();
        List<AiSession> list = aiSessionService.selectAiSessionList(aiSession);
        return getDataTable(list);
    }

    /**
     * 导出用户对话Session信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:session:export')")
    @Log(title = "用户对话Session信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiSession aiSession)
    {
        List<AiSession> list = aiSessionService.selectAiSessionList(aiSession);
        ExcelUtil<AiSession> util = new ExcelUtil<AiSession>(AiSession.class);
        util.exportExcel(response, list, "用户对话Session信息数据");
    }

    /**
     * 获取用户对话Session信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:session:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(aiSessionService.selectAiSessionById(id));
    }

    /**
     * 新增用户对话Session信息
     */
    @PreAuthorize("@ss.hasPermi('ai:session:add')")
    @Log(title = "用户对话Session信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiSession aiSession)
    {
        return toAjax(aiSessionService.insertAiSession(aiSession));
    }

    /**
     * 修改用户对话Session信息
     */
    @PreAuthorize("@ss.hasPermi('ai:session:edit')")
    @Log(title = "用户对话Session信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiSession aiSession)
    {
        return toAjax(aiSessionService.updateAiSession(aiSession));
    }

    /**
     * 删除用户对话Session信息
     */
    @PreAuthorize("@ss.hasPermi('ai:session:remove')")
    @Log(title = "用户对话Session信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(aiSessionService.deleteAiSessionByIds(ids));
    }
}
