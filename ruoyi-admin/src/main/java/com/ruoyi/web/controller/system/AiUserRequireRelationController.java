package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiUserRequireRelation;
import com.ruoyi.system.service.IAiUserRequireRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户需求商机信息Controller
 *
 * @author ruoyi
 * @date 2024-05-15
 */
@RestController
@RequestMapping("/ai/relation")
public class AiUserRequireRelationController extends BaseController {
    @Autowired
    private IAiUserRequireRelationService aiUserRequireRelationService;

    /**
     * 查询用户需求商机信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiUserRequireRelation aiUserRequireRelation) {
        startPage();
        List<AiUserRequireRelation> list = aiUserRequireRelationService.selectAiUserRequireRelationList(aiUserRequireRelation);
        return getDataTable(list);
    }

    /**
     * 导出用户需求商机信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:export')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiUserRequireRelation aiUserRequireRelation) {
        List<AiUserRequireRelation> list = aiUserRequireRelationService.selectAiUserRequireRelationList(aiUserRequireRelation);
        ExcelUtil<AiUserRequireRelation> util = new ExcelUtil<AiUserRequireRelation>(AiUserRequireRelation.class);
        util.exportExcel(response, list, "用户需求商机信息数据");
    }

    /**
     * 获取用户需求商机信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(aiUserRequireRelationService.selectAiUserRequireRelationById(id));
    }

    /**
     * 新增用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:add')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiUserRequireRelation aiUserRequireRelation) {
        return toAjax(aiUserRequireRelationService.insertAiUserRequireRelation(aiUserRequireRelation));
    }

    /**
     * 修改用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:edit')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiUserRequireRelation aiUserRequireRelation) {
        return toAjax(aiUserRequireRelationService.updateAiUserRequireRelation(aiUserRequireRelation));
    }

    /**
     * 删除用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:relation:remove')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(aiUserRequireRelationService.deleteAiUserRequireRelationByIds(ids));
    }
}
