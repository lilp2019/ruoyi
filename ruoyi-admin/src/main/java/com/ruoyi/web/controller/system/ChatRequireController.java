package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.ChatRequire;
import com.ruoyi.system.service.IChatRequireService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户需求信息Controller
 *
 * @author ruoyi
 * @date 2024-04-06
 */
@RestController
@RequestMapping("/system/require")
public class ChatRequireController extends BaseController {
    @Autowired
    private IChatRequireService chatRequireService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户需求信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:require:list')")
    @GetMapping("/list")
    public TableDataInfo list(ChatRequire chatRequire) {
        startPage();
        List<ChatRequire> list = chatRequireService.selectChatRequireList(chatRequire);
        if (null != list && list.size() > 0) {
            for (ChatRequire require : list) {
                SysUser sysUser = sysUserService.selectUserById(require.getUserId());
                require.setUserName(null != sysUser ? sysUser.getUserName() : String.valueOf(require.getUserId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户需求信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:require:export')")
    @Log(title = "用户需求信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ChatRequire chatRequire) {
        List<ChatRequire> list = chatRequireService.selectChatRequireList(chatRequire);
        ExcelUtil<ChatRequire> util = new ExcelUtil<ChatRequire>(ChatRequire.class);
        util.exportExcel(response, list, "用户需求信息数据");
    }

    /**
     * 获取用户需求信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:require:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return success(chatRequireService.selectChatRequireById(id));
    }

    /**
     * 新增用户需求信息
     */
    @PreAuthorize("@ss.hasPermi('system:require:add')")
    @Log(title = "用户需求信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ChatRequire chatRequire) {
        chatRequire.setCreateBy(getUsername());
        return toAjax(chatRequireService.insertChatRequire(chatRequire));
    }

    /**
     * 修改用户需求信息
     */
    @PreAuthorize("@ss.hasPermi('system:require:edit')")
    @Log(title = "用户需求信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ChatRequire chatRequire) {
        return toAjax(chatRequireService.updateChatRequire(chatRequire));
    }

    /**
     * 删除用户需求信息
     */
    @PreAuthorize("@ss.hasPermi('system:require:remove')")
    @Log(title = "用户需求信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids) {
        return toAjax(chatRequireService.deleteChatRequireByIds(ids));
    }
}
