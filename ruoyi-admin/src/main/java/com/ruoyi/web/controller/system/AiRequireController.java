package com.ruoyi.web.controller.system;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiRequire;
import com.ruoyi.system.service.IAiRequireService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户需求商机信息Controller
 *
 * @author ruoyi
 * @date 2024-05-05
 */
@RestController
@RequestMapping("/ai/require")
public class AiRequireController extends BaseController {
    @Autowired
    private IAiRequireService aiRequireService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户需求商机信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:require:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiRequire aiRequire) {
        startPage();
        List<AiRequire> list = aiRequireService.selectAiRequireList(aiRequire);
        if (CollUtil.isNotEmpty(list)) {
            for (AiRequire require : list) {
                SysUser sysUser = sysUserService.selectUserById(require.getUserId());
                require.setUserName(null != sysUser ? sysUser.getNickName() : String.valueOf(require.getUserId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户需求商机信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:require:export')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiRequire aiRequire) {
        List<AiRequire> list = aiRequireService.selectAiRequireList(aiRequire);
        ExcelUtil<AiRequire> util = new ExcelUtil<AiRequire>(AiRequire.class);
        util.exportExcel(response, list, "用户需求商机信息数据");
    }

    /**
     * 获取用户需求商机信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:require:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(aiRequireService.selectAiRequireById(id));
    }

    /**
     * 新增用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:require:add')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiRequire aiRequire) {
        return toAjax(aiRequireService.insertAiRequire(aiRequire));
    }

    /**
     * 修改用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:require:edit')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiRequire aiRequire) {
        return toAjax(aiRequireService.updateAiRequire(aiRequire));
    }

    /**
     * 删除用户需求商机信息
     */
    @PreAuthorize("@ss.hasPermi('ai:require:remove')")
    @Log(title = "用户需求商机信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(aiRequireService.deleteAiRequireByIds(ids));
    }
}
