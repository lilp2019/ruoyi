package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiRequire;
import com.ruoyi.system.domain.AiRequirePlan;
import com.ruoyi.system.service.IAiRequirePlanService;
import com.ruoyi.system.service.IAiRequireService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户商机意向合作信息Controller
 *
 * @author ruoyi
 * @date 2024-05-10
 */
@RestController
@RequestMapping("/ai/plan")
public class AiRequirePlanController extends BaseController {
    @Autowired
    private IAiRequirePlanService aiRequirePlanService;
    @Autowired
    private IAiRequireService aiRequireService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户商机意向合作信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiRequirePlan aiRequirePlan) {
        startPage();
        List<AiRequirePlan> list = aiRequirePlanService.selectAiRequirePlanList(aiRequirePlan);
        if (null != list && list.size() > 0) {
            for (AiRequirePlan plan : list) {
                SysUser sysUser = sysUserService.selectUserById(plan.getUserId());
                plan.setNickName(null != sysUser ? sysUser.getNickName() : String.valueOf(plan.getUserId()));
                AiRequire require = aiRequireService.selectAiRequireById(plan.getRequireId());
                plan.setRequireTitle(null != require ? require.getTitle() : String.valueOf(plan.getRequireId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户商机意向合作信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:export')")
    @Log(title = "用户商机意向合作信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiRequirePlan aiRequirePlan) {
        List<AiRequirePlan> list = aiRequirePlanService.selectAiRequirePlanList(aiRequirePlan);
        ExcelUtil<AiRequirePlan> util = new ExcelUtil<AiRequirePlan>(AiRequirePlan.class);
        util.exportExcel(response, list, "用户商机意向合作信息数据");
    }

    /**
     * 获取用户商机意向合作信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(aiRequirePlanService.selectAiRequirePlanById(id));
    }

    /**
     * 新增用户商机意向合作信息
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:add')")
    @Log(title = "用户商机意向合作信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiRequirePlan aiRequirePlan) {
        return toAjax(aiRequirePlanService.insertAiRequirePlan(aiRequirePlan));
    }

    /**
     * 修改用户商机意向合作信息
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:edit')")
    @Log(title = "用户商机意向合作信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiRequirePlan aiRequirePlan) {
        return toAjax(aiRequirePlanService.updateAiRequirePlan(aiRequirePlan));
    }

    /**
     * 删除用户商机意向合作信息
     */
    @PreAuthorize("@ss.hasPermi('ai:plan:remove')")
    @Log(title = "用户商机意向合作信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(aiRequirePlanService.deleteAiRequirePlanByIds(ids));
    }
}
