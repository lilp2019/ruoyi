package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiTopic;
import com.ruoyi.system.service.IAiTopicService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户会话topic信息Controller
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/topic")
public class AiTopicController extends BaseController {
    @Autowired
    private IAiTopicService aiTopicService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户会话topic信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiTopic aiTopic) {
        startPage();
        List<AiTopic> list = aiTopicService.selectAiTopicList(aiTopic);
        if (null != list && list.size() > 0) {
            for (AiTopic topic : list) {
                SysUser sysUser = sysUserService.selectUserById(topic.getUserId());
                topic.setUserName(null != sysUser ? sysUser.getNickName() : String.valueOf(topic.getUserId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户会话topic信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:export')")
    @Log(title = "用户会话topic信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiTopic aiTopic) {
        List<AiTopic> list = aiTopicService.selectAiTopicList(aiTopic);
        ExcelUtil<AiTopic> util = new ExcelUtil<AiTopic>(AiTopic.class);
        util.exportExcel(response, list, "用户会话topic信息数据");
    }

    /**
     * 获取用户会话topic信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(aiTopicService.selectAiTopicById(id));
    }

    /**
     * 新增用户会话topic信息
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:add')")
    @Log(title = "用户会话topic信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiTopic aiTopic) {
        aiTopic.setId(StringUtils.randomStr(8));
        return toAjax(aiTopicService.insertAiTopic(aiTopic));
    }

    /**
     * 修改用户会话topic信息
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:edit')")
    @Log(title = "用户会话topic信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiTopic aiTopic) {
        return toAjax(aiTopicService.updateAiTopic(aiTopic));
    }

    /**
     * 删除用户会话topic信息
     */
    @PreAuthorize("@ss.hasPermi('ai:topic:remove')")
    @Log(title = "用户会话topic信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(aiTopicService.deleteAiTopicByIds(ids));
    }
}
