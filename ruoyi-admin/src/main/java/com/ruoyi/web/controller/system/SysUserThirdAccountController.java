package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysUserThirdAccount;
import com.ruoyi.system.service.ISysUserThirdAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 第三方账户绑定Controller
 *
 * @author ruoyi
 * @date 2023-05-08
 */
@RestController
@RequestMapping("/system/account")
public class SysUserThirdAccountController extends BaseController {
    @Autowired
    private ISysUserThirdAccountService sysUserThirdAccountService;

    /**
     * 查询第三方账户绑定列表
     */
    @PreAuthorize("@ss.hasPermi('system:account:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserThirdAccount sysUserThirdAccount) {
        startPage();
        List list = sysUserThirdAccountService.selectSysUserThirdAccountList(sysUserThirdAccount);
        return getDataTable(list);
    }

    /**
     * 导出第三方账户绑定列表
     */
    @PreAuthorize("@ss.hasPermi('system:account:export')")
    @Log(title = "第三方账户绑定", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserThirdAccount sysUserThirdAccount) {
        List list = sysUserThirdAccountService.selectSysUserThirdAccountList(sysUserThirdAccount);
        ExcelUtil util = new ExcelUtil(SysUserThirdAccount.class);
        util.exportExcel(response, list, "第三方账户绑定数据");
    }

    /**
     * 获取第三方账户绑定详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:account:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(sysUserThirdAccountService.selectSysUserThirdAccountById(id));
    }

    /**
     * 新增第三方账户绑定
     */
    @PreAuthorize("@ss.hasPermi('system:account:add')")
    @Log(title = "第三方账户绑定", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserThirdAccount sysUserThirdAccount) {
        return toAjax(sysUserThirdAccountService.insertSysUserThirdAccount(sysUserThirdAccount));
    }

    /**
     * 修改第三方账户绑定
     */
    @PreAuthorize("@ss.hasPermi('system:account:edit')")
    @Log(title = "第三方账户绑定", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserThirdAccount sysUserThirdAccount) {
        return toAjax(sysUserThirdAccountService.updateSysUserThirdAccount(sysUserThirdAccount));
    }

    /**
     * 删除第三方账户绑定
     */
    @PreAuthorize("@ss.hasPermi('system:account:remove')")
    @Log(title = "第三方账户绑定", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(sysUserThirdAccountService.deleteSysUserThirdAccountByIds(ids));
    }

    /**
     * 查询当前用户第三方账户绑定列表
     */
    @GetMapping("/thirdAccountList")
    public TableDataInfo thirdAccountList(SysUserThirdAccount sysUserThirdAccount) {
        //设置优化id
        sysUserThirdAccount.setUserId(SecurityUtils.getUserId());
        sysUserThirdAccount.setBindFlag("1");
        startPage();
        List list = sysUserThirdAccountService.selectSysUserThirdAccountList(sysUserThirdAccount);
        return getDataTable(list);
    }
}
