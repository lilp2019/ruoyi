package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.AiAgent;
import com.ruoyi.system.service.IAiAgentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户对话Agent配置信息Controller
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/agent")
public class AiAgentController extends BaseController
{
    @Autowired
    private IAiAgentService aiAgentService;

    /**
     * 查询用户对话Agent配置信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiAgent aiAgent)
    {
        startPage();
        List<AiAgent> list = aiAgentService.selectAiAgentList(aiAgent);
        return getDataTable(list);
    }

    /**
     * 导出用户对话Agent配置信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:export')")
    @Log(title = "用户对话Agent配置信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiAgent aiAgent)
    {
        List<AiAgent> list = aiAgentService.selectAiAgentList(aiAgent);
        ExcelUtil<AiAgent> util = new ExcelUtil<AiAgent>(AiAgent.class);
        util.exportExcel(response, list, "用户对话Agent配置信息数据");
    }

    /**
     * 获取用户对话Agent配置信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(aiAgentService.selectAiAgentById(id));
    }

    /**
     * 新增用户对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:add')")
    @Log(title = "用户对话Agent配置信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiAgent aiAgent)
    {
        return toAjax(aiAgentService.insertAiAgent(aiAgent));
    }

    /**
     * 修改用户对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:edit')")
    @Log(title = "用户对话Agent配置信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiAgent aiAgent)
    {
        return toAjax(aiAgentService.updateAiAgent(aiAgent));
    }

    /**
     * 删除用户对话Agent配置信息
     */
    @PreAuthorize("@ss.hasPermi('ai:agent:remove')")
    @Log(title = "用户对话Agent配置信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(aiAgentService.deleteAiAgentByIds(ids));
    }
}
