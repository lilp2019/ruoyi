package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.AiMessage;
import com.ruoyi.system.service.IAiMessageService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户会话信息Controller
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@RestController
@RequestMapping("/ai/message")
public class AiMessageController extends BaseController {
    @Autowired
    private IAiMessageService aiMessageService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询用户会话信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:message:list')")
    @GetMapping("/list")
    public TableDataInfo list(AiMessage aiMessage) {
        startPage();
        List<AiMessage> list = aiMessageService.selectAiMessageList(aiMessage);
        if (null != list && list.size() > 0) {
            for (AiMessage message : list) {
                SysUser sysUser = sysUserService.selectUserById(message.getUserId());
                message.setUserName(null != sysUser ? sysUser.getNickName() : String.valueOf(message.getUserId()));
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出用户会话信息列表
     */
    @PreAuthorize("@ss.hasPermi('ai:message:export')")
    @Log(title = "用户会话信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AiMessage aiMessage) {
        List<AiMessage> list = aiMessageService.selectAiMessageList(aiMessage);
        ExcelUtil<AiMessage> util = new ExcelUtil<AiMessage>(AiMessage.class);
        util.exportExcel(response, list, "用户会话信息数据");
    }

    /**
     * 获取用户会话信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ai:message:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return success(aiMessageService.selectAiMessageById(id));
    }

    /**
     * 新增用户会话信息
     */
    @PreAuthorize("@ss.hasPermi('ai:message:add')")
    @Log(title = "用户会话信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AiMessage aiMessage) {
        return toAjax(aiMessageService.insertAiMessage(aiMessage));
    }

    /**
     * 修改用户会话信息
     */
    @PreAuthorize("@ss.hasPermi('ai:message:edit')")
    @Log(title = "用户会话信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AiMessage aiMessage) {
        return toAjax(aiMessageService.updateAiMessage(aiMessage));
    }

    /**
     * 删除用户会话信息
     */
    @PreAuthorize("@ss.hasPermi('ai:message:remove')")
    @Log(title = "用户会话信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(aiMessageService.deleteAiMessageByIds(ids));
    }
}
