package com.ruoyi.web.task;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.system.domain.AiRequire;
import com.ruoyi.system.domain.AiScheme;
import com.ruoyi.system.service.IAiRequireService;
import com.ruoyi.system.service.IAiSchemeService;
import com.ruoyi.web.service.ChatBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("chatBankTask")
public class ChatBankTask {

    @Autowired
    private IAiSchemeService schemeService;
    @Autowired
    private IAiRequireService requireService;
    @Autowired
    private ChatBankService chatBankService;

    /**
     * 完善方案/商机相关信息
     */
    public void ryFillSchemeRequire() {
        // 完善方案信息
        AiScheme scheme = new AiScheme();
        scheme.setInitFlag("0"); // 查找待完善中的方案
        List<AiScheme> schemes = schemeService.selectAiSchemeList(scheme);
        if (CollUtil.isNotEmpty(schemes)) {
            for (AiScheme aiScheme : schemes) {
                chatBankService.requestFillChatScheme(aiScheme);
            }
        }
        // 完善商机信息
        AiRequire require = new AiRequire();
        require.setInitFlag("0"); // 查找待完善中的方案
        List<AiRequire> requireList = requireService.selectAiRequireList(require);
        if (CollUtil.isNotEmpty(requireList)) {
            for (AiRequire aiRequire : requireList) {
                chatBankService.requestFillChatRequire(aiRequire);
            }
        }
    }
}
