package com.ruoyi.web.core.config;

import com.ruoyi.common.config.WechatConfig;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * WechatMp配置信息
 *
 * @author ruoyi
 */
@Component
public class WechatMpConfig {

    @Autowired
    private WechatConfig wechatConfig;

    @Bean
    public WxMpService wxMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
        configStorage.setAppId(wechatConfig.getAppId());
        configStorage.setSecret(wechatConfig.getAppSecret());
        wxMpService.setWxMpConfigStorage(configStorage);
        return wxMpService;
    }
}
