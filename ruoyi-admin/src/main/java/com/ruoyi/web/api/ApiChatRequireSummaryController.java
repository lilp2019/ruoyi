package com.ruoyi.web.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.ChatRequireSummary;
import com.ruoyi.system.domain.vo.SysUserVo;
import com.ruoyi.system.service.IChatRequireSummaryService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户需求总结方案信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/chat/require/summary")
public class ApiChatRequireSummaryController extends BaseController {

    @Autowired
    private IChatRequireSummaryService chatRequireSummaryService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询需求总结方案信息详情
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getById(@PathVariable("id") Integer id) {
        ChatRequireSummary chatRequireSummary = chatRequireSummaryService.selectChatRequireSummaryById(id);
        if (null != chatRequireSummary) {
            SysUser sysUser = sysUserService.selectUserById(chatRequireSummary.getUserId());
            if (null != sysUser) {
                chatRequireSummary.setUser(new SysUserVo(sysUser.getUserId(), sysUser.getNickName(), sysUser.getAvatar()));
            }
            if (chatRequireSummary.getStatus().equalsIgnoreCase("1")) {
                chatRequireSummary.setStatus("进行中");
            } else if (chatRequireSummary.getStatus().equalsIgnoreCase("2")) {
                chatRequireSummary.setStatus("已完成");
            } else if (chatRequireSummary.getStatus().equalsIgnoreCase("3")) {
                chatRequireSummary.setStatus("已失败");
            }
        }
        return AjaxResult.success(chatRequireSummary);
    }

    /**
     * 获取需求总结方案信息列表
     */
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam(value = "requireId", required = false) Integer requireId) {
        ChatRequireSummary chatRequireSummary = new ChatRequireSummary();
        chatRequireSummary.setRequireId(requireId);
        chatRequireSummary.setUserId(hasLoginUser());
        List<ChatRequireSummary> chatRequireSummaries = chatRequireSummaryService.selectChatRequireSummaryList(chatRequireSummary);
        if (null != chatRequireSummaries && chatRequireSummaries.size() > 0) {
            for (ChatRequireSummary summary : chatRequireSummaries) {
                SysUser sysUser = sysUserService.selectUserById(summary.getUserId());
                if (null != sysUser) {
                    summary.setUser(new SysUserVo(sysUser.getUserId(), sysUser.getNickName(), sysUser.getAvatar()));
                }
                if (summary.getStatus().equalsIgnoreCase("1")) {
                    summary.setStatus("进行中");
                } else if (summary.getStatus().equalsIgnoreCase("2")) {
                    summary.setStatus("已完成");
                } else if (summary.getStatus().equalsIgnoreCase("3")) {
                    summary.setStatus("已失败");
                }
            }
        }
        return AjaxResult.success(chatRequireSummaries);
    }
}
