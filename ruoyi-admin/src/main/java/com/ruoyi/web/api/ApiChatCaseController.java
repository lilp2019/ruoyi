package com.ruoyi.web.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.ChatCase;
import com.ruoyi.system.service.IChatCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 案例信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/chat/case")
public class ApiChatCaseController extends BaseController {

    @Autowired
    private IChatCaseService chatCaseService;

    /**
     * 获取案例详情
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getById(@PathVariable("id") Integer id) {
        ChatCase chatCase = chatCaseService.selectChatCaseById(id);
        return AjaxResult.success(chatCase);
    }

    /**
     * 获取案例列表信息
     */
    @GetMapping(value = "/list")
    public AjaxResult list() {
        ChatCase chatCase = new ChatCase();
        chatCase.setStatus("0");
        List<ChatCase> chatCases = chatCaseService.selectChatCaseList(chatCase);
        return AjaxResult.success(chatCases);
    }
}
