package com.ruoyi.web.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.sql.SqlUtil;
import com.ruoyi.system.domain.AiRequirePlan;
import com.ruoyi.system.domain.vo.AiRequirePlanReqVo;
import com.ruoyi.system.domain.vo.AiRequirePlanVo;
import com.ruoyi.system.domain.vo.SysUserVo;
import com.ruoyi.system.service.IAiRequirePlanService;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户需求意向合作信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/require/plan")
public class ApiAiRequirePlanController extends BaseController {

    @Autowired
    private IAiRequirePlanService requirePlanService;
    @Autowired
    private ISysUserService sysUserService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "查询商机的意向合作信息列表")
    public AjaxResult list(@RequestParam(value = "requireId", required = false) Long requireId,
                           @RequestParam(value = "current", required = false) Long current,
                           @RequestParam(value = "size", required = false) Long size) {
        AiRequirePlan requirePlan = new AiRequirePlan();
        requirePlan.setRequireId(requireId);
        Map<String, Object> params = new HashMap<>();
        params.put("page", SqlUtil.getLimit(current, size));
        requirePlan.setParams(params);
        List<AiRequirePlan> requirePlans = requirePlanService.selectAiRequirePlanList(requirePlan);
        if (CollUtil.isEmpty(requirePlans)) return AjaxResult.success(new ArrayList<>());
        return AjaxResult.success(requirePlans.stream().map(this::changeItem).collect(Collectors.toList()));
    }

    @Log(title = "【意向合作】", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    @ApiOperation(value = "新增意向合作信息")
    public AjaxResult add(@RequestBody AiRequirePlanReqVo req) {
        AiRequirePlan aiRequirePlan = new AiRequirePlan();
        aiRequirePlan.setUserId(getUserId());
        aiRequirePlan.setRequireId(req.getRequireId());
        aiRequirePlan.setReplyId(req.getReplyId());
        aiRequirePlan.setContent(req.getContent());
        aiRequirePlan.setTags(req.getTags());
        return AjaxResult.success(requirePlanService.insertAiRequirePlan(aiRequirePlan));
    }

    private AiRequirePlanVo changeItem(AiRequirePlan requirePlan) {
        AiRequirePlanVo resp = new AiRequirePlanVo();
        BeanUtil.copyProperties(requirePlan, resp);
        SysUser sysUser = sysUserService.selectUserById(requirePlan.getUserId());
        if (null != sysUser) {
            resp.setUser(new SysUserVo(sysUser.getUserId(), sysUser.getNickName(), sysUser.getAvatar()));
        }
        return resp;
    }
}
