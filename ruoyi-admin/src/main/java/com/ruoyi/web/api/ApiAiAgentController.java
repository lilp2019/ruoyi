package com.ruoyi.web.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.AiAgentCfg;
import com.ruoyi.system.domain.AiSession;
import com.ruoyi.system.domain.vo.AiAgentCfgVo;
import com.ruoyi.system.domain.vo.AiSessionAgentVo;
import com.ruoyi.system.service.IAiAgentCfgService;
import com.ruoyi.system.service.IAiSessionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 案例信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/agent")
public class ApiAiAgentController extends BaseController {

    @Autowired
    private IAiSessionService sessionService;
    @Autowired
    private IAiAgentCfgService agentCfgService;

    @Transactional
    @GetMapping(value = "/info")
    @ApiOperation(value = "根据类型，关联ID获取Agent配置信息")
    public AjaxResult getAgentInfo(@RequestParam(value = "type", required = false) String type,
                                   @RequestParam(value = "relationId", required = false) String relationId) {
        // 每个用户 + type + type下的ID = sessionId，由三个唯一决定
        AiSession aiSession = new AiSession();
        aiSession.setType(type);
        // idea类型，只有用户+type,只有唯一的一个，没有relationId区分
        if (!type.equalsIgnoreCase("idea")) {
            aiSession.setRelationId(relationId);
        }
        aiSession.setUserId(getUserId());
        List<AiSession> sessions = sessionService.selectAiSessionList(aiSession);
        String sessionId = null;
        if (CollUtil.isNotEmpty(sessions)) {
            sessionId = sessions.get(0).getId();
        } else {
            // 没有则初始化一个session
            AiSession session = new AiSession();
            session.setUserId(getUserId());
            session.setType(type);
            session.setRelationId(relationId);
            if (type.equalsIgnoreCase("idea")) {
                session.setRelationId("0");  // idea只有一种，所以没有id, 默认0
            }
            sessionService.insertAiSession(session);
            sessionId = session.getId();
        }
        AiSessionAgentVo resp = new AiSessionAgentVo();
        resp.setSessionId(sessionId);
        // 根据type查找Agent配置信息
        AiAgentCfg agentCfg = new AiAgentCfg();
        agentCfg.setType(type);
        agentCfg.setStatus("0"); // 查找正常的状态的配置信息
        List<AiAgentCfg> agentConfigs = agentCfgService.selectAiAgentCfgList(agentCfg);
        if (CollUtil.isNotEmpty(agentConfigs)) {
            BeanUtil.copyProperties(agentConfigs.get(0), resp);
        }
        return AjaxResult.success(resp);
    }

    @GetMapping(value = "/cfg")
    @ApiOperation(value = "根据类型type，获取Agent配置信息")
    public AjaxResult getAgentCfg(@RequestParam(value = "type", required = false) String type) {
        AiAgentCfg agentCfg = new AiAgentCfg();
        agentCfg.setType(type);
        agentCfg.setStatus("0"); // 查找正常的状态的配置信息
        List<AiAgentCfg> list = agentCfgService.selectAiAgentCfgList(agentCfg);
        if (CollUtil.isEmpty(list)) return AjaxResult.success();
        AiAgentCfgVo resp = new AiAgentCfgVo();
        BeanUtil.copyProperties(list.get(0), resp);
        return AjaxResult.success(resp);
    }
}
