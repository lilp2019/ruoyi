package com.ruoyi.web.api;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.wx.WechatSignatureVo;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信分享相关接口
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/wechat")
public class ApiWechatShareController {
    private static final Logger log = LoggerFactory.getLogger(ApiWechatShareController.class);

    @Autowired
    private WxMpService wxMpService;

    @GetMapping("/signature")
    public AjaxResult getSignature(@RequestParam("url") String url) throws WxErrorException {
        String currentUrl = url.replace("ericToken", "#");
        WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(currentUrl);
        log.info("分享解析Signature：{}", JSONObject.toJSONString(jsapiSignature));

        WechatSignatureVo wechatSignature = new WechatSignatureVo();
        BeanUtils.copyProperties(jsapiSignature, wechatSignature);
        return AjaxResult.success(wechatSignature);
    }
}
