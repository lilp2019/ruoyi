package com.ruoyi.web.api;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.system.domain.ChatMessage;
import com.ruoyi.system.service.IChatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/chat/message")
public class ApiFastGptController {

    @Autowired
    private IChatMessageService chatMessageService;
    private final static String BASE_URL = "http://121.37.159.13:3020/api";
    private final static String API_KEY = "fastgpt-k5pTWlxaC33NURmul3R3C032cHETmGnbY7dGx3E3FhyYKpUakkJFfdq56";

    /**
     * 新增【BankChat会话消息】
     */
    @Log(title = "【BankChat会话消息】", businessType = BusinessType.INSERT)
    @GetMapping(value = "/test")
    public AjaxResult test() {
        // 构建请求体
        Map<String, Object> data = new HashMap<>();
        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);

        // 添加用户消息
        List<Map<String, String>> messages = new ArrayList<>();
        Map<String, String> userMsg = new HashMap<>();
        userMsg.put("role", "user");
        userMsg.put("content", "创作一首七言诗");
        messages.add(userMsg);
        data.put("messages", messages);

        String content = doPost("/v1/chat/completions", JSON.toJSONString(data));
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setContent(content);
        chatMessage.setId(StringUtils.randomStr(8));
        chatMessage.setRole("user");
        return AjaxResult.success(chatMessageService.insertChatMessage(chatMessage));
    }

    public static void main(String[] args) {
        // 构建请求体
        Map<String, Object> data = new HashMap<>();
        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);

        List<Map<String, String>> messages = new ArrayList<>();

        // 添加用户消息
        Map<String, String> userMsg = new HashMap<>();
        userMsg.put("role", "user");
        userMsg.put("content", "我有一辆闲置的二手车，价值10万，有什么好办法");
        messages.add(userMsg);

        userMsg = new HashMap<>();
        userMsg.put("role", "assistant");
        userMsg.put("content", "您希望了解如何有效处理这辆价值10万元的闲置二手车，是吗？为了给您提供最佳建议，请允许我通过几个问题进一步明确您的意图：\n" +
                "出售意向：您是否打算出售这辆二手车？还是考虑其他方式如出租、置换新车、赠予亲友等？\n" +
                "时间要求：您希望在多长时间内处理掉这辆车？是否有特定的时间节点需要关注？\n" +
                "期望收益：除了获得理想售价外，您是否还有其他收益期待，比如希望快速成交、省心省力、尽量减少交易成本等？\n" +
                "车辆状况：这辆二手车的整体车况如何？里程数、保养情况、有无事故记录等具体信息能否分享一下？\n" +
                "市场了解：您是否已经对当前二手车市场行情有所了解，或者有无偏好的交易渠道（如二手车交易平台、4S店置换、个人买家等）？\n" +
                "您可以选择回答以上问题，或者如果您认为我对您的意图理解无误，可以直接告诉我，我们将进入下一步，为您提供具体的处理建议。当然，如果您想直接跳过意图对齐环节，也可以告诉我，我会基于现有信息为您提供一般性的建议。");
        messages.add(userMsg);


        userMsg = new HashMap<>();
        userMsg.put("role", "user");
        userMsg.put("content", "闲置二手车租赁，每月最多能获得多少收益");
        messages.add(userMsg);

        userMsg = new HashMap<>();
        userMsg.put("role", "assistant");
        userMsg.put("content", "您希望了解闲置二手车用于租赁时，每月可能获得的最大收益是多少。在提供更准确的建议前，我需要进一步明确以下几个方面：\\n\\n1. **市场需求与车型**：不同车型、品牌、配置的二手车在租赁市场的受欢迎程度及租金差异较大。能否告诉我您的车辆具体品牌、型号、年份以及主要配置信息？\\n2. **地域因素**：租车市场的供需状况及租金水平受地域影响显著。请告知车辆所在地，以便了解当地的租车市场行情。\\n3. **车辆状况**：车辆的保养状况、里程数、有无事故历史等都会影响其租赁价值。能否描述一下这些方面的详细情况？\\n4. **租赁方式**：您计划通过何种方式进行租赁？例如，通过专业租车平台、个人间短租、长租给企业或个人等，不同的租赁方式可能有不同的收益预期。\\n5. **运营成本**：租赁过程中可能产生的费用，如保险费增加、定期保养、轻微损坏维修、平台服务费（如有）等，需要纳入考虑以计算净收益。\\n\\n请您回答上述问题，以便我为您提供更精确的收益预估。如果您愿意跳过意图对齐环节，我可以基于一般情况给出大致的参考范围，但请注意，实际收益可能会因具体条件差异而有所不同。");
        messages.add(userMsg);

        userMsg = new HashMap<>();
        userMsg.put("role", "user");
        userMsg.put("content", "可以让我的利益最大化的方案");
        messages.add(userMsg);

        userMsg = new HashMap<>();
        userMsg.put("role", "assistant");
        userMsg.put("content", "您希望得到一个能使您在处理闲置二手车时利益最大化的方案。为确保方案的针对性，请允许我通过以下问题进一步明确您的情况和偏好：\n" +
                "\n" +
                "车辆详情：能否详细描述一下车辆的品牌、型号、年份、里程数、保养状况、有无事故历史等信息？这些因素直接影响到车辆的市场价值。\n" +
                "时间考量：您对处置闲置车辆的时间有何要求？是希望尽快出手，还是愿意等待一段时间以寻求更高价格？\n" +
                "市场了解：您是否已经对本地二手车市场行情、热门车型需求、季节性价格波动等有所了解？如果有，能否分享一下？\n" +
                "交易方式偏好：您倾向于通过哪种方式出售二手车？如二手车交易平台、4S店置换、个人买家直售、拍卖、寄售等。\n" +
                "额外资源：您是否具备某些有助于提高出售收益的资源，如丰富的购车者人脉、专业的车辆评估知识、出色的谈判技巧等？\n" +
                "请您回答上述问题，以便我为您设计一个尽可能实现利益最大化的方案。如果您希望直接跳过意图对齐环节，我可以基于一般情况提供一个通用的建议，但可能无法充分考虑到您的个性化需求。");
        messages.add(userMsg);

        userMsg = new HashMap<>();
        userMsg.put("role", "user");
        userMsg.put("content", "判断是否AI已正常提供相应的答案或解决方案。如果是否，则回答：False。如果是则提炼总结提供的相应答案或解决方案，使用JSON格式输出内容。内容模版如下：{\\n\\\"title\\\": \\\"标题\\\",\\n\\\"content\\\": \\\"标准方案内容，内容为完整的Markdown格式，并在每一个内容块中判断是否需要第三方企业、其他个人或者银行协助，如需要则表明协助的内容\\\",\\n\\\"tags\\\": [\\\"主要关键词\\\", \\\"相关关键词1\\\", \\\"相关关键词2\\\", \\\"相关关键词3\\\", \\\"相关关键词4\\\"]\\n}，请严格使用上述模版生成Json内容。");
        messages.add(userMsg);

        data.put("messages", messages);

        // String jdata ="{\"chatId\":\"\",\"stream\":false,\"messages\":[{\"role\":\"user\",\"content\":\"闲置二手车租赁，每月最多能获得多少收益\"},{\"role\":\"assistant\",\"content\":\"您希望了解闲置二手车用于租赁时，每月可能获得的最大收益是多少。在提供更准确的建议前，我需要进一步明确以下几个方面：\\\\n\\\\n1.**市场需求与车型**：不同车型、品牌、配置的二手车在租赁市场的受欢迎程度及租金差异较大。能否告诉我您的车辆具体品牌、型号、年份以及主要配置信息？\\\\n2.**地域因素**：租车市场的供需状况及租金水平受地域影响显著。请告知车辆所在地，以便了解当地的租车市场行情。\\\\n3.**车辆状况**：车辆的保养状况、里程数、有无事故历史等都会影响其租赁价值。能否描述一下这些方面的详细情况？\\\\n4.**租赁方式**：您计划通过何种方式进行租赁？例如，通过专业租车平台、个人间短租、长租给企业或个人等，不同的租赁方式可能有不同的收益预期。\\\\n5.**运营成本**：租赁过程中可能产生的费用，如保险费增加、定期保养、轻微损坏维修、平台服务费（如有）等，需要纳入考虑以计算净收益。\\\\n\\\\n请您回答上述问题，以便我为您提供更精确的收益预估。如果您愿意跳过意图对齐环节，我可以基于一般情况给出大致的参考范围，但请注意，实际收益可能会因具体条件差异而有所不同。\"},{\"role\":\"user\",\"content\":\"帮忙给出最优的方案\"},{\"role\":\"assistant\",\"content\":\"您希望我为您制定处理闲置二手车的最优方案。在提供方案前，我需要通过一些问题来进一步明确您的意图和偏好：\\\\n\\\\n1.**目标优先级**：在处理闲置二手车时，您最看重的是什么？是尽快变现、最大化收益、省心省力，还是其他因素？\\\\n2.**时间要求**：您希望在多长时间内完成车辆的处置？是否有特定的时间节点需要考虑？\\\\n3.**车辆状况**：能否详细描述一下车辆的品牌、型号、年份、里程数、保养状况、有无事故历史等信息？\\\\n4.**市场了解**：您是否已对本地二手车市场行情有一定了解，或者有无偏好的交易方式（如二手车交易平台、4S店置换、个人买家、租赁等）？\\\\n5.**个人资源**：您在推广销售、谈判议价、办理手续等方面有哪些优势或资源可以利用？\\\\n\\\\n请您回答上述问题，以便我为您定制最符合您需求的最优方案。如果您希望直接跳过意图对齐环节，我可以基于一般情况提供一个通用的建议，但可能无法充分考虑到您的个性化需求。\"},{\"role\":\"user\",\"content\":\"判断是否AI已正常提供相应的答案或解决方案。如果是否，则回答：False。如果是则提炼总结提供的相应答案或解决方案，使用JSON格式输出内容。内容模版如下：{\\\\\\\\n\\\\\\\\\\\\\\\"title\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"标题\\\\\\\\\\\\\\\",\\\\\\\\n\\\\\\\\\\\\\\\"content\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"标准方案内容，内容为完整的Markdown格式，并在每一个内容块中判断是否需要第三方企业、其他个人或者银行协助，如需要则表明协助的内容\\\\\\\\\\\\\\\",\\\\\\\\n\\\\\\\\\\\\\\\"tags\\\\\\\\\\\\\\\":[\\\\\\\\\\\\\\\"主要关键词\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"相关关键词1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"相关关键词2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"相关关键词3\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"相关关键词4\\\\\\\\\\\\\\\"]\\\\\\\\n}，请严格使用上述模版生成Json内容。\"}],\"detail\":false}";

        String result = doPost("/v1/chat/completions", JSON.toJSONString(data));

        System.out.println(result);

        JSONObject jsonObject = JSON.parseObject(result);
        JSONObject mess = (JSONObject) jsonObject.getJSONArray("choices").get(0);
        String content = mess.getJSONObject("message").getString("content");
        System.out.println(content);

    }

    /**
     * 截取字符串string中特定字符str1、str2之间的字符串
     *
     * @param string
     * @param str1
     * @param str2
     * @return 截取之后的字符串result（不包含str1和str2,只取中间的字符串）
     */
    public static String subString(String string, String str1, String str2) {
        //记录特定字符str1、str2索引
        int beginIndex = string.indexOf(str1);
        int endIndex = string.indexOf(str2);
        //索引<0，表示字符串中不存在字符，return
        if (beginIndex < 0) {
            return "[" + string + "]" + " 中不存在 " + str1 + "，无法截取";
        } else if (endIndex < 0) {
            return "[" + string + "]" + " 中不存在 " + str2 + "，无法截取";
        }
        //参数str1 str2位置可调换，不管str1 str2在字符串中的位置顺序如何，都能截取它们之间的字符串
        if (beginIndex > endIndex) {
            int tempIndex = beginIndex;
            beginIndex = endIndex;
            endIndex = tempIndex;
            String tempStr = str1;
            str1 = str2;
            str2 = tempStr;
        }
        //截取 第一个substring()方法包含边界字符str1或str2,为了使截取结果不包含边界，用第二个substring()方法去掉边界字符
        String result = string.substring(beginIndex, endIndex).substring(str1.length());
        return result;
    }

    private static String doPost(String url, String params) {
        try {
            URL realURL = new URL(BASE_URL + url);
            HttpURLConnection conn = (HttpURLConnection) realURL.openConnection();
            conn.setRequestMethod("POST");  //设定请求的方法为"POST"，默认是GET
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + API_KEY);
            // 设置是否向httpUrlConnection输出，因为这个是post请求，参数要放在http正文内，因此需要设为true, 默认情况下是false;
            conn.setDoOutput(true);
            // 设置是否从httpUrlConnection读入，默认情况下是true;
            conn.setDoInput(true);
            // Post请求不能使用缓存
            conn.setUseCaches(false);

            // 发送请求
            conn.getOutputStream().write(params.getBytes(StandardCharsets.UTF_8));
            // 读取响应
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            String res = response.toString();
            JSONObject jsonObject = JSON.parseObject(res);
            JSONObject mess = (JSONObject) jsonObject.getJSONArray("choices").get(0);
            return mess.getJSONObject("message").getString("content");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
