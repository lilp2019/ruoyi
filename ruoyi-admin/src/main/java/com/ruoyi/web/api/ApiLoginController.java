package com.ruoyi.web.api;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.framework.web.service.SysLoginService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录验证
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/user")
public class ApiLoginController {
    private static final Logger log = LoggerFactory.getLogger(ApiLoginController.class);

    @Autowired
    private SysLoginService loginService;
    @Autowired
    private RedisCache redisCache;

    @PostMapping("/login")
    @ApiOperation("账号密码登录")
    public AjaxResult login(@RequestBody LoginBody loginBody) {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getCode());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

//    @PostMapping("/sms/login")
//    @ApiOperation("手机号短信登录")
//    public AjaxResult smsLogin(@RequestBody SmsLoginBody loginBody) {
//        String mobile = loginBody.getMobile();
//        String smsCode = loginBody.getSmsCode();
//        String uuid = loginBody.getUuid();
//        AjaxResult ajax = AjaxResult.success();
//        // 生成令牌
//        String token = loginService.smsLogin(mobile, smsCode, uuid);
//        ajax.put(Constants.TOKEN, token);
//        return ajax;
//    }
//
//    @PostMapping("/sms/code")
//    @ApiOperation("发生手机短信验证码")
//    public AjaxResult sms(@RequestBody SmsLoginBody loginBody) {
//        String mobile = loginBody.getMobile();
//        // 保存验证码信息
//        String uuid = IdUtils.simpleUUID();
//        String verifyKey = Constants.SMS_CAPTCHA_CODE_KEY + uuid;
//
//        int code = (int) Math.ceil(Math.random() * 9000 + 1000);
//        Map<String, Object> map = new HashMap<>(16);
//        map.put("mobile", mobile);
//        map.put("code", code);
//
//        redisCache.setCacheObject(verifyKey, map, Constants.SMS_EXPIRATION, TimeUnit.MINUTES);
//        // session.setAttribute("smsCode", map);
//
//        log.info(" 为 {} 设置短信验证码：{}", mobile, code);
//        AjaxResult ajax = AjaxResult.success();
//        ajax.put("uuid", uuid);
//        return ajax;
//    }
}
