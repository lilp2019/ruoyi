package com.ruoyi.web.api;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ChatRequire;
import com.ruoyi.system.domain.ChatRequirePlan;
import com.ruoyi.system.domain.ChatRequireSummary;
import com.ruoyi.system.service.IChatRequirePlanService;
import com.ruoyi.system.service.IChatRequireService;
import com.ruoyi.system.service.IChatRequireSummaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户会话需求信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/chat/require")
public class ApiChatRequireController extends BaseController {

    @Autowired
    private IChatRequireService chatRequireService;
    @Autowired
    private IChatRequirePlanService chatRequirePlanService;
    @Autowired
    private IChatRequireSummaryService chatRequireSummaryService;

    /**
     * 获取需求详情
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getById(@PathVariable("id") Integer id) {
        ChatRequire chatRequire = chatRequireService.selectChatRequireById(id);
        return AjaxResult.success(chatRequire);
    }

    /**
     * 获取用户需求列表信息
     */
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam(value = "sessionId", required = false) String sessionId,
                           @RequestParam(value = "topicId", required = false) String topicId,
                           @RequestParam(value = "identify", required = false) String identify) {
        ChatRequire chatRequire = new ChatRequire();
        chatRequire.setUserId(hasLoginUser());
        chatRequire.setSessionId(sessionId);
        //chatRequire.setTopicId(topicId);
        chatRequire.setIdentify(identify);
        chatRequire.setStatus("0");
        List<ChatRequire> chatRequires = chatRequireService.selectChatRequireList(chatRequire);
        if (null != chatRequires && chatRequires.size() > 0) {
            for (ChatRequire require : chatRequires) {
                ChatRequirePlan chatRequirePlan = new ChatRequirePlan();
                chatRequirePlan.setRequireId(require.getId());
                List<ChatRequirePlan> plans = chatRequirePlanService.selectChatRequirePlanList(chatRequirePlan);
                require.setCountPlan(null != plans ? plans.size() : 0);
                ChatRequireSummary chatRequireSummary = new ChatRequireSummary();
                chatRequireSummary.setRequireId(require.getId());
                List<ChatRequireSummary> summaries = chatRequireSummaryService.selectChatRequireSummaryList(chatRequireSummary);
                require.setCountSummary(null != summaries ? summaries.size() : 0);
            }
        }
        return AjaxResult.success(chatRequires);
    }

    /**
     * 新增用户需求信息
     */
    @Log(title = "【用户需求】", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    public AjaxResult add(@RequestBody List<ChatRequire> chatRequires) {
        for (ChatRequire chatRequire : chatRequires) {
            chatRequire.setUserId(getUserId());
            chatRequire.setCreateBy(getUsername());
            chatRequireService.insertChatRequire(chatRequire);
        }
        return AjaxResult.success(1);
    }
}
