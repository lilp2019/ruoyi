package com.ruoyi.web.api;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.system.domain.ChatRequirePlan;
import com.ruoyi.system.domain.vo.SysUserVo;
import com.ruoyi.system.service.IChatRequirePlanService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户会话需求方案信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/chat/require/plan")
public class ApiChatRequirePlanController extends BaseController {

    @Autowired
    private IChatRequirePlanService chatRequirePlanService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 获取需求方案信息列表
     */
    @GetMapping(value = "/list")
    public AjaxResult list(@RequestParam(value = "requireId", required = false) Integer requireId) {
        ChatRequirePlan chatRequirePlan = new ChatRequirePlan();
        chatRequirePlan.setRequireId(requireId);
        chatRequirePlan.setUserId(hasLoginUser());
        List<ChatRequirePlan> chatRequirePlans = chatRequirePlanService.selectChatRequirePlanList(chatRequirePlan);
        if (null != chatRequirePlans && chatRequirePlans.size() > 0) {
            for (ChatRequirePlan plan : chatRequirePlans) {
                SysUser sysUser = sysUserService.selectUserById(plan.getUserId());
                if (null != sysUser) {
                    plan.setUser(new SysUserVo(sysUser.getUserId(), sysUser.getNickName(), sysUser.getAvatar()));
                }
                plan.setIp(IpUtils.getIpCity(plan.getIp()));
            }
        }
        return AjaxResult.success(chatRequirePlans);
    }

    /**
     * 新增【需求方案】
     */
    @Log(title = "【需求方案】", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    public AjaxResult add(@RequestBody ChatRequirePlan chatRequirePlan) {
        chatRequirePlan.setUserId(getUserId());
        chatRequirePlan.setIp(IpUtils.getIpAddr());
        chatRequirePlan.setCreateBy(getUsername());
        return AjaxResult.success(chatRequirePlanService.insertChatRequirePlan(chatRequirePlan));
    }
}
