package com.ruoyi.web.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.service.IAiSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户会话Agent窗口信息-对外API接口请求处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/session")
public class ApiAiSessionController extends BaseController {

    @Autowired
    private IAiSessionService sessionService;

}
