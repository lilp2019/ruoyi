package com.ruoyi.web.service;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.ChatUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.AiRequire;
import com.ruoyi.system.domain.AiScheme;
import com.ruoyi.system.service.IAiRequireService;
import com.ruoyi.system.service.IAiSchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ChatBank相关业务服务
 *
 * @author ruoyi
 */
@Service
public class ChatBankService {

    @Autowired
    private IAiSchemeService schemeService;
    @Autowired
    private IAiRequireService requireService;

    /**
     * 方案完善
     *
     * @param aiScheme
     * @return
     */
    @Async
    public Integer requestFillChatScheme(AiScheme aiScheme) {
        if (!aiScheme.getInitFlag().equalsIgnoreCase("0"))
            return 0;

        // 调用fast-gpt，完善方案相关信息，构建请求体
        Map<String, Object> data = new HashMap<>();
        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);
        // 组装消息参数，进行完善方案相关信息
        List<Map<String, Object>> messages = new ArrayList<>();
        messages.add(ChatUtils.message("user", aiScheme.getContent()));

        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);
        data.put("messages", messages);

        // 对于fast-gpt返回的content, 是否能转成JSON，能则解析用户的商机，保存下来，否则不管
        ChatUtils.Scheme scheme = ChatUtils.requestFillChatScheme(data);
        if (null == scheme) return 0;
        aiScheme.setTitle(scheme.getTitle());
        aiScheme.setDescription(scheme.getDescription());
        aiScheme.setTags(StringUtils.join(scheme.getTags(), ","));
        aiScheme.setInitFlag("1"); // 由初始状态转为已完成状态
        return schemeService.updateAiScheme(aiScheme);
    }

    /**
     * 商机完善
     *
     * @param aiRequire
     * @return
     */
    @Async
    public Integer requestFillChatRequire(AiRequire aiRequire) {
        if (!aiRequire.getInitFlag().equalsIgnoreCase("0"))
            return 0;

        AiScheme aiScheme = schemeService.selectAiSchemeById(aiRequire.getSchemeId());
        if (null == aiScheme) return 0;
        // 调用fast-gpt，完善方案相关信息，构建请求体
        Map<String, Object> data = new HashMap<>();
        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);
        // 组装全局变量，进行完善商机内容，主要是{{scheme}}，{{require}}两个参数
        Map<String, Object> variables = new HashMap<>();
        variables.put("scheme", aiScheme.getContent());
        variables.put("require", "背景：" + aiRequire.getBackground() + "\n目标：" + aiRequire.getTarget() + "\n要求：" + aiRequire.getRequirement() + "\n职责：" + aiRequire.getResponsibility());

        // 组装消息参数，进行完善商机内容，该参数默认即可
        List<Map<String, Object>> messages = new ArrayList<>();
        messages.add(ChatUtils.message("user", "请给出结果"));
        data.put("chatId", "");
        data.put("stream", false);
        data.put("detail", false);
        data.put("variables", variables);
        data.put("messages", messages);

        // 对于fast-gpt返回的content商机内容，保存下来
        String content = ChatUtils.requestFillChatRequire(data);
        if (StrUtil.isBlank(content)) return 0;
        aiRequire.setContent(content);
        aiRequire.setInitFlag("1"); // 由初始状态转为已完成状态
        return requireService.updateAiRequire(aiRequire);
    }
}
