package com.ruoyi.common.utils;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter dateFormatterChinese = DateTimeFormatter.ofPattern("yyyy年M月d日");
    public static final DateTimeFormatter dateFormatterMonthDay = DateTimeFormatter.ofPattern("M-d日");
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter monthFormatter = DateTimeFormatter.ofPattern("yyyy-MM");
    public static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");


    /**
     * 将给定的 Date 对象往后推指定分钟数。
     *
     * @param date   要推后的 Date 对象
     * @param minutes 要推后的分钟数
     * @return 推后后的 Date 对象
     */
    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }

    /**
     * 将 Date 对象格式化为字符串，指定日期时间格式。
     *
     * @param date   要格式化的 Date 对象
     * @param format 日期时间格式
     * @return 格式化后的字符串
     */
    public static String formatDateToString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }


    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 计算两个时间之间的分钟数
     *
     * @param date
     * @return
     */
    public static Long timeToTimeMin(Date date, Date date2) {
        return timeToTimeMin(asLocalDateTime(date), asLocalDateTime(date2));
    }

    public static Long timeToTimeMin(LocalDateTime localDateTime, LocalDateTime localDateTime2) {
        Duration duration = Duration.between(localDateTime, localDateTime2);
        return duration.toMinutes();
    }

    /**
     * 计算两个日期差
     *
     * @param localDate
     * @param localDate2
     * @return
     */
    public static Long DateDifference(LocalDate localDate, LocalDate localDate2) {
        return localDate.toEpochDay() - localDate2.toEpochDay();
    }

    // 计算两个LocalDateTime秒数差
    public static Long LocalDateTimeDifference(LocalDateTime localDateTime, LocalDateTime localDateTime2) {
        Duration duration = Duration.between(localDateTime, localDateTime2);
        return duration.getSeconds();
    }

    /**
     * 获取星期
     *
     * @param parse
     * @return
     */
    public static String getWeek(LocalDate parse) {
        int value = parse.getDayOfWeek().getValue();
        if (value == 1) return "一";
        if (value == 2) return "二";
        if (value == 3) return "三";
        if (value == 4) return "四";
        if (value == 5) return "五";
        if (value == 6) return "六";
        if (value == 7) return "日";
        return null;
    }
}
