package com.ruoyi.common.utils;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson2.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatUtils {

    static String user1 = "我有一辆商务车闲置";
    static String assistant1 = "了解您有一辆商务车处于闲置状态。为了更好地帮您处理这个问题，我需要对您的意图进行更细致的对齐。以下是我为您准备的一组问题，请您逐一回答或选择跳过：\\n\\n1. **出售意向**：您是否有意将这辆商务车出售？或者您只是在寻找其他利用方式？\\n   - 是，我想出售\\n   - 不，我想出租/租赁\\n   - 不，我想暂时存放，未来可能使用\\n   - 其他（请说明）\\n\\n2. **车辆状况**：这辆商务车目前的整体状况如何？新车、良好、一般还是需要维修？\\n   - 新车/准新车\\n   - 良好，正常使用痕迹\\n   - 一般，有部分磨损或小故障\\n   - 需要维修，存在明显问题\\n   - 不清楚\\n\\n3. **期望收益**：对于这辆商务车的处置，您有何种经济上的期待？比如期望售价、租金范围，或是希望节省的存储费用等。\\n   - 期望获得较高售价\\n   - 希望稳定获取租金收入\\n   - 主要目标是节省闲置成本，价格/租金可协商\\n   - 无特定经济期待，灵活处理\\n\\n4. **时间安排**：您希望在多长时间内完成车辆的处置或利用安排？\\n   - 尽快（一周内）\\n   - 稍缓（一至三个月）\\n   - 不急于一时（三至六个月及以上）\\n   - 无特定时间要求\\n\\n5. **额外考虑**：除了上述因素外，还有哪些因素对您来说比较重要？如交易便利性、承租人资质审核、长期合作关系建立等。\\n   - 交易流程简便快捷\\n   - 承租人/买家资质严格审核\\n   - 建立长期合作关系（如定期出租给同一公司）\\n   - 其他（请说明）\\n\\n您可以选择回答这些问题，以便我为您提供更精准的建议；如果您认为我已经充分理解了您的意图，也可以选择跳过此意图对齐环节直接获取解决方案。\\n\\n[跳过意图对齐，提供解决方案]";
    static String user2 = "我想出租，主要目标是节省闲置成本，价格可协商，希望尽快，同时承租人资质严格审核，能长期合作，信用担保以及风险由承租人承担";
    static String assistant2 = "基于您提供的信息，我理解到您希望将闲置商务车出租出去，主要目标是节省闲置成本，对租金价格持开放态度，希望尽快找到合适的承租方，并且对承租人的资质有严格要求，期望能建立长期合作关系，同时要求承租人提供信用担保并承担所有风险。接下来，我将为您提供一套符合您意图的解决方案：\\n\\n**解决方案：**\\n\\n1. **选择专业平台发布出租信息**：\\n   - 利用如凹凸租车、神州租车、携程租车等知名在线租车平台发布您的商务车出租信息。这些平台流量大、用户基数广，有助于快速匹配到潜在承租人。\\n\\n2. **详细描述车辆信息及出租条件**：\\n   - 在发布信息时，详细列出车辆品牌型号、年份、里程数、保养情况、保险状况等关键信息，以吸引合适承租人。\\n   - 明确指出您对承租人的资质要求（如需提供信用证明、企业营业执照等），以及风险承担原则（如车辆损坏、违章等责任归属承租人）。\\n\\n3. **设定合理租金范围**：\\n   - 虽然您表示价格可协商，但建议参考同类车型在市场上的平均租金水平，设定一个大致的租金范围。这样既能体现您的诚意，也能避免过低报价影响收益。\\n\\n4. **强调长期合作意愿**：\\n   - 在出租信息中明确表示欢迎长期租赁合作，可以提供一定的租金优惠作为激励，如签订一年以上租赁合同可享受租金折扣。\\n\\n5. **利用平台信用体系筛选承租人**：\\n   - 利用租车平台自带的信用评价系统和身份验证机制，优先选择信用等级高、历史评价良好的个人或企业用户。\\n\\n6. **签订正式租赁合同**：\\n   - 与承租方达成意向后，务必签订书面租赁合同，明确双方权利义务，特别是关于车辆维护、保险、事故处理、违约责任等方面的规定，确保您的权益得到法律保护。\\n\\n7. **定期跟进车辆状况**：\\n   - 即便有平台作为中介，也应定期与承租人沟通，了解车辆使用情况，及时发现并解决问题，维持良好的合作关系。\\n\\n通过上述方案，您有望在较短时间内将闲置商务车出租给符合条件的承租人，有效节省闲置成本，同时通过正规渠道保障自身权益，实现长期稳定的合作关系。如果您在实施过程中遇到任何问题，欢迎随时向我咨询。";
    static String user3 = "判断是否AI已正常提供相应的答案或解决方案。如果是否，则回答：False。如果是则提炼总结提供的相应答案或解决方案，使用JSON格式输出内容，其中包含3个字段：title、content、tags，其中content字段的内容为：提取的标准方案内容，内容为完整的Markdown格式，并在每一个内容块中判断是否需要第三方企业、其他个人或者银行协助，如需要则表明协助的内容。内容模版如下：{'title':'标题','content':'内容','tags':['主要关键词','相关关键词1','相关关键词2']}，请严格使用上述模版生成Json内容。";
    static String assistant3 = "{\"title\": \"闲置商务车出租解决方案\",\\n\"content\": \"以下是针对您闲置商务车出租需求的详细解决方案：\\n\\n### **选择专业平台发布出租信息**\\n- **操作**：在凹凸租车、神州租车、携程租车等知名在线租车平台注册并发布您的商务车出租信息。\\n- **协助方**：租车平台（如凹凸租车、神州租车、携程租车）\\n\\n### **详细描述车辆信息及出租条件**\\n- **操作**：在发布信息时，提供车辆品牌型号、年份、里程数、保养情况、保险状况等详细信息，并明确指出对承租人的资质要求（如信用证明、企业营业执照等）以及风险承担原则（车辆损坏、违章等责任归属承租人）。\\n- **协助方**：无\\n\\n### **设定合理租金范围**\\n- **操作**：参考同类车型市场平均租金水平，设定一个大致的租金范围，体现诚意的同时避免过低报价。\\n- **协助方**：市场调研（可通过网络搜索、咨询行业人士等方式获取租金参考数据）\\n\\n### **强调长期合作意愿**\\n- **操作**：在出租信息中明确提出欢迎长期租赁合作，并可提供租金优惠作为激励，如签订一年以上租赁合同可享受折扣。\\n- **协助方**：无\\n\\n### **利用平台信用体系筛选承租人**\\n- **操作**：利用租车平台的信用评价系统和身份验证机制，优先选择信用等级高、历史评价良好的个人或企业用户。\\n- **协助方**：租车平台（如凹凸租车、神州租车、携程租车）\\n\\n### **签订正式租赁合同**\\n- **操作**：与承租方达成意向后，签订书面租赁合同，明确双方权利义务，特别是关于车辆维护、保险、事故处理、违约责任等内容。\\n- **协助方**：律师或法务人员（如有必要，可寻求专业法律咨询以确保合同条款合法、完备）\\n\\n### **定期跟进车辆状况**\\n- **操作**：即使有平台作为中介，也应定期与承租人沟通，了解车辆使用情况，及时发现问题并解决，保持良好的合作关系。\\n- **协助方**：承租人（定期配合提供车辆使用情况更新）\\n\\n通过以上步骤，您将能够在较短时间内将闲置商务车出租给符合条件的承租人，有效节省闲置成本，同时通过正规渠道保障自身权益，实现长期稳定的合作关系。如在实施过程中遇到问题，可随时向我咨询。\"\\n\"tags\": [\"商务车出租\", \"闲置成本节省\", \"租金协商\", \"承租人资质审核\", \"长期合作关系\", \"信用担保\", \"风险承担\"]}\"";

    static String resp = "{\"id\":\"\",\"model\":\"\",\"usage\":{\"prompt_tokens\":1,\"completion_tokens\":1,\"total_tokens\":1},\"choices\":[{\"message\":{\"role\":\"assistant\",\"content\":\"{'title': '闲置商务车出租解决方案',\\n 'content': '''\\n# 闲置商务车出租解决方案\\n\\n## 1. 选择专业平台发布出租信息\\n- **操作**：利用凹凸租车、神州租车、携程租车等知名在线租车平台发布商务车出租信息。\\n- **协助**：无需第三方企业协助，直接在平台注册账号并按照指引发布车辆信息。\\n\\n## 2. 详细描述车辆信息及出租条件\\n- **操作**：\\n  - 列出车辆品牌型号、年份、里程数、保养情况、保险状况等关键信息。\\n  - 明确承租人资质要求（信用证明、企业营业执照等）及风险承担原则（车辆损坏、违章责任归属承租人）。\\n- **协助**：无需第三方协助，自行整理并准确描述相关信息。\\n\\n## 3. 设定合理租金范围\\n- **操作**：参考同类车型市场平均租金，设定租金范围，体现诚意且避免低价影响收益。\\n- **协助**：可能需要查阅租车平台或其他行业报告获取市场租金数据。\\n\\n## 4. 强调长期合作意愿\\n- **操作**：在出租信息中声明欢迎长期租赁合作，提供租金折扣作为签订一年以上租赁合同的激励。\\n- **协助**：无需第三方协助，自行在信息发布时注明合作优惠政策。\\n\\n## 5. 利用平台信用体系筛选承租人\\n- **操作**：利用租车平台信用评价系统和身份验证机制，优先选择信用等级高、历史评价良好的个人或企业用户。\\n- **协助**：由租车平台自动完成信用筛选，无需额外第三方协助。\\n\\n## 6. 签订正式租赁合同\\n- **操作**：与承租方达成意向后，签订书面租赁合同，明确双方权利义务，特别是车辆维护、保险、事故处理、违约责任等内容。\\n- **协助**：可能需要律师或专业机构协助起草或审查租赁合同，确保条款合法合规。\\n\\n## 7. 定期跟进车辆状况\\n- **操作**：定期与承租人沟通，了解车辆使用情况，及时发现问题并解决，保持良好合作关系。\\n- **协助**：无需第三方协助，自行与承租人进行沟通。\\n\\n''',\\n 'tags': ['商务车出租', '闲置成本节省', '承租人资质审核', '长期合作', '信用担保', '风险承担'] }\"},\"finish_reason\":\"stop\",\"index\":0}]}";

    static String title = "养牛产业平台，寻求合作银行";
    static String content = "针对养牛企业和农户的批量贷款业务，由上游养牛企业发起。\n" +
            "\n" +
            "预计单一项目的资金需求规模可达数十亿/年/省。\n" +
            "\n" +
            "`吉林吉牧` 是吉林省“百万头牛计划”重点扶持的中大型牛企，意欲开展全国性布局，五年内达成IPO。\n" +
            "\n" +
            "但吉牧不愿占用银行现有的授信额度，且向外地拓展业务需要较高的管理成本。\n" +
            "\n" +
            "### 搜索\n" +
            "\n" +
            "[农行吉林省分行首笔2100万元，“吉牧e贷”活牛抵押贷款成功发放](https://www.farmer.com.cn/2021/03/30/wap_99867971.html?_blank)\n" +
            "\n" +
            "<a href=\"https://www.farmer.com.cn/2021/03/30/wap_99867971.html\" target=\"_blank\">\n" +
            "农行吉林省分行首笔2100万元，“吉牧e贷”活牛抵押贷款成功发放</a>\n";
    static String tags = "养牛,贷款,授信额度,寻求合作";

    public static void main(String[] args) {
        Map<String, Object> body = new HashMap<>();
        body.put("chatId", "");
        body.put("stream", false);
        body.put("detail", false);
        List<Map<String, Object>> messages = new ArrayList<>();
        messages.add(message("user", "建立海外独立站"));
        messages.add(message("assistant", "了解您的需求，为了更好地帮助您建立海外独立站，我将通过几个关键问题来对齐我们的意图。这将帮助我为您提供一个精准且详细的解决方案。当然，如果您已经很清楚自己的需求，随时可以选择跳过这一步。\\n\\n1. **目标市场定位**：您计划面向哪个国家或地区的客户群建立独立站？是否有特定的受众群体？\\n2. **产品与服务**：您的独立站主要销售哪些类型的产品或服务？请简要描述。\\n3. **功能需求**：您期望独立站具备哪些核心功能？例如，多语言支持、在线支付系统、SEO优化、社交媒体集成等。\\n4. **预算与时间框架**：您为这个项目设定的预算是多少？以及您期望在多久时间内完成独立站的搭建？\\n\\n当然，如果您已准备好直接进入解决方案阶段，只需告诉我“跳过对齐”，我们将直接开始规划您的海外独立站建设方案。"));
        messages.add(message("user", "手工艺品，支持PayPal的平台，预算$5k以内，需要社交媒体"));
        messages.add(message("assistant", "了解了，基于您提供的信息，我为您打造了以下专属需求解决方案：\n" +
                "\n" +
                "## 需求概览\n" +
                "您希望创建一个面向全球市场的海外独立站，专注于销售手工艺品。该网站需集成PayPal作为支付手段，并且要与社交媒体平台紧密集成，以便于推广和用户互动。您的预算控制在5000美元以内，并期待一个高效且具有吸引力的在线商城。\n" +
                "\n" +
                "## 解决方案步骤\n" +
                "\n" +
                "### 1. 平台选择与设置\n" +
                "**目标**：选择适合手工艺品展示与销售的电商平台解决方案。\n" +
                "**要求**：\n" +
                "- 采用Shopify或WooCommerce等成熟平台，便于快速搭建且支持多语言。\n" +
                "- 集成PayPal作为支付网关，确保安全便捷的交易体验。\n" +
                "\n" +
                "### 2. 网站设计与优化\n" +
                "**目标**：设计吸引目标市场的网站界面，优化用户体验。\n" +
                "**要求**：\n" +
                "- 选择或定制符合手工艺品特色的主题模板。\n" +
                "- 确保网站响应式设计，适应不同设备访问。\n" +
                "- 进行基础SEO设置，提高搜索引擎可见度。\n" +
                "\n" +
                "### 3. 社交媒体集成与营销\n" +
                "**目标**：利用社交媒体增强品牌影响力，促进用户互动与销售。\n" +
                "**要求**：\n" +
                "- 在网站上集成Facebook、Instagram等社交媒体插件，方便用户分享和关注。\n" +
                "- 定期发布手工艺品制作过程、故事等内容至社交媒体，增加用户粘性。\n" +
                "- 考虑使用社交媒体广告，初期小规模测试效果。\n" +
                "\n" +
                "### 4. 物流与客户服务\n" +
                "**目标**：建立顺畅的物流解决方案和客户服务机制。\n" +
                "**要求**：\n" +
                "- 选择合适的国际物流合作伙伴，如DHL、FedEx，提供多种运输选项。\n" +
                "- 设置FAQ页面和在线客服系统，确保及时响应顾客咨询。\n" +
                "\n" +
                "### 5. 测试与上线\n" +
                "**目标**：全面测试网站功能，确保无误后正式上线。\n" +
                "**要求**：\n" +
                "- 进行跨浏览器兼容性测试，确保网站在各平台正常运行。\n" +
                "- 内部测试购物流程，包括支付、订单处理等环节。\n" +
                "- 正式上线前进行一次全面的用户体验审查。\n" +
                "\n" +
                "## 时间与预算\n" +
                "- **时间框架**：预计从项目启动到网站正式上线需2-3个月，具体取决于定制化需求的复杂度。\n" +
                "- **预算分配**：考虑到您的预算为5000美元，建议如下分配：平台搭建与设计（$2000），社交媒体营销初期投入（$500），物流解决方案准备（$500），剩余资金用于支付网关费用、域名购买、SSL证书、测试及后期维护等。\n" +
                "\n" +
                "此方案旨在高效、经济地满足您的需求，如有任何调整意见或进一步的细节讨论，请随时告知。"));
        messages.add(message("user", "社交媒体需要国内社交平台"));
        messages.add(message("assistant", "了解您的补充需求，针对国内社交平台的集成，我们将对解决方案进行相应的调整：\n" +
                "\n" +
                "### 3. 社交媒体集成与营销（更新）\n" +
                "**目标**：结合国内外市场，利用社交媒体增强品牌影响力，促进用户互动与销售。\n" +
                "**要求**：\n" +
                "- **国内平台集成**：集成微信公众号、小程序以及微博等国内主流社交平台，便于国内用户关注、分享和直接购买。\n" +
                "- **内容本地化**：针对国内社交平台特性，制定符合本土文化的手工艺品故事、教程等内容策略。\n" +
                "- **互动与社群建设**：在各平台上定期举办互动活动，如手工艺教学直播、作品展示竞赛等，增强用户参与感和品牌忠诚度。\n" +
                "- **营销工具利用**：利用微信广告、微博粉丝通等国内社交平台的广告系统，进行精准营销推广。\n" +
                "\n" +
                "请确认以上调整是否符合您的预期。如果有其他特定的国内社交平台需求或详细要求，请进一步说明，以便我们提供更加精准的支持。"));
        body.put("messages", messages);
        String content = requestChatScheme(body);
        System.out.println("content:" + content);
        // content = extractJsonFromMarkdown(content);  // 返回的是json格式数据才需要进行格式处理
    }

    /**
     * 根据chatMessage产生方案
     *
     * @param body
     * @return
     */
    public static String requestChatScheme(Map<String, Object> body) {
        String resp = HttpRequest.post("http://120.77.181.233:3020/api/v1/chat/completions")
                .header("Authorization", "Bearer fastgpt-Ln0nTMHvTE6QgNcGi4dvBMwn6lZA3oANqMnjk1eEWSaihlxvntSBHcVn")
                .body(JSON.toJSONString(body))
                .execute().body();
        System.out.println("RequestChatScheme resp:" + resp);
        try {
            ChatMessage jsonResp = JSON.parseObject(resp, ChatMessage.class);
            return jsonResp.getChoices().get(0).getMessage().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 根据方案，产生商机信息
     *
     * @param body
     * @return
     */
    public static Require requestChatRequire(Map<String, Object> body) {
        String resp = HttpRequest.post("http://120.77.181.233:3020/api/v1/chat/completions")
                .header("Authorization", "Bearer fastgpt-0ymQ7vCpbq5FGV9MfGbGDccdh9JaHe0rqEIXrE3tRkujHajUXg0toFcKC")
                .body(JSON.toJSONString(body))
                .execute().body();
        System.out.println("RequestChatRequire resp:" + resp);
        try {
            ChatMessage jsonResp = JSON.parseObject(resp, ChatMessage.class);
            String content = jsonResp.getChoices().get(0).getMessage().getContent();
            if (StringUtils.isBlank(content)) return null;
            content = extractJsonFromMarkdown(content);
            return JSON.parseObject(content, Require.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据初始化状态的方案，组装content，进行完善方案信息，返回方案title，description，tags
     *
     * @param body
     * @return
     */
    public static Scheme requestFillChatScheme(Map<String, Object> body) {
        String resp = HttpRequest.post("http://120.77.181.233:3020/api/v1/chat/completions")
                .header("Authorization", "Bearer fastgpt-d2hQYnMorznd9Ybpye40LXZF0tfKDuQn1sOqCc3zf1Tcp3KmD3uKD")
                .body(JSON.toJSONString(body))
                .execute().body();
        System.out.println("RequestFillChatScheme resp:" + resp);
        try {
            ChatMessage jsonResp = JSON.parseObject(resp, ChatMessage.class);
            String content = jsonResp.getChoices().get(0).getMessage().getContent();
            if (StringUtils.isBlank(content)) return null;
            content = extractJsonFromMarkdown(content);
            return JSON.parseObject(content, Scheme.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据初始化状态的商机，组装scheme, require全局参数，进行完善商机信息，返回方案内容
     *
     * @param body
     * @return
     */
    public static String requestFillChatRequire(Map<String, Object> body) {
        String resp = HttpRequest.post("http://120.77.181.233:3020/api/v1/chat/completions")
                .header("Authorization", "Bearer fastgpt-bL1xmJhq6gZqwCqFFGvblumBZHveQ0AqJ07ohlGPyDu7clieNm47Of70OZdpC3XQf")
                .body(JSON.toJSONString(body))
                .execute().body();
        System.out.println("RequestFillChatRequire resp:" + resp);
        try {
            ChatMessage jsonResp = JSON.parseObject(resp, ChatMessage.class);
            return jsonResp.getChoices().get(0).getMessage().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<String, Object> message(String role, String content) {
        Map<String, Object> map = new HashMap<>();
        map.put("role", role);
        map.put("content", content);
        return map;
    }

    /**
     * 从Markdown文档中获取对应的Json内容。可以直接解析出来Json完整内容进行后续操作
     *
     * @param markdownContent Markdown文档内容
     */
    private static String extractJsonFromMarkdown(String markdownContent) {
        // Regular expression to match JSON block within Markdown
        Pattern pattern = Pattern.compile("```json\\s*(.*?)```", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(markdownContent);
        String content = null;
        while (matcher.find()) {
            String jsonBlock = matcher.group(1);
            System.out.println("Found JSON block:");
            System.out.println(jsonBlock);
            System.out.println("------------------------");
            content = jsonBlock;
        }
        return content;
    }

    public static class ChatMessage {
        private String id;
        private String model;
        private List<Choice> choices;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public List<Choice> getChoices() {
            return choices;
        }

        public void setChoices(List<Choice> choices) {
            this.choices = choices;
        }
    }

    public static class Choice {
        private Integer index;
        private Message message;

        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }

        public Message getMessage() {
            return message;
        }

        public void setMessage(Message message) {
            this.message = message;
        }
    }

    public static class Message {
        String role;
        String content;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class Content {
        private String title;
        private String content;
        private List<String> tags;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }
    }

    public static class Scheme {
        private String title;
        private String description;
        private List<String> tags;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getTags() {
            return tags;
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }
    }

    public static class Require {
        private String background;
        private List<RequireDetail> children;

        public String getBackground() {
            return background;
        }

        public void setBackground(String background) {
            this.background = background;
        }

        public List<RequireDetail> getChildren() {
            return children;
        }

        public void setChildren(List<RequireDetail> children) {
            this.children = children;
        }
    }

    public static class RequireDetail {
        private String title;
        private String target;
        private String req;
        private String resp;
        private String tags;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public String getReq() {
            return req;
        }

        public void setReq(String req) {
            this.req = req;
        }

        public String getResp() {
            return resp;
        }

        public void setResp(String resp) {
            this.resp = resp;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }
    }
}
