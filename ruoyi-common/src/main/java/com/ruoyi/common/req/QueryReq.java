package com.ruoyi.common.req;

/**
 * 分页查询基础信息
 */
public class QueryReq extends BaseReq {

    /**
     * 当前页，默认为1
     */
    private long current = 1L;
    /**
     * 页面大小，默认为10
     */
    private long size = 10L;

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = 10;
    }
}
