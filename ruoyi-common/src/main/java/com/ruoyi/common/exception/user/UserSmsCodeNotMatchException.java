package com.ruoyi.common.exception.user;

/**
 * 用户手机验证码不正确或不符合规范异常类
 * 
 * @author ruoyi
 */
public class UserSmsCodeNotMatchException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserSmsCodeNotMatchException()
    {
        super("user.sms.code.not.match", null);
    }
}
