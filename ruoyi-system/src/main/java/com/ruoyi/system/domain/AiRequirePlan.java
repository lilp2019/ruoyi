package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户商机意向合作信息对象 ai_require_plan
 *
 * @author ruoyi
 * @date 2024-05-10
 */
public class AiRequirePlan extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 商机ID
     */
    @Excel(name = "商机ID")
    private Long requireId;

    /**
     * 商机标题
     */
    private String requireTitle;

    /**
     * 会话标识ID
     */
    @Excel(name = "会话标识ID")
    private String sessionId;

    /**
     * 主题ID
     */
    @Excel(name = "主题ID")
    private String topicId;

    /**
     * 合作内容
     */
    @Excel(name = "合作内容")
    private String content;

    /**
     * 回复内容ID
     */
    @Excel(name = "回复内容ID")
    private Long replyId;

    /**
     * 回复用户ID
     */
    @Excel(name = "回复用户ID")
    private Long replyUserId;

    /**
     * 商机标签，多个用,隔开
     */
    @Excel(name = "商机标签")
    private String tags;

    /**
     * 状态（0正常 1-停用）
     */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setRequireId(Long requireId) {
        this.requireId = requireId;
    }

    public Long getRequireId() {
        return requireId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRequireTitle() {
        return requireTitle;
    }

    public void setRequireTitle(String requireTitle) {
        this.requireTitle = requireTitle;
    }

    public String getContent() {
        return content;
    }

    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    public Long getReplyId() {
        return replyId;
    }

    public void setReplyUserId(Long replyUserId) {
        this.replyUserId = replyUserId;
    }

    public Long getReplyUserId() {
        return replyUserId;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("requireId", getRequireId())
                .append("sessionId", getSessionId())
                .append("topicId", getTopicId())
                .append("content", getContent())
                .append("replyId", getReplyId())
                .append("replyUserId", getReplyUserId())
                .append("tags", getTags())
                .append("status", getStatus())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .toString();
    }
}
