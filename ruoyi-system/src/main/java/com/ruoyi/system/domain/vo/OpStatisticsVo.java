package com.ruoyi.system.domain.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 运营端数据统计信息
 *
 * @author ruoyi
 */
public class OpStatisticsVo {
    /**
     * 访客数据
     */
    private Integer userLoginData = 0;
    /**
     * 会话消息数据
     */
    private Integer chatMessageData = 0;
    /**
     * 案例数据
     */
    private Integer chatCaseData = 0;
    /**
     * 商机数据
     */
    private Integer chatRequireData = 0;
    /**
     * 意向合作数据
     */
    private Integer chatRequirePlanData = 0;
    /**
     * 合作方案数据
     */
    private Integer chatRequireSummaryData = 0;

    private List<Integer> userLoginList = new ArrayList<>();
    private List<Integer> chatMessageList = new ArrayList<>();
    private List<Integer> chatCaseList = new ArrayList<>();
    private List<Integer> chatRequireList = new ArrayList<>();
    private List<Integer> chatRequirePlanList = new ArrayList<>();
    private List<Integer> chatRequireSummaryList = new ArrayList<>();

    public Integer getUserLoginData() {
        return userLoginData;
    }

    public void setUserLoginData(Integer userLoginData) {
        this.userLoginData = userLoginData;
    }

    public Integer getChatMessageData() {
        return chatMessageData;
    }

    public void setChatMessageData(Integer chatMessageData) {
        this.chatMessageData = chatMessageData;
    }

    public Integer getChatCaseData() {
        return chatCaseData;
    }

    public void setChatCaseData(Integer chatCaseData) {
        this.chatCaseData = chatCaseData;
    }

    public Integer getChatRequireData() {
        return chatRequireData;
    }

    public void setChatRequireData(Integer chatRequireData) {
        this.chatRequireData = chatRequireData;
    }

    public Integer getChatRequirePlanData() {
        return chatRequirePlanData;
    }

    public void setChatRequirePlanData(Integer chatRequirePlanData) {
        this.chatRequirePlanData = chatRequirePlanData;
    }

    public Integer getChatRequireSummaryData() {
        return chatRequireSummaryData;
    }

    public void setChatRequireSummaryData(Integer chatRequireSummaryData) {
        this.chatRequireSummaryData = chatRequireSummaryData;
    }

    public List<Integer> getUserLoginList() {
        return userLoginList;
    }

    public void setUserLoginList(List<Integer> userLoginList) {
        this.userLoginList = userLoginList;
    }

    public List<Integer> getChatMessageList() {
        return chatMessageList;
    }

    public void setChatMessageList(List<Integer> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }

    public List<Integer> getChatCaseList() {
        return chatCaseList;
    }

    public void setChatCaseList(List<Integer> chatCaseList) {
        this.chatCaseList = chatCaseList;
    }

    public List<Integer> getChatRequireList() {
        return chatRequireList;
    }

    public void setChatRequireList(List<Integer> chatRequireList) {
        this.chatRequireList = chatRequireList;
    }

    public List<Integer> getChatRequirePlanList() {
        return chatRequirePlanList;
    }

    public void setChatRequirePlanList(List<Integer> chatRequirePlanList) {
        this.chatRequirePlanList = chatRequirePlanList;
    }

    public List<Integer> getChatRequireSummaryList() {
        return chatRequireSummaryList;
    }

    public void setChatRequireSummaryList(List<Integer> chatRequireSummaryList) {
        this.chatRequireSummaryList = chatRequireSummaryList;
    }
}
