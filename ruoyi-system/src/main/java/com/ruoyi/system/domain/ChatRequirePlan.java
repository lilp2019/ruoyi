package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.vo.SysUserVo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 需求方案信息对象 chat_require_plan
 *
 * @author ruoyi
 * @date 2024-04-06
 */
public class ChatRequirePlan extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 需求ID
     */
    @Excel(name = "需求ID")
    private Integer requireId;

    private String requireTitle;

    /**
     * 方案提供方用户ID
     */
    @Excel(name = "方案提供方用户ID")
    private Long userId;

    private String userName;

    private SysUserVo user;

    /**
     * 方案内容
     */
    @Excel(name = "方案内容")
    private String content;

    /**
     * 标签信息
     */
    private String tags;

    /**
     * 客户端IP地址
     */
    private String ip;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setRequireId(Integer requireId) {
        this.requireId = requireId;
    }

    public Integer getRequireId() {
        return requireId;
    }

    public String getRequireTitle() {
        return requireTitle;
    }

    public void setRequireTitle(String requireTitle) {
        this.requireTitle = requireTitle;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public SysUserVo getUser() {
        return user;
    }

    public void setUser(SysUserVo user) {
        this.user = user;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("requireId", getRequireId())
                .append("userId", getUserId())
                .append("content", getContent())
                .append("tags", getTags())
                .append("ip", getIp())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
