package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 订单信息对象 ai_order
 *
 * @author ruoyi
 * @date 2024-05-07
 */
public class AiOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 订单号
     */
    @Excel(name = "订单号")
    private String orderNo;

    /**
     * 订单类型
     */
    @Excel(name = "订单类型")
    private String orderType;

    /**
     * 下单用户
     */
    @Excel(name = "下单用户")
    private Long userId;

    /**
     * 商品描述信息
     */
    @Excel(name = "商品描述信息")
    private String body;

    /**
     * 订单金额，单位分
     */
    @Excel(name = "订单金额，单位分")
    private Integer orderAmount;

    /**
     * 订单状态: 0-待支付, 1-已支付, 2-已取消
     */
    @Excel(name = "订单状态: 0-待支付, 1-已支付, 2-已取消")
    private Integer status;

    /**
     * 支付金额,单位分
     */
    @Excel(name = "支付金额,单位分")
    private Integer payAmount;

    /**
     * 订单支付时间
     */
    @Excel(name = "订单支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /**
     * 支付方式
     */
    @Excel(name = "支付方式")
    private String payType;

    /**
     * 第三方交易单号-支付完成返回的交易单号
     */
    @Excel(name = "第三方交易单号-支付完成返回的交易单号")
    private String tradeNo;

    /**
     * 订单取消时间
     */
    @Excel(name = "订单取消时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cancelTime;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayType() {
        return payType;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("orderId", getOrderId())
                .append("orderNo", getOrderNo())
                .append("orderType", getOrderType())
                .append("userId", getUserId())
                .append("body", getBody())
                .append("orderAmount", getOrderAmount())
                .append("status", getStatus())
                .append("payAmount", getPayAmount())
                .append("payTime", getPayTime())
                .append("payType", getPayType())
                .append("tradeNo", getTradeNo())
                .append("cancelTime", getCancelTime())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .toString();
    }
}
