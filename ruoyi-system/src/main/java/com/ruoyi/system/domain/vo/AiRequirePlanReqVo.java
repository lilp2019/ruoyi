package com.ruoyi.system.domain.vo;

/**
 * 用户商机意向合作信息对象 ai_require_plan
 *
 * @author ruoyi
 * @date 2024-05-10
 */
public class AiRequirePlanReqVo {

    /**
     * 商机ID
     */
    private Long requireId;

    /**
     * 合作内容
     */
    private String content;

    /**
     * 回复内容ID
     */
    private Long replyId;

    /**
     * 商机标签，多个用,隔开
     */
    private String tags;

    public Long getRequireId() {
        return requireId;
    }

    public void setRequireId(Long requireId) {
        this.requireId = requireId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getReplyId() {
        return replyId;
    }

    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
