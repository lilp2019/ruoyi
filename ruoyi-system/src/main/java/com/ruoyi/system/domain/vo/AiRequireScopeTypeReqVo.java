package com.ruoyi.system.domain.vo;

/**
 * 用户修改需求商机查看范围
 *
 * @author ruoyi
 * @date 2024-05-05
 */
public class AiRequireScopeTypeReqVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 范围查看类型：0-公有，1-私有，2-不开放
     */
    private String scopeType;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
    }
}
