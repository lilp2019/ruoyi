package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户方案信息对象 ai_scheme
 *
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiScheme extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * sessionId：用户对话Agent所属窗口ID
     */
    private String sessionId;

    /**
     * 主题ID
     */
    private String topicId;

    /**
     * 方案推荐标识，通过类型区分（如：系统推荐，AI推荐，人气最高等）
     */
    private String type;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 方案标题
     */
    @Excel(name = "方案标题")
    private String title;

    /**
     * 方案简介
     */
    @Excel(name = "方案简介")
    private String description;

    /**
     * 方案内容
     */
    @Excel(name = "方案内容")
    private String content;

    /**
     * 方案标签
     */
    @Excel(name = "方案标签")
    private String tags;

    /**
     * 状态（1-进行中 2-已完成 3-已失败）
     */
    @Excel(name = "状态", readConverterExp = "1-进行中 2-已完成 3-已失败")
    private String status;

    /**
     * 分解后节点总数
     */
    @Excel(name = "分解后节点总数")
    private Integer totalStepCount;

    /**
     * 进行中节点数
     */
    @Excel(name = "进行中节点数")
    private Integer processCount;

    /**
     * 已完成节点数
     */
    @Excel(name = "已完成节点数")
    private Integer finishedCount;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sort;

    /**
     * 方案初始化标识
     */
    @Excel(name = "方案初始化标识", readConverterExp = "0=初始化,1=正常")
    private String initFlag;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTotalStepCount(Integer totalStepCount) {
        this.totalStepCount = totalStepCount;
    }

    public Integer getTotalStepCount() {
        return totalStepCount;
    }

    public void setProcessCount(Integer processCount) {
        this.processCount = processCount;
    }

    public Integer getProcessCount() {
        return processCount;
    }

    public void setFinishedCount(Integer finishedCount) {
        this.finishedCount = finishedCount;
    }

    public Integer getFinishedCount() {
        return finishedCount;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public String getInitFlag() {
        return initFlag;
    }

    public void setInitFlag(String initFlag) {
        this.initFlag = initFlag;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("sessionId", getSessionId())
                .append("topicId", getTopicId())
                .append("type", getType())
                .append("title", getTitle())
                .append("description", getDescription())
                .append("content", getContent())
                .append("tags", getTags())
                .append("status", getStatus())
                .append("totalStepCount", getTotalStepCount())
                .append("processCount", getProcessCount())
                .append("finishedCount", getFinishedCount())
                .append("sort", getSort())
                .append("initFlag", getInitFlag())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .toString();
    }
}
