package com.ruoyi.system.domain.vo;

import java.util.Date;

/**
 * 用户需求商机信息对象 ai_require
 *
 * @author ruoyi
 * @date 2024-05-05
 */
public class AiRequireVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户信息
     */
    private SysUserVo user;

    /**
     * 方案ID
     */
    private Long schemeId;

    /**
     * 会话标识ID
     */
    private String sessionId;

    /**
     * 主题ID
     */
    private String topicId;

    /**
     * 商机标题
     */
    private String title;

    /**
     * 商机背景
     */
    private String background;

    /**
     * 商机目标
     */
    private String target;

    /**
     * 商机要求
     */
    private String requirement;

    /**
     * 商机职责
     */
    private String responsibility;

    /**
     * 商机内容
     */
    private String content;

    /**
     * 商机标签，多个用,隔开
     */
    private String tags;

    /**
     * 商机是否是初始化标识（0初始化 1正常）
     */
    private String initFlag;

    /**
     * 范围查看类型：0-公有，1-私有，2-不开放
     */
    private String scopeType;

    /**
     * 分享次数
     */
    private Integer shareCount = 0;

    /**
     * 合作意向数
     */
    private Integer planCount = 0;

    /**
     * 查看数
     */
    private Integer viewCount = 0;

    /**
     * 收藏数
     */
    private Integer collectionCount = 0;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public SysUserVo getUser() {
        return user;
    }

    public void setUser(SysUserVo user) {
        this.user = user;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
    }

    public String getInitFlag() {
        return initFlag;
    }

    public void setInitFlag(String initFlag) {
        this.initFlag = initFlag;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    public Integer getPlanCount() {
        return planCount;
    }

    public void setPlanCount(Integer planCount) {
        this.planCount = planCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getCollectionCount() {
        return collectionCount;
    }

    public void setCollectionCount(Integer collectionCount) {
        this.collectionCount = collectionCount;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

}
