package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户需求商机信息对象 ai_require
 *
 * @author ruoyi
 * @date 2024-05-05
 */
public class AiRequire extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    private String openId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 方案ID
     */
    @Excel(name = "方案ID")
    private Long schemeId;

    /**
     * 会话标识ID
     */
    @Excel(name = "会话标识ID")
    private String sessionId;

    /**
     * 主题ID
     */
    @Excel(name = "主题ID")
    private String topicId;

    /**
     * 商机标题
     */
    @Excel(name = "商机标题")
    private String title;

    /**
     * 商机背景
     */
    @Excel(name = "商机背景")
    private String background;

    /**
     * 商机目标
     */
    @Excel(name = "商机目标")
    private String target;

    /**
     * 商机要求
     */
    @Excel(name = "商机要求")
    private String requirement;

    /**
     * 商机职责
     */
    @Excel(name = "商机职责")
    private String responsibility;

    /**
     * 商机内容
     */
    @Excel(name = "商机内容")
    private String content;

    /**
     * 商机标签，多个用,隔开
     */
    @Excel(name = "商机标签，多个用,隔开")
    private String tags;

    /**
     * 商机状态（0正常 1停用）
     */
    @Excel(name = "商机状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 商机是否是初始化标识（0初始化 1正常）
     */
    @Excel(name = "商机初始化标识", readConverterExp = "0=初始化,1=正常")
    private String initFlag;

    /**
     * 范围查看类型：0-公有，1-私有，2-不开放
     */
    private String scopeType;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sort;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
    }

    public String getInitFlag() {
        return initFlag;
    }

    public void setInitFlag(String initFlag) {
        this.initFlag = initFlag;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("schemeId", getSchemeId())
                .append("sessionId", getSessionId())
                .append("topicId", getTopicId())
                .append("title", getTitle())
                .append("background", getBackground())
                .append("target", getTarget())
                .append("requirement", getRequirement())
                .append("responsibility", getResponsibility())
                .append("content", getContent())
                .append("tags", getTags())
                .append("status", getStatus())
                .append("scopeType", getScopeType())
                .append("initFlag", getInitFlag())
                .append("sort", getSort())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .toString();
    }
}
