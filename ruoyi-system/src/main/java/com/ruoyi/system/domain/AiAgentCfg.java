package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 不同类型对话Agent配置信息对象 ai_agent_cfg
 *
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiAgentCfg extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * Agent类型code：scheme，require，idea等
     */
    @Excel(name = "Agent类型code：scheme，require，idea等")
    private String type;

    /**
     * Agent类型名称：方案，商机，想法等
     */
    @Excel(name = "Agent类型名称：方案，商机，想法等")
    private String name;

    /**
     * Agent对话聊天apiKey
     */
    @Excel(name = "Agent对话聊天apiKey")
    private String apiKey;

    /**
     * Agent模型，可选
     */
    @Excel(name = "Agent模型，可选")
    private String model;

    /**
     * 模型提供厂商，如OpenAI等
     */
    @Excel(name = "模型提供厂商，如OpenAI等")
    private String provider;

    /**
     * 系统Prompt，预留字段
     */
    @Excel(name = "系统Prompt，预留字段")
    private String systemRole;

    /**
     * 系统endpoint，预留字段
     */
    @Excel(name = "系统endpoint，预留字段")
    private String endpoint;

    /**
     * 状态（0正常 1停用）
     */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }

    public String getSystemRole() {
        return systemRole;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("type", getType())
                .append("name", getName())
                .append("apiKey", getApiKey())
                .append("model", getModel())
                .append("provider", getProvider())
                .append("systemRole", getSystemRole())
                .append("endpoint", getEndpoint())
                .append("status", getStatus())
                .append("createdAt", getCreatedAt())
                .append("updatedAt", getUpdatedAt())
                .toString();
    }
}
