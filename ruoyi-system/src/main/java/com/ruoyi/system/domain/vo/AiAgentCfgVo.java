package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 不同类型对话Agent配置信息对象 ai_agent_cfg
 *
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiAgentCfgVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * Agent类型code：scheme，require，idea等
     */
    private String type;

    /**
     * Agent类型名称：方案，商机，想法等
     */
    private String name;

    /**
     * Agent对话聊天apiKey
     */
    private String apiKey;

    /**
     * Agent模型，可选
     */
    private String model;

    /**
     * 模型提供厂商，如OpenAI等
     */
    private String provider;

    /**
     * 系统Prompt，预留字段
     */
    private String systemRole;

    /**
     * 系统endpoint，预留字段
     */
    private String endpoint;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }

    public String getSystemRole() {
        return systemRole;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
