package com.ruoyi.system.domain.vo;

public class AiSchemeReqVo {

    /**
     * 方案ID
     */
    private Long schemeId;

    public Long getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }
}
