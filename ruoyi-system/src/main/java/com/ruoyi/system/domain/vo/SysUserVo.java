package com.ruoyi.system.domain.vo;

public class SysUserVo {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * openId
     */
    private String openId;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phonenumber;

    /**
     * 用户性别
     */
    private String sex;

    /**
     * 企业保证金标识（0-没有缴纳保证金，1-缴纳了保证金）
     */
    private String companyMarginFlag;

    /**
     * 企业授权标识（0-未认证，1-等待审核，2-认证成功，3-认证失败）
     */
    private String companyAuthFlag;

    /**
     * 企业名称
     */
    private String companyName;

    /**
     * 企业法人
     */
    private String companyLeader;

    /**
     * 公司信用编码
     */
    private String companyCode;

    public SysUserVo() {
    }

    public SysUserVo(Long userId, String userName, String avatar) {
        this.userId = userId;
        this.userName = userName;
        this.avatar = avatar;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCompanyMarginFlag() {
        return companyMarginFlag;
    }

    public void setCompanyMarginFlag(String companyMarginFlag) {
        this.companyMarginFlag = companyMarginFlag;
    }

    public String getCompanyAuthFlag() {
        return companyAuthFlag;
    }

    public void setCompanyAuthFlag(String companyAuthFlag) {
        this.companyAuthFlag = companyAuthFlag;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLeader() {
        return companyLeader;
    }

    public void setCompanyLeader(String companyLeader) {
        this.companyLeader = companyLeader;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
