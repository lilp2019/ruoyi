package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户对话Agent配置信息对象 ai_agent
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiAgent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** sessionId：用户对话Agent所属窗口ID */
    private String id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** Agent类型：方案，商机，想法等 */
    @Excel(name = "Agent类型：方案，商机，想法等")
    private String type;

    /** 不同类型关联ID */
    @Excel(name = "不同类型关联ID")
    private String relationId;

    /** Agent对话聊天apiKey */
    @Excel(name = "Agent对话聊天apiKey")
    private String apiKey;

    /** Agent模型，可选 */
    @Excel(name = "Agent模型，可选")
    private String model;

    /** 模型提供厂商，如OpenAI等 */
    @Excel(name = "模型提供厂商，如OpenAI等")
    private String provider;

    /** 系统Prompt，预留字段 */
    @Excel(name = "系统Prompt，预留字段")
    private String systemRole;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setRelationId(String relationId) 
    {
        this.relationId = relationId;
    }

    public String getRelationId() 
    {
        return relationId;
    }
    public void setApiKey(String apiKey) 
    {
        this.apiKey = apiKey;
    }

    public String getApiKey() 
    {
        return apiKey;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setProvider(String provider) 
    {
        this.provider = provider;
    }

    public String getProvider() 
    {
        return provider;
    }
    public void setSystemRole(String systemRole) 
    {
        this.systemRole = systemRole;
    }

    public String getSystemRole() 
    {
        return systemRole;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("type", getType())
            .append("relationId", getRelationId())
            .append("apiKey", getApiKey())
            .append("model", getModel())
            .append("provider", getProvider())
            .append("systemRole", getSystemRole())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
