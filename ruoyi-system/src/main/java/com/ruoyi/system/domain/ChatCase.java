package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 案例信息对象 chat_case
 *
 * @author ruoyi
 * @date 2024-04-08
 */
public class ChatCase extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 案例编号
     */
    private String identify;

    /**
     * 案例发起银行
     */
    @Excel(name = "案例发起银行")
    private String author;

    /**
     * 案例图标
     */
    @Excel(name = "案例图标")
    private String avatar;

    /**
     * 案例标题
     */
    @Excel(name = "案例标题")
    private String title;

    /**
     * 案例描述
     */
    @Excel(name = "案例描述")
    private String description;

    /**
     * 案例标签
     */
    @Excel(name = "案例标签")
    private String tags;

    /**
     * 首页链接
     */
    @Excel(name = "首页链接")
    private String homepage;

    /**
     * 案例详细内容
     */
    @Excel(name = "案例详细内容")
    private String detail;

    /**
     * 状态（0正常 1停用）
     */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Long sort;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Long getSort() {
        return sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("identify", getIdentify())
                .append("author", getAuthor())
                .append("avatar", getAvatar())
                .append("title", getTitle())
                .append("description", getDescription())
                .append("tags", getTags())
                .append("homepage", getHomepage())
                .append("detail", getDetail())
                .append("status", getStatus())
                .append("sort", getSort())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
