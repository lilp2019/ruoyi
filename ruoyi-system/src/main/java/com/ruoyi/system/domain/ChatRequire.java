package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户需求信息对象 chat_require
 *
 * @author ruoyi
 * @date 2024-04-06
 */
public class ChatRequire extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 需求方用户ID
     */
    private Long userId;

    private String userName;

    /**
     * 案例编号
     */
    private String identify;

    /**
     * 会话标识ID
     */
    private String sessionId;

    /**
     * 主题ID
     */
    private String topicId;

    /**
     * 需求标题
     */
    private String title;

    /**
     * 需求内容
     */
    private String content;

    /**
     * 标签信息
     */
    private String tags;

    /**
     * 状态（0正常 1关闭）
     */
    private String status;

    private Integer sort;

    /**
     * 合作意向：需求留言
     */
    private Integer countPlan = 0;

    /**
     * 推荐合作： 总结方案
     */
    private Integer countSummary = 0;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getCountPlan() {
        return countPlan;
    }

    public void setCountPlan(Integer countPlan) {
        this.countPlan = countPlan;
    }

    public Integer getCountSummary() {
        return countSummary;
    }

    public void setCountSummary(Integer countSummary) {
        this.countSummary = countSummary;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("identify", getIdentify())
                .append("sessionId", getSessionId())
                .append("topicId", getTopicId())
                .append("title", getTitle())
                .append("content", getContent())
                .append("tags", getTags())
                .append("status", getStatus())
                .append("sort", getSort())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
