package com.ruoyi.system.domain.vo;

import java.util.List;

public class ChatRequireReqVo {

    /**
     * 会话标识ID
     */
    private String sessionId;

    /**
     * 主题ID
     */
    private String topicId;

    /**
     * 请求fast-gpt消息
     */
    private List<MessageVo> messages;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public List<MessageVo> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageVo> messages) {
        this.messages = messages;
    }
}
