package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.vo.SysUserVo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 需求总结方案信息对象 chat_require_summary
 *
 * @author ruoyi
 * @date 2024-04-06
 */
public class ChatRequireSummary extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;

    /**
     * 需求ID
     */
    @Excel(name = "需求ID")
    private Integer requireId;

    private String requireTitle;

    /**
     * 方案提供方用户ID
     */
    @Excel(name = "方案提供方用户ID")
    private Long userId;

    private String userName;

    private SysUserVo user;

    /**
     * 方案标签（AI推荐 人气最高）
     */
    @Excel(name = "方案推荐标签", readConverterExp = "AI推荐,人气最高")
    private String tag;

    /**
     * 方案主图URL
     */
    @Excel(name = "方案主图URL")
    private String mainImage;

    /**
     * 方案总结内容
     */
    @Excel(name = "方案总结内容")
    private String content;

    /**
     * 预估价格
     */
    @Excel(name = "预估价格")
    private String prePrice;

    /**
     * 方案提供方公司名称
     */
    @Excel(name = "方案提供方公司名称")
    private String companyName;

    /**
     * 方案标签（多个用,隔开）
     */
    private String tags;

    /**
     * 方案状态（1-进行中，2-已完成，3-已失败）
     */
    private String status;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setRequireId(Integer requireId) {
        this.requireId = requireId;
    }

    public Integer getRequireId() {
        return requireId;
    }

    public String getRequireTitle() {
        return requireTitle;
    }

    public void setRequireTitle(String requireTitle) {
        this.requireTitle = requireTitle;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public SysUserVo getUser() {
        return user;
    }

    public void setUser(SysUserVo user) {
        this.user = user;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setPrePrice(String prePrice) {
        this.prePrice = prePrice;
    }

    public String getPrePrice() {
        return prePrice;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("requireId", getRequireId())
                .append("userId", getUserId())
                .append("tag", getTag())
                .append("mainImage", getMainImage())
                .append("content", getContent())
                .append("prePrice", getPrePrice())
                .append("companyName", getCompanyName())
                .append("tags", getTags())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
