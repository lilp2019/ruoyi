package com.ruoyi.system.domain.vo;

/**
 * 用户需求商机关联关系
 *
 * @author ruoyi
 * @date 2024-05-05
 */
public class AiUserRequireReqVo {

    /**
     * 商机ID
     */
    private Long requireId;

    /**
     * 状态：0-接受，1-拒绝
     */
    private String status;

    /**
     * 意向合作内容
     */
    private String content;

    public Long getRequireId() {
        return requireId;
    }

    public void setRequireId(Long requireId) {
        this.requireId = requireId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
