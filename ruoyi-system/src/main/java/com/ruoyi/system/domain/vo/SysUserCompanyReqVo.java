package com.ruoyi.system.domain.vo;

public class SysUserCompanyReqVo {

    /**
     * 企业授权标识
     */
    private String companyAuthFlag;

    /**
     * 企业名称
     */
    private String companyName;

    /**
     * 企业法人
     */
    private String companyLeader;

    /**
     * 公司信用编码
     */
    private String companyCode;

    public String getCompanyAuthFlag() {
        return companyAuthFlag;
    }

    public void setCompanyAuthFlag(String companyAuthFlag) {
        this.companyAuthFlag = companyAuthFlag;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLeader() {
        return companyLeader;
    }

    public void setCompanyLeader(String companyLeader) {
        this.companyLeader = companyLeader;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
