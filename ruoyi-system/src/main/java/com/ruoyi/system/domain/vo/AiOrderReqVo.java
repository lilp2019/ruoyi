package com.ruoyi.system.domain.vo;

public class AiOrderReqVo {

    /**
     * 订单金额，单位分
     */
    private Integer orderAmount;

    /**
     * 商品信息描述
     */
    private String body;

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
