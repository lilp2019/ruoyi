package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

public class SysUserThirdAccount extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 对应sys_user的用户id
     */
    private Long userId;

    /**
     * 第三方唯一用户id，可以是微信的openid，可以是QQ的openid，抑或苹果id,
     */
    private String thirdUniqueAccount;

    /**
     * 第三方用户名
     */
    private String thirdUniqueName;

    /**
     * 第三方用户头像url
     */
    private String thirdUniqueAvatar;

    /**
     * 标识第三方类型，1.gitee_id
     */
    private String bindType;

    /**
     * 标识是否绑定，1绑定，2解绑
     */
    private String bindFlag;

    /**
     * 绑定时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date bindDate;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setThirdUniqueAccount(String thirdUniqueAccount) {
        this.thirdUniqueAccount = thirdUniqueAccount;
    }

    public String getThirdUniqueAccount() {
        return thirdUniqueAccount;
    }

    public void setThirdUniqueName(String thirdUniqueName) {
        this.thirdUniqueName = thirdUniqueName;
    }

    public String getThirdUniqueName() {
        return thirdUniqueName;
    }

    public void setThirdUniqueAvatar(String thirdUniqueAvatar) {
        this.thirdUniqueAvatar = thirdUniqueAvatar;
    }

    public String getThirdUniqueAvatar() {
        return thirdUniqueAvatar;
    }

    public void setBindType(String bindType) {
        this.bindType = bindType;
    }

    public String getBindType() {
        return bindType;
    }

    public void setBindFlag(String bindFlag) {
        this.bindFlag = bindFlag;
    }

    public String getBindFlag() {
        return bindFlag;
    }

    public void setBindDate(Date bindDate) {
        this.bindDate = bindDate;
    }

    public Date getBindDate() {
        return bindDate;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("thirdUniqueAccount", getThirdUniqueAccount())
                .append("thirdUniqueName", getThirdUniqueName())
                .append("thirdUniqueAvatar", getThirdUniqueAvatar())
                .append("bindType", getBindType())
                .append("bindFlag", getBindFlag())
                .append("bindDate", getBindDate())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("delFlag", getDelFlag())
                .toString();
    }
}