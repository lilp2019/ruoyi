package com.ruoyi.system.domain.vo;

/**
 * 用户对话Agent配置信息对象 ai_agent
 *
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiSessionAgentVo {

    /**
     * sessionId：用户对话Agent所属窗口ID
     */
    private String sessionId;

    /**
     * Agent对话聊天apiKey
     */
    private String apiKey;

    /**
     * Agent模型，可选
     */
    private String model;

    /**
     * 模型提供厂商，如OpenAI等
     */
    private String provider;

    /**
     * 系统Prompt，预留字段
     */
    private String systemRole;

    /**
     * 系统endpoint，预留字段
     */
    private String endpoint;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }

    public String getSystemRole() {
        return systemRole;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
