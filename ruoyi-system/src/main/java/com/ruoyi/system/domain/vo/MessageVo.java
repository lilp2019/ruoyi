package com.ruoyi.system.domain.vo;

public class MessageVo {

    /**
     * 用户角色
     */
    private String role;

    /**
     * 内容
     */
    private String content;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
