package com.ruoyi.system.domain.vo;

import java.util.Date;

/**
 * 推荐方案（系统案例）信息对象 ai_scheme
 *
 * @author ruoyi
 * @date 2024-05-01
 */
public class AiSchemeCaseVo {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 方案推荐标识，通过类型区分（如：系统推荐，AI推荐，人气最高等）
     */
    private String type;

    /**
     * 方案标题
     */
    private String title;

    /**
     * 方案简介
     */
    private String description;

    /**
     * 方案内容
     */
    private String content;

    /**
     * 方案标签
     */
    private String tags;

    /**
     * 状态（0-进行中 1-已完成）
     */
    private String status;

    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 分解后节点总数
     */
    private Integer totalStepCount;

    /**
     * 进行中节点数
     */
    private Integer processCount;

    /**
     * 已完成节点数
     */
    private Integer finishedCount;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Integer getTotalStepCount() {
        return totalStepCount;
    }

    public void setTotalStepCount(Integer totalStepCount) {
        this.totalStepCount = totalStepCount;
    }

    public Integer getProcessCount() {
        return processCount;
    }

    public void setProcessCount(Integer processCount) {
        this.processCount = processCount;
    }

    public Integer getFinishedCount() {
        return finishedCount;
    }

    public void setFinishedCount(Integer finishedCount) {
        this.finishedCount = finishedCount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
