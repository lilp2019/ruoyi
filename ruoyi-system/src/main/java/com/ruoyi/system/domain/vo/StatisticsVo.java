package com.ruoyi.system.domain.vo;

/**
 * 数据统计信息
 *
 * @author ruoyi
 */
public class StatisticsVo {
    /**
     * 商机数据
     */
    private String chatRequireData = "0";
    /**
     * 意向合作数据
     */
    private String chatRequirePlanData = "0";
    /**
     * 合作方案数据
     */
    private String chatRequireSummaryData = "0";
    /**
     * 用户访问量数据
     */
    private String userLoginData = "0";

    public String getChatRequireData() {
        return chatRequireData;
    }

    public void setChatRequireData(String chatRequireData) {
        this.chatRequireData = chatRequireData;
    }

    public String getChatRequirePlanData() {
        return chatRequirePlanData;
    }

    public void setChatRequirePlanData(String chatRequirePlanData) {
        this.chatRequirePlanData = chatRequirePlanData;
    }

    public String getChatRequireSummaryData() {
        return chatRequireSummaryData;
    }

    public void setChatRequireSummaryData(String chatRequireSummaryData) {
        this.chatRequireSummaryData = chatRequireSummaryData;
    }

    public String getUserLoginData() {
        return userLoginData;
    }

    public void setUserLoginData(String userLoginData) {
        this.userLoginData = userLoginData;
    }
}
