package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AiRequireTemp;

/**
 * 用户临时需求商机信息Service接口
 * 
 * @author ruoyi
 * @date 2024-05-06
 */
public interface IAiRequireTempService 
{
    /**
     * 查询用户临时需求商机信息
     * 
     * @param id 用户临时需求商机信息主键
     * @return 用户临时需求商机信息
     */
    public AiRequireTemp selectAiRequireTempById(Long id);

    /**
     * 查询用户临时需求商机信息列表
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 用户临时需求商机信息集合
     */
    public List<AiRequireTemp> selectAiRequireTempList(AiRequireTemp aiRequireTemp);

    /**
     * 新增用户临时需求商机信息
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 结果
     */
    public int insertAiRequireTemp(AiRequireTemp aiRequireTemp);

    /**
     * 修改用户临时需求商机信息
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 结果
     */
    public int updateAiRequireTemp(AiRequireTemp aiRequireTemp);

    /**
     * 批量删除用户临时需求商机信息
     * 
     * @param ids 需要删除的用户临时需求商机信息主键集合
     * @return 结果
     */
    public int deleteAiRequireTempByIds(Long[] ids);

    /**
     * 删除用户临时需求商机信息信息
     * 
     * @param id 用户临时需求商机信息主键
     * @return 结果
     */
    public int deleteAiRequireTempById(Long id);
}
