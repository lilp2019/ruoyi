package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChatRequireMapper;
import com.ruoyi.system.domain.ChatRequire;
import com.ruoyi.system.service.IChatRequireService;

/**
 * 用户需求信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
@Service
public class ChatRequireServiceImpl implements IChatRequireService 
{
    @Autowired
    private ChatRequireMapper chatRequireMapper;

    /**
     * 查询用户需求信息
     * 
     * @param id 用户需求信息主键
     * @return 用户需求信息
     */
    @Override
    public ChatRequire selectChatRequireById(Integer id)
    {
        return chatRequireMapper.selectChatRequireById(id);
    }

    /**
     * 查询用户需求信息列表
     * 
     * @param chatRequire 用户需求信息
     * @return 用户需求信息
     */
    @Override
    public List<ChatRequire> selectChatRequireList(ChatRequire chatRequire)
    {
        return chatRequireMapper.selectChatRequireList(chatRequire);
    }

    /**
     * 新增用户需求信息
     * 
     * @param chatRequire 用户需求信息
     * @return 结果
     */
    @Override
    public int insertChatRequire(ChatRequire chatRequire)
    {
        chatRequire.setCreateTime(DateUtils.getNowDate());
        return chatRequireMapper.insertChatRequire(chatRequire);
    }

    /**
     * 修改用户需求信息
     * 
     * @param chatRequire 用户需求信息
     * @return 结果
     */
    @Override
    public int updateChatRequire(ChatRequire chatRequire)
    {
        chatRequire.setUpdateTime(DateUtils.getNowDate());
        return chatRequireMapper.updateChatRequire(chatRequire);
    }

    /**
     * 批量删除用户需求信息
     * 
     * @param ids 需要删除的用户需求信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequireByIds(Integer[] ids)
    {
        return chatRequireMapper.deleteChatRequireByIds(ids);
    }

    /**
     * 删除用户需求信息信息
     * 
     * @param id 用户需求信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequireById(Integer id)
    {
        return chatRequireMapper.deleteChatRequireById(id);
    }
}
