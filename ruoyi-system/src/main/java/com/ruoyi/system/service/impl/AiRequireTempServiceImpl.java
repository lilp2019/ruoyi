package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiRequireTempMapper;
import com.ruoyi.system.domain.AiRequireTemp;
import com.ruoyi.system.service.IAiRequireTempService;

/**
 * 用户临时需求商机信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-06
 */
@Service
public class AiRequireTempServiceImpl implements IAiRequireTempService 
{
    @Autowired
    private AiRequireTempMapper aiRequireTempMapper;

    /**
     * 查询用户临时需求商机信息
     * 
     * @param id 用户临时需求商机信息主键
     * @return 用户临时需求商机信息
     */
    @Override
    public AiRequireTemp selectAiRequireTempById(Long id)
    {
        return aiRequireTempMapper.selectAiRequireTempById(id);
    }

    /**
     * 查询用户临时需求商机信息列表
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 用户临时需求商机信息
     */
    @Override
    public List<AiRequireTemp> selectAiRequireTempList(AiRequireTemp aiRequireTemp)
    {
        return aiRequireTempMapper.selectAiRequireTempList(aiRequireTemp);
    }

    /**
     * 新增用户临时需求商机信息
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 结果
     */
    @Override
    public int insertAiRequireTemp(AiRequireTemp aiRequireTemp)
    {
        return aiRequireTempMapper.insertAiRequireTemp(aiRequireTemp);
    }

    /**
     * 修改用户临时需求商机信息
     * 
     * @param aiRequireTemp 用户临时需求商机信息
     * @return 结果
     */
    @Override
    public int updateAiRequireTemp(AiRequireTemp aiRequireTemp)
    {
        return aiRequireTempMapper.updateAiRequireTemp(aiRequireTemp);
    }

    /**
     * 批量删除用户临时需求商机信息
     * 
     * @param ids 需要删除的用户临时需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequireTempByIds(Long[] ids)
    {
        return aiRequireTempMapper.deleteAiRequireTempByIds(ids);
    }

    /**
     * 删除用户临时需求商机信息信息
     * 
     * @param id 用户临时需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequireTempById(Long id)
    {
        return aiRequireTempMapper.deleteAiRequireTempById(id);
    }
}
