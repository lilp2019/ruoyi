package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChatRequirePlanMapper;
import com.ruoyi.system.domain.ChatRequirePlan;
import com.ruoyi.system.service.IChatRequirePlanService;

/**
 * 需求方案信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
@Service
public class ChatRequirePlanServiceImpl implements IChatRequirePlanService 
{
    @Autowired
    private ChatRequirePlanMapper chatRequirePlanMapper;

    /**
     * 查询需求方案信息
     * 
     * @param id 需求方案信息主键
     * @return 需求方案信息
     */
    @Override
    public ChatRequirePlan selectChatRequirePlanById(Integer id)
    {
        return chatRequirePlanMapper.selectChatRequirePlanById(id);
    }

    /**
     * 查询需求方案信息列表
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 需求方案信息
     */
    @Override
    public List<ChatRequirePlan> selectChatRequirePlanList(ChatRequirePlan chatRequirePlan)
    {
        return chatRequirePlanMapper.selectChatRequirePlanList(chatRequirePlan);
    }

    /**
     * 新增需求方案信息
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 结果
     */
    @Override
    public int insertChatRequirePlan(ChatRequirePlan chatRequirePlan)
    {
        chatRequirePlan.setCreateTime(DateUtils.getNowDate());
        return chatRequirePlanMapper.insertChatRequirePlan(chatRequirePlan);
    }

    /**
     * 修改需求方案信息
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 结果
     */
    @Override
    public int updateChatRequirePlan(ChatRequirePlan chatRequirePlan)
    {
        chatRequirePlan.setUpdateTime(DateUtils.getNowDate());
        return chatRequirePlanMapper.updateChatRequirePlan(chatRequirePlan);
    }

    /**
     * 批量删除需求方案信息
     * 
     * @param ids 需要删除的需求方案信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequirePlanByIds(Integer[] ids)
    {
        return chatRequirePlanMapper.deleteChatRequirePlanByIds(ids);
    }

    /**
     * 删除需求方案信息信息
     * 
     * @param id 需求方案信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequirePlanById(Integer id)
    {
        return chatRequirePlanMapper.deleteChatRequirePlanById(id);
    }
}
