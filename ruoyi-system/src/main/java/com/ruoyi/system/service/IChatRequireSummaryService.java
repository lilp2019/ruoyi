package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ChatRequireSummary;

/**
 * 需求总结方案信息Service接口
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
public interface IChatRequireSummaryService 
{
    /**
     * 查询需求总结方案信息
     * 
     * @param id 需求总结方案信息主键
     * @return 需求总结方案信息
     */
    public ChatRequireSummary selectChatRequireSummaryById(Integer id);

    /**
     * 查询需求总结方案信息列表
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 需求总结方案信息集合
     */
    public List<ChatRequireSummary> selectChatRequireSummaryList(ChatRequireSummary chatRequireSummary);

    /**
     * 新增需求总结方案信息
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 结果
     */
    public int insertChatRequireSummary(ChatRequireSummary chatRequireSummary);

    /**
     * 修改需求总结方案信息
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 结果
     */
    public int updateChatRequireSummary(ChatRequireSummary chatRequireSummary);

    /**
     * 批量删除需求总结方案信息
     * 
     * @param ids 需要删除的需求总结方案信息主键集合
     * @return 结果
     */
    public int deleteChatRequireSummaryByIds(Integer[] ids);

    /**
     * 删除需求总结方案信息信息
     * 
     * @param id 需求总结方案信息主键
     * @return 结果
     */
    public int deleteChatRequireSummaryById(Integer id);
}
