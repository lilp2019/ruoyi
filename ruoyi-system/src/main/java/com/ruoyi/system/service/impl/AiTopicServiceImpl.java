package com.ruoyi.system.service.impl;

import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiTopicMapper;
import com.ruoyi.system.domain.AiTopic;
import com.ruoyi.system.service.IAiTopicService;

/**
 * 用户会话topic信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiTopicServiceImpl implements IAiTopicService {
    @Autowired
    private AiTopicMapper aiTopicMapper;

    /**
     * 查询用户会话topic信息
     *
     * @param id 用户会话topic信息主键
     * @return 用户会话topic信息
     */
    @Override
    public AiTopic selectAiTopicById(String id) {
        return aiTopicMapper.selectAiTopicById(id);
    }

    /**
     * 查询用户会话topic信息列表
     *
     * @param aiTopic 用户会话topic信息
     * @return 用户会话topic信息
     */
    @Override
    public List<AiTopic> selectAiTopicList(AiTopic aiTopic) {
        return aiTopicMapper.selectAiTopicList(aiTopic);
    }

    /**
     * 新增用户会话topic信息
     *
     * @param aiTopic 用户会话topic信息
     * @return 结果
     */
    @Override
    public int insertAiTopic(AiTopic aiTopic) {
        if (StrUtil.isBlank(aiTopic.getId())) {
            aiTopic.setId(StringUtils.randomStr(8));
        }
        return aiTopicMapper.insertAiTopic(aiTopic);
    }

    /**
     * 修改用户会话topic信息
     *
     * @param aiTopic 用户会话topic信息
     * @return 结果
     */
    @Override
    public int updateAiTopic(AiTopic aiTopic) {
        return aiTopicMapper.updateAiTopic(aiTopic);
    }

    /**
     * 批量删除用户会话topic信息
     *
     * @param ids 需要删除的用户会话topic信息主键
     * @return 结果
     */
    @Override
    public int deleteAiTopicByIds(String[] ids) {
        return aiTopicMapper.deleteAiTopicByIds(ids);
    }

    /**
     * 删除用户会话topic信息信息
     *
     * @param id 用户会话topic信息主键
     * @return 结果
     */
    @Override
    public int deleteAiTopicById(String id) {
        return aiTopicMapper.deleteAiTopicById(id);
    }
}
