package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysUserThirdAccount;

import java.util.List;

/**
 * 【三方登录】Service接口
 *
 * @author ruoyi
 * @date 2024-03-25
 */
public interface ISysUserThirdAccountService {
    /**
     * 查询第三方账户绑定
     *
     * @param id 第三方账户绑定主键
     * @return 第三方账户绑定
     */
    public SysUserThirdAccount selectSysUserThirdAccountById(Long id);

    /**
     * 查询第三方账户绑定列表
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 第三方账户绑定集合
     */
    public List<SysUserThirdAccount> selectSysUserThirdAccountList(SysUserThirdAccount sysUserThirdAccount);

    /**
     * 新增第三方账户绑定
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 结果
     */
    public int insertSysUserThirdAccount(SysUserThirdAccount sysUserThirdAccount);

    /**
     * 修改第三方账户绑定
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 结果
     */
    public int updateSysUserThirdAccount(SysUserThirdAccount sysUserThirdAccount);

    /**
     * 批量删除第三方账户绑定
     *
     * @param ids 需要删除的第三方账户绑定主键集合
     * @return 结果
     */
    public int deleteSysUserThirdAccountByIds(Long[] ids);

    /**
     * 删除第三方账户绑定信息
     *
     * @param id 第三方账户绑定主键
     * @return 结果
     */
    public int deleteSysUserThirdAccountById(Long id);
}
