package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiAgentCfgMapper;
import com.ruoyi.system.domain.AiAgentCfg;
import com.ruoyi.system.service.IAiAgentCfgService;

/**
 * 不同类型对话Agent配置信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiAgentCfgServiceImpl implements IAiAgentCfgService 
{
    @Autowired
    private AiAgentCfgMapper aiAgentCfgMapper;

    /**
     * 查询不同类型对话Agent配置信息
     * 
     * @param id 不同类型对话Agent配置信息主键
     * @return 不同类型对话Agent配置信息
     */
    @Override
    public AiAgentCfg selectAiAgentCfgById(Long id)
    {
        return aiAgentCfgMapper.selectAiAgentCfgById(id);
    }

    /**
     * 查询不同类型对话Agent配置信息列表
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 不同类型对话Agent配置信息
     */
    @Override
    public List<AiAgentCfg> selectAiAgentCfgList(AiAgentCfg aiAgentCfg)
    {
        return aiAgentCfgMapper.selectAiAgentCfgList(aiAgentCfg);
    }

    /**
     * 新增不同类型对话Agent配置信息
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 结果
     */
    @Override
    public int insertAiAgentCfg(AiAgentCfg aiAgentCfg)
    {
        return aiAgentCfgMapper.insertAiAgentCfg(aiAgentCfg);
    }

    /**
     * 修改不同类型对话Agent配置信息
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 结果
     */
    @Override
    public int updateAiAgentCfg(AiAgentCfg aiAgentCfg)
    {
        return aiAgentCfgMapper.updateAiAgentCfg(aiAgentCfg);
    }

    /**
     * 批量删除不同类型对话Agent配置信息
     * 
     * @param ids 需要删除的不同类型对话Agent配置信息主键
     * @return 结果
     */
    @Override
    public int deleteAiAgentCfgByIds(Long[] ids)
    {
        return aiAgentCfgMapper.deleteAiAgentCfgByIds(ids);
    }

    /**
     * 删除不同类型对话Agent配置信息信息
     * 
     * @param id 不同类型对话Agent配置信息主键
     * @return 结果
     */
    @Override
    public int deleteAiAgentCfgById(Long id)
    {
        return aiAgentCfgMapper.deleteAiAgentCfgById(id);
    }
}
