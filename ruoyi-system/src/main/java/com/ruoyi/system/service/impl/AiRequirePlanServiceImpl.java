package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiRequirePlanMapper;
import com.ruoyi.system.domain.AiRequirePlan;
import com.ruoyi.system.service.IAiRequirePlanService;

/**
 * 用户商机意向合作信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@Service
public class AiRequirePlanServiceImpl implements IAiRequirePlanService 
{
    @Autowired
    private AiRequirePlanMapper aiRequirePlanMapper;

    /**
     * 查询用户商机意向合作信息
     * 
     * @param id 用户商机意向合作信息主键
     * @return 用户商机意向合作信息
     */
    @Override
    public AiRequirePlan selectAiRequirePlanById(Long id)
    {
        return aiRequirePlanMapper.selectAiRequirePlanById(id);
    }

    /**
     * 查询用户商机意向合作信息列表
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 用户商机意向合作信息
     */
    @Override
    public List<AiRequirePlan> selectAiRequirePlanList(AiRequirePlan aiRequirePlan)
    {
        return aiRequirePlanMapper.selectAiRequirePlanList(aiRequirePlan);
    }

    /**
     * 新增用户商机意向合作信息
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 结果
     */
    @Override
    public int insertAiRequirePlan(AiRequirePlan aiRequirePlan)
    {
        return aiRequirePlanMapper.insertAiRequirePlan(aiRequirePlan);
    }

    /**
     * 修改用户商机意向合作信息
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 结果
     */
    @Override
    public int updateAiRequirePlan(AiRequirePlan aiRequirePlan)
    {
        return aiRequirePlanMapper.updateAiRequirePlan(aiRequirePlan);
    }

    /**
     * 批量删除用户商机意向合作信息
     * 
     * @param ids 需要删除的用户商机意向合作信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequirePlanByIds(Long[] ids)
    {
        return aiRequirePlanMapper.deleteAiRequirePlanByIds(ids);
    }

    /**
     * 删除用户商机意向合作信息信息
     * 
     * @param id 用户商机意向合作信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequirePlanById(Long id)
    {
        return aiRequirePlanMapper.deleteAiRequirePlanById(id);
    }
}
