package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiSchemeMapper;
import com.ruoyi.system.domain.AiScheme;
import com.ruoyi.system.service.IAiSchemeService;

/**
 * 用户方案信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiSchemeServiceImpl implements IAiSchemeService 
{
    @Autowired
    private AiSchemeMapper aiSchemeMapper;

    /**
     * 查询用户方案信息
     * 
     * @param id 用户方案信息主键
     * @return 用户方案信息
     */
    @Override
    public AiScheme selectAiSchemeById(Long id)
    {
        return aiSchemeMapper.selectAiSchemeById(id);
    }

    /**
     * 查询用户方案信息列表
     * 
     * @param aiScheme 用户方案信息
     * @return 用户方案信息
     */
    @Override
    public List<AiScheme> selectAiSchemeList(AiScheme aiScheme)
    {
        return aiSchemeMapper.selectAiSchemeList(aiScheme);
    }

    /**
     * 新增用户方案信息
     * 
     * @param aiScheme 用户方案信息
     * @return 结果
     */
    @Override
    public int insertAiScheme(AiScheme aiScheme)
    {
        return aiSchemeMapper.insertAiScheme(aiScheme);
    }

    /**
     * 修改用户方案信息
     * 
     * @param aiScheme 用户方案信息
     * @return 结果
     */
    @Override
    public int updateAiScheme(AiScheme aiScheme)
    {
        return aiSchemeMapper.updateAiScheme(aiScheme);
    }

    /**
     * 批量删除用户方案信息
     * 
     * @param ids 需要删除的用户方案信息主键
     * @return 结果
     */
    @Override
    public int deleteAiSchemeByIds(Long[] ids)
    {
        return aiSchemeMapper.deleteAiSchemeByIds(ids);
    }

    /**
     * 删除用户方案信息信息
     * 
     * @param id 用户方案信息主键
     * @return 结果
     */
    @Override
    public int deleteAiSchemeById(Long id)
    {
        return aiSchemeMapper.deleteAiSchemeById(id);
    }
}
