package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AiRequire;

/**
 * 用户需求商机信息Service接口
 * 
 * @author ruoyi
 * @date 2024-05-05
 */
public interface IAiRequireService 
{
    /**
     * 查询用户需求商机信息
     * 
     * @param id 用户需求商机信息主键
     * @return 用户需求商机信息
     */
    public AiRequire selectAiRequireById(Long id);

    /**
     * 查询用户需求商机信息列表
     * 
     * @param aiRequire 用户需求商机信息
     * @return 用户需求商机信息集合
     */
    public List<AiRequire> selectAiRequireList(AiRequire aiRequire);

    /**
     * 查询用户需求商机信息列表
     *
     * @param aiRequire 用户需求商机信息
     * @return 用户需求商机信息集合
     */
    public List<AiRequire> selectAiRequireRelationList(AiRequire aiRequire);

    /**
     * 新增用户需求商机信息
     * 
     * @param aiRequire 用户需求商机信息
     * @return 结果
     */
    public int insertAiRequire(AiRequire aiRequire);

    /**
     * 修改用户需求商机信息
     * 
     * @param aiRequire 用户需求商机信息
     * @return 结果
     */
    public int updateAiRequire(AiRequire aiRequire);

    /**
     * 批量删除用户需求商机信息
     * 
     * @param ids 需要删除的用户需求商机信息主键集合
     * @return 结果
     */
    public int deleteAiRequireByIds(Long[] ids);

    /**
     * 删除用户需求商机信息信息
     * 
     * @param id 用户需求商机信息主键
     * @return 结果
     */
    public int deleteAiRequireById(Long id);
}
