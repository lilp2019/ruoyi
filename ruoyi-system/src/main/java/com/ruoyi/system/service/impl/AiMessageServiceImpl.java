package com.ruoyi.system.service.impl;

import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiMessageMapper;
import com.ruoyi.system.domain.AiMessage;
import com.ruoyi.system.service.IAiMessageService;

/**
 * 用户会话信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiMessageServiceImpl implements IAiMessageService {
    @Autowired
    private AiMessageMapper aiMessageMapper;

    /**
     * 查询用户会话信息
     *
     * @param id 用户会话信息主键
     * @return 用户会话信息
     */
    @Override
    public AiMessage selectAiMessageById(String id) {
        return aiMessageMapper.selectAiMessageById(id);
    }

    /**
     * 查询用户会话信息列表
     *
     * @param aiMessage 用户会话信息
     * @return 用户会话信息
     */
    @Override
    public List<AiMessage> selectAiMessageList(AiMessage aiMessage) {
        return aiMessageMapper.selectAiMessageList(aiMessage);
    }

    /**
     * 新增用户会话信息
     *
     * @param aiMessage 用户会话信息
     * @return 结果
     */
    @Override
    public int insertAiMessage(AiMessage aiMessage) {
        if (StrUtil.isBlank(aiMessage.getId())) {
            aiMessage.setId(StringUtils.randomStr(8));
        }
        return aiMessageMapper.insertAiMessage(aiMessage);
    }

    /**
     * 修改用户会话信息
     *
     * @param aiMessage 用户会话信息
     * @return 结果
     */
    @Override
    public int updateAiMessage(AiMessage aiMessage) {
        return aiMessageMapper.updateAiMessage(aiMessage);
    }

    /**
     * 批量删除用户会话信息
     *
     * @param ids 需要删除的用户会话信息主键
     * @return 结果
     */
    @Override
    public int deleteAiMessageByIds(String[] ids) {
        return aiMessageMapper.deleteAiMessageByIds(ids);
    }

    /**
     * 删除用户会话信息信息
     *
     * @param id 用户会话信息主键
     * @return 结果
     */
    @Override
    public int deleteAiMessageById(String id) {
        return aiMessageMapper.deleteAiMessageById(id);
    }
}
