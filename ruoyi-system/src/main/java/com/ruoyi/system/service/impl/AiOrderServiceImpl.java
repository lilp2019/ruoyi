package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.AiOrder;
import com.ruoyi.system.mapper.AiOrderMapper;
import com.ruoyi.system.service.IAiOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-07
 */
@Service
public class AiOrderServiceImpl implements IAiOrderService {
    @Autowired
    private AiOrderMapper aiOrderMapper;

    /**
     * 查询订单信息
     *
     * @param orderId 订单信息主键
     * @return 订单信息
     */
    @Override
    public AiOrder selectAiOrderByOrderId(Long orderId) {
        return aiOrderMapper.selectAiOrderByOrderId(orderId);
    }

    /**
     * 根据订单编号查询订单信息
     *
     * @param orderNo
     * @return
     */
    @Override
    public AiOrder selectAiOrderByOrderNo(String orderNo) {
        return aiOrderMapper.selectAiOrderByOrderNo(orderNo);
    }

    /**
     * 查询订单信息列表
     *
     * @param aiOrder 订单信息
     * @return 订单信息
     */
    @Override
    public List<AiOrder> selectAiOrderList(AiOrder aiOrder) {
        return aiOrderMapper.selectAiOrderList(aiOrder);
    }

    /**
     * 新增订单信息
     *
     * @param aiOrder 订单信息
     * @return 结果
     */
    @Override
    public int insertAiOrder(AiOrder aiOrder) {
        aiOrder.setCreateTime(DateUtils.getNowDate());
        return aiOrderMapper.insertAiOrder(aiOrder);
    }

    /**
     * 修改订单信息
     *
     * @param aiOrder 订单信息
     * @return 结果
     */
    @Override
    public int updateAiOrder(AiOrder aiOrder) {
        aiOrder.setUpdateTime(DateUtils.getNowDate());
        return aiOrderMapper.updateAiOrder(aiOrder);
    }

    /**
     * 批量删除订单信息
     *
     * @param orderIds 需要删除的订单信息主键
     * @return 结果
     */
    @Override
    public int deleteAiOrderByOrderIds(Long[] orderIds) {
        return aiOrderMapper.deleteAiOrderByOrderIds(orderIds);
    }

    /**
     * 删除订单信息信息
     *
     * @param orderId 订单信息主键
     * @return 结果
     */
    @Override
    public int deleteAiOrderByOrderId(Long orderId) {
        return aiOrderMapper.deleteAiOrderByOrderId(orderId);
    }
}
