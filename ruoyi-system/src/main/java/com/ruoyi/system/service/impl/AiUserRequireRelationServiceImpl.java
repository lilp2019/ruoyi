package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiUserRequireRelationMapper;
import com.ruoyi.system.domain.AiUserRequireRelation;
import com.ruoyi.system.service.IAiUserRequireRelationService;

/**
 * 用户需求商机信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-15
 */
@Service
public class AiUserRequireRelationServiceImpl implements IAiUserRequireRelationService 
{
    @Autowired
    private AiUserRequireRelationMapper aiUserRequireRelationMapper;

    /**
     * 查询用户需求商机信息
     * 
     * @param id 用户需求商机信息主键
     * @return 用户需求商机信息
     */
    @Override
    public AiUserRequireRelation selectAiUserRequireRelationById(Long id)
    {
        return aiUserRequireRelationMapper.selectAiUserRequireRelationById(id);
    }

    /**
     * 查询用户需求商机信息列表
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 用户需求商机信息
     */
    @Override
    public List<AiUserRequireRelation> selectAiUserRequireRelationList(AiUserRequireRelation aiUserRequireRelation)
    {
        return aiUserRequireRelationMapper.selectAiUserRequireRelationList(aiUserRequireRelation);
    }

    /**
     * 新增用户需求商机信息
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 结果
     */
    @Override
    public int insertAiUserRequireRelation(AiUserRequireRelation aiUserRequireRelation)
    {
        return aiUserRequireRelationMapper.insertAiUserRequireRelation(aiUserRequireRelation);
    }

    /**
     * 修改用户需求商机信息
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 结果
     */
    @Override
    public int updateAiUserRequireRelation(AiUserRequireRelation aiUserRequireRelation)
    {
        return aiUserRequireRelationMapper.updateAiUserRequireRelation(aiUserRequireRelation);
    }

    /**
     * 批量删除用户需求商机信息
     * 
     * @param ids 需要删除的用户需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiUserRequireRelationByIds(Long[] ids)
    {
        return aiUserRequireRelationMapper.deleteAiUserRequireRelationByIds(ids);
    }

    /**
     * 删除用户需求商机信息信息
     * 
     * @param id 用户需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiUserRequireRelationById(Long id)
    {
        return aiUserRequireRelationMapper.deleteAiUserRequireRelationById(id);
    }
}
