package com.ruoyi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.system.domain.AiSession;
import com.ruoyi.system.mapper.AiSessionMapper;
import com.ruoyi.system.service.IAiSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户对话Session信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiSessionServiceImpl implements IAiSessionService {
    @Autowired
    private AiSessionMapper aiSessionMapper;

    /**
     * 查询用户对话Session信息
     *
     * @param id 用户对话Session信息主键
     * @return 用户对话Session信息
     */
    @Override
    public AiSession selectAiSessionById(String id) {
        return aiSessionMapper.selectAiSessionById(id);
    }

    /**
     * 查询用户对话Session信息列表
     *
     * @param aiSession 用户对话Session信息
     * @return 用户对话Session信息
     */
    @Override
    public List<AiSession> selectAiSessionList(AiSession aiSession) {
        return aiSessionMapper.selectAiSessionList(aiSession);
    }

    /**
     * 新增用户对话Session信息
     *
     * @param aiSession 用户对话Session信息
     * @return 结果
     */
    @Override
    public int insertAiSession(AiSession aiSession) {
        if (StrUtil.isBlank(aiSession.getId())) {
            aiSession.setId(IdUtils.fastUUID());
        }
        return aiSessionMapper.insertAiSession(aiSession);
    }

    /**
     * 修改用户对话Session信息
     *
     * @param aiSession 用户对话Session信息
     * @return 结果
     */
    @Override
    public int updateAiSession(AiSession aiSession) {
        return aiSessionMapper.updateAiSession(aiSession);
    }

    /**
     * 批量删除用户对话Session信息
     *
     * @param ids 需要删除的用户对话Session信息主键
     * @return 结果
     */
    @Override
    public int deleteAiSessionByIds(String[] ids) {
        return aiSessionMapper.deleteAiSessionByIds(ids);
    }

    /**
     * 删除用户对话Session信息信息
     *
     * @param id 用户对话Session信息主键
     * @return 结果
     */
    @Override
    public int deleteAiSessionById(String id) {
        return aiSessionMapper.deleteAiSessionById(id);
    }
}
