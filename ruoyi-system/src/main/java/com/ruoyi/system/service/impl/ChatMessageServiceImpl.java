package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.ChatMessage;
import com.ruoyi.system.mapper.ChatMessageMapper;
import com.ruoyi.system.service.IChatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【BankChat会话消息】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-25
 */
@Service
public class ChatMessageServiceImpl implements IChatMessageService {
    @Autowired
    private ChatMessageMapper chatMessageMapper;

    /**
     * 查询【BankChat会话消息】
     *
     * @param id 【BankChat会话消息】主键
     * @return 【BankChat会话消息】
     */
    @Override
    public ChatMessage selectChatMessageById(String id) {
        return chatMessageMapper.selectChatMessageById(id);
    }

    @Override
    public ChatMessage selectChatMessageByParentId(String parentId) {
        return chatMessageMapper.selectChatMessageByParentId(parentId);
    }

    /**
     * 查询【BankChat会话消息】列表
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 【BankChat会话消息】
     */
    @Override
    public List<ChatMessage> selectChatMessageList(ChatMessage chatMessage) {
        return chatMessageMapper.selectChatMessageList(chatMessage);
    }

    /**
     * 新增【BankChat会话消息】
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 结果
     */
    @Override
    public int insertChatMessage(ChatMessage chatMessage) {
        return chatMessageMapper.insertChatMessage(chatMessage);
    }

    /**
     * 修改【BankChat会话消息】
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 结果
     */
    @Override
    public int updateChatMessage(ChatMessage chatMessage) {
        return chatMessageMapper.updateChatMessage(chatMessage);
    }

    /**
     * 批量删除【BankChat会话消息】
     *
     * @param ids 需要删除的【BankChat会话消息】主键
     * @return 结果
     */
    @Override
    public int deleteChatMessageByIds(String[] ids) {
        return chatMessageMapper.deleteChatMessageByIds(ids);
    }

    /**
     * 删除【BankChat会话消息】信息
     *
     * @param id 【BankChat会话消息】主键
     * @return 结果
     */
    @Override
    public int deleteChatMessageById(String id) {
        return chatMessageMapper.deleteChatMessageById(id);
    }
}
