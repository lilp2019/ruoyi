package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AiAgent;

/**
 * 用户对话Agent配置信息Service接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface IAiAgentService 
{
    /**
     * 查询用户对话Agent配置信息
     * 
     * @param id 用户对话Agent配置信息主键
     * @return 用户对话Agent配置信息
     */
    public AiAgent selectAiAgentById(String id);

    /**
     * 查询用户对话Agent配置信息列表
     * 
     * @param aiAgent 用户对话Agent配置信息
     * @return 用户对话Agent配置信息集合
     */
    public List<AiAgent> selectAiAgentList(AiAgent aiAgent);

    /**
     * 新增用户对话Agent配置信息
     * 
     * @param aiAgent 用户对话Agent配置信息
     * @return 结果
     */
    public int insertAiAgent(AiAgent aiAgent);

    /**
     * 修改用户对话Agent配置信息
     * 
     * @param aiAgent 用户对话Agent配置信息
     * @return 结果
     */
    public int updateAiAgent(AiAgent aiAgent);

    /**
     * 批量删除用户对话Agent配置信息
     * 
     * @param ids 需要删除的用户对话Agent配置信息主键集合
     * @return 结果
     */
    public int deleteAiAgentByIds(String[] ids);

    /**
     * 删除用户对话Agent配置信息信息
     * 
     * @param id 用户对话Agent配置信息主键
     * @return 结果
     */
    public int deleteAiAgentById(String id);
}
