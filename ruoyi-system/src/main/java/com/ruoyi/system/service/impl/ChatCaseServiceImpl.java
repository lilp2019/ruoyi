package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChatCaseMapper;
import com.ruoyi.system.domain.ChatCase;
import com.ruoyi.system.service.IChatCaseService;

/**
 * 案例信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-08
 */
@Service
public class ChatCaseServiceImpl implements IChatCaseService 
{
    @Autowired
    private ChatCaseMapper chatCaseMapper;

    /**
     * 查询案例信息
     * 
     * @param id 案例信息主键
     * @return 案例信息
     */
    @Override
    public ChatCase selectChatCaseById(Integer id)
    {
        return chatCaseMapper.selectChatCaseById(id);
    }

    /**
     * 查询案例信息列表
     * 
     * @param chatCase 案例信息
     * @return 案例信息
     */
    @Override
    public List<ChatCase> selectChatCaseList(ChatCase chatCase)
    {
        return chatCaseMapper.selectChatCaseList(chatCase);
    }

    /**
     * 新增案例信息
     * 
     * @param chatCase 案例信息
     * @return 结果
     */
    @Override
    public int insertChatCase(ChatCase chatCase)
    {
        chatCase.setCreateTime(DateUtils.getNowDate());
        return chatCaseMapper.insertChatCase(chatCase);
    }

    /**
     * 修改案例信息
     * 
     * @param chatCase 案例信息
     * @return 结果
     */
    @Override
    public int updateChatCase(ChatCase chatCase)
    {
        chatCase.setUpdateTime(DateUtils.getNowDate());
        return chatCaseMapper.updateChatCase(chatCase);
    }

    /**
     * 批量删除案例信息
     * 
     * @param ids 需要删除的案例信息主键
     * @return 结果
     */
    @Override
    public int deleteChatCaseByIds(Integer[] ids)
    {
        return chatCaseMapper.deleteChatCaseByIds(ids);
    }

    /**
     * 删除案例信息信息
     * 
     * @param id 案例信息主键
     * @return 结果
     */
    @Override
    public int deleteChatCaseById(Integer id)
    {
        return chatCaseMapper.deleteChatCaseById(id);
    }
}
