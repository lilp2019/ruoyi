package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AiOrder;

/**
 * 订单信息Service接口
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
public interface IAiOrderService 
{
    /**
     * 查询订单信息
     * 
     * @param orderId 订单信息主键
     * @return 订单信息
     */
    public AiOrder selectAiOrderByOrderId(Long orderId);

    /**
     * 根据订单编号查询订单
     *
     * @param orderNo
     * @return
     */
    public AiOrder selectAiOrderByOrderNo(String orderNo);

    /**
     * 查询订单信息列表
     * 
     * @param aiOrder 订单信息
     * @return 订单信息集合
     */
    public List<AiOrder> selectAiOrderList(AiOrder aiOrder);

    /**
     * 新增订单信息
     * 
     * @param aiOrder 订单信息
     * @return 结果
     */
    public int insertAiOrder(AiOrder aiOrder);

    /**
     * 修改订单信息
     * 
     * @param aiOrder 订单信息
     * @return 结果
     */
    public int updateAiOrder(AiOrder aiOrder);

    /**
     * 批量删除订单信息
     * 
     * @param orderIds 需要删除的订单信息主键集合
     * @return 结果
     */
    public int deleteAiOrderByOrderIds(Long[] orderIds);

    /**
     * 删除订单信息信息
     * 
     * @param orderId 订单信息主键
     * @return 结果
     */
    public int deleteAiOrderByOrderId(Long orderId);
}
