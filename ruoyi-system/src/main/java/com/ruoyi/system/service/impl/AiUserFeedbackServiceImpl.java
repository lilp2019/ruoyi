package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiUserFeedbackMapper;
import com.ruoyi.system.domain.AiUserFeedback;
import com.ruoyi.system.service.IAiUserFeedbackService;

/**
 * 用户意见反馈信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
@Service
public class AiUserFeedbackServiceImpl implements IAiUserFeedbackService 
{
    @Autowired
    private AiUserFeedbackMapper aiUserFeedbackMapper;

    /**
     * 查询用户意见反馈信息
     * 
     * @param id 用户意见反馈信息主键
     * @return 用户意见反馈信息
     */
    @Override
    public AiUserFeedback selectAiUserFeedbackById(Long id)
    {
        return aiUserFeedbackMapper.selectAiUserFeedbackById(id);
    }

    /**
     * 查询用户意见反馈信息列表
     * 
     * @param aiUserFeedback 用户意见反馈信息
     * @return 用户意见反馈信息
     */
    @Override
    public List<AiUserFeedback> selectAiUserFeedbackList(AiUserFeedback aiUserFeedback)
    {
        return aiUserFeedbackMapper.selectAiUserFeedbackList(aiUserFeedback);
    }

    /**
     * 新增用户意见反馈信息
     * 
     * @param aiUserFeedback 用户意见反馈信息
     * @return 结果
     */
    @Override
    public int insertAiUserFeedback(AiUserFeedback aiUserFeedback)
    {
        return aiUserFeedbackMapper.insertAiUserFeedback(aiUserFeedback);
    }

    /**
     * 修改用户意见反馈信息
     * 
     * @param aiUserFeedback 用户意见反馈信息
     * @return 结果
     */
    @Override
    public int updateAiUserFeedback(AiUserFeedback aiUserFeedback)
    {
        return aiUserFeedbackMapper.updateAiUserFeedback(aiUserFeedback);
    }

    /**
     * 批量删除用户意见反馈信息
     * 
     * @param ids 需要删除的用户意见反馈信息主键
     * @return 结果
     */
    @Override
    public int deleteAiUserFeedbackByIds(Long[] ids)
    {
        return aiUserFeedbackMapper.deleteAiUserFeedbackByIds(ids);
    }

    /**
     * 删除用户意见反馈信息信息
     * 
     * @param id 用户意见反馈信息主键
     * @return 结果
     */
    @Override
    public int deleteAiUserFeedbackById(Long id)
    {
        return aiUserFeedbackMapper.deleteAiUserFeedbackById(id);
    }
}
