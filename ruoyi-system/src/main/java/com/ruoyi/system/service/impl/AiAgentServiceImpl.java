package com.ruoyi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.system.domain.AiAgent;
import com.ruoyi.system.mapper.AiAgentMapper;
import com.ruoyi.system.service.IAiAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户对话Agent配置信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-01
 */
@Service
public class AiAgentServiceImpl implements IAiAgentService {
    @Autowired
    private AiAgentMapper aiAgentMapper;

    /**
     * 查询用户对话Agent配置信息
     *
     * @param id 用户对话Agent配置信息主键
     * @return 用户对话Agent配置信息
     */
    @Override
    public AiAgent selectAiAgentById(String id) {
        return aiAgentMapper.selectAiAgentById(id);
    }

    /**
     * 查询用户对话Agent配置信息列表
     *
     * @param aiAgent 用户对话Agent配置信息
     * @return 用户对话Agent配置信息
     */
    @Override
    public List<AiAgent> selectAiAgentList(AiAgent aiAgent) {
        return aiAgentMapper.selectAiAgentList(aiAgent);
    }

    /**
     * 新增用户对话Agent配置信息
     *
     * @param aiAgent 用户对话Agent配置信息
     * @return 结果
     */
    @Override
    public int insertAiAgent(AiAgent aiAgent) {
        if (StrUtil.isBlank(aiAgent.getId())) {
            aiAgent.setId(IdUtils.fastUUID());
        }
        return aiAgentMapper.insertAiAgent(aiAgent);
    }

    /**
     * 修改用户对话Agent配置信息
     *
     * @param aiAgent 用户对话Agent配置信息
     * @return 结果
     */
    @Override
    public int updateAiAgent(AiAgent aiAgent) {
        return aiAgentMapper.updateAiAgent(aiAgent);
    }

    /**
     * 批量删除用户对话Agent配置信息
     *
     * @param ids 需要删除的用户对话Agent配置信息主键
     * @return 结果
     */
    @Override
    public int deleteAiAgentByIds(String[] ids) {
        return aiAgentMapper.deleteAiAgentByIds(ids);
    }

    /**
     * 删除用户对话Agent配置信息信息
     *
     * @param id 用户对话Agent配置信息主键
     * @return 结果
     */
    @Override
    public int deleteAiAgentById(String id) {
        return aiAgentMapper.deleteAiAgentById(id);
    }
}
