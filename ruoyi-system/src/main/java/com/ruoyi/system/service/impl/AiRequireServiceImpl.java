package com.ruoyi.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AiRequireMapper;
import com.ruoyi.system.domain.AiRequire;
import com.ruoyi.system.service.IAiRequireService;

/**
 * 用户需求商机信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-05-05
 */
@Service
public class AiRequireServiceImpl implements IAiRequireService {
    @Autowired
    private AiRequireMapper aiRequireMapper;

    /**
     * 查询用户需求商机信息
     *
     * @param id 用户需求商机信息主键
     * @return 用户需求商机信息
     */
    @Override
    public AiRequire selectAiRequireById(Long id) {
        return aiRequireMapper.selectAiRequireById(id);
    }

    /**
     * 查询用户需求商机信息列表
     *
     * @param aiRequire 用户需求商机信息
     * @return 用户需求商机信息
     */
    @Override
    public List<AiRequire> selectAiRequireList(AiRequire aiRequire) {
        return aiRequireMapper.selectAiRequireList(aiRequire);
    }

    @Override
    public List<AiRequire> selectAiRequireRelationList(AiRequire aiRequire) {
        return aiRequireMapper.selectAiRequireRelationList(aiRequire);
    }

    /**
     * 新增用户需求商机信息
     *
     * @param aiRequire 用户需求商机信息
     * @return 结果
     */
    @Override
    public int insertAiRequire(AiRequire aiRequire) {
        return aiRequireMapper.insertAiRequire(aiRequire);
    }

    /**
     * 修改用户需求商机信息
     *
     * @param aiRequire 用户需求商机信息
     * @return 结果
     */
    @Override
    public int updateAiRequire(AiRequire aiRequire) {
        return aiRequireMapper.updateAiRequire(aiRequire);
    }

    /**
     * 批量删除用户需求商机信息
     *
     * @param ids 需要删除的用户需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequireByIds(Long[] ids) {
        return aiRequireMapper.deleteAiRequireByIds(ids);
    }

    /**
     * 删除用户需求商机信息信息
     *
     * @param id 用户需求商机信息主键
     * @return 结果
     */
    @Override
    public int deleteAiRequireById(Long id) {
        return aiRequireMapper.deleteAiRequireById(id);
    }
}
