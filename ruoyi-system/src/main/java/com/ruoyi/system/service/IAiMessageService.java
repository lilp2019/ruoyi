package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.AiMessage;

/**
 * 用户会话信息Service接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface IAiMessageService 
{
    /**
     * 查询用户会话信息
     * 
     * @param id 用户会话信息主键
     * @return 用户会话信息
     */
    public AiMessage selectAiMessageById(String id);

    /**
     * 查询用户会话信息列表
     * 
     * @param aiMessage 用户会话信息
     * @return 用户会话信息集合
     */
    public List<AiMessage> selectAiMessageList(AiMessage aiMessage);

    /**
     * 新增用户会话信息
     * 
     * @param aiMessage 用户会话信息
     * @return 结果
     */
    public int insertAiMessage(AiMessage aiMessage);

    /**
     * 修改用户会话信息
     * 
     * @param aiMessage 用户会话信息
     * @return 结果
     */
    public int updateAiMessage(AiMessage aiMessage);

    /**
     * 批量删除用户会话信息
     * 
     * @param ids 需要删除的用户会话信息主键集合
     * @return 结果
     */
    public int deleteAiMessageByIds(String[] ids);

    /**
     * 删除用户会话信息信息
     * 
     * @param id 用户会话信息主键
     * @return 结果
     */
    public int deleteAiMessageById(String id);
}
