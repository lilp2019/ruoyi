package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysUserThirdAccount;
import com.ruoyi.system.mapper.SysUserThirdAccountMapper;
import com.ruoyi.system.service.ISysUserThirdAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 【三方登录】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-25
 */
@Service
public class SysUserThirdAccountServiceImpl implements ISysUserThirdAccountService {
    @Autowired
    private SysUserThirdAccountMapper sysUserThirdAccountMapper;

    /**
     * 查询第三方账户绑定
     *
     * @param id 第三方账户绑定主键
     * @return 第三方账户绑定
     */
    @Override
    public SysUserThirdAccount selectSysUserThirdAccountById(Long id) {
        return sysUserThirdAccountMapper.selectSysUserThirdAccountById(id);
    }

    /**
     * 查询第三方账户绑定列表
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 第三方账户绑定
     */
    @Override
    public List<SysUserThirdAccount> selectSysUserThirdAccountList(SysUserThirdAccount sysUserThirdAccount) {
        return sysUserThirdAccountMapper.selectSysUserThirdAccountList(sysUserThirdAccount);
    }

    /**
     * 新增第三方账户绑定
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 结果
     */
    @Override
    public int insertSysUserThirdAccount(SysUserThirdAccount sysUserThirdAccount) {
        sysUserThirdAccount.setCreateTime(DateUtils.getNowDate());
        return sysUserThirdAccountMapper.insertSysUserThirdAccount(sysUserThirdAccount);
    }

    /**
     * 修改第三方账户绑定
     *
     * @param sysUserThirdAccount 第三方账户绑定
     * @return 结果
     */
    @Override
    public int updateSysUserThirdAccount(SysUserThirdAccount sysUserThirdAccount) {
        sysUserThirdAccount.setUpdateTime(DateUtils.getNowDate());
        return sysUserThirdAccountMapper.updateSysUserThirdAccount(sysUserThirdAccount);
    }

    /**
     * 批量删除第三方账户绑定
     *
     * @param ids 需要删除的第三方账户绑定主键
     * @return 结果
     */
    @Override
    public int deleteSysUserThirdAccountByIds(Long[] ids) {
        return sysUserThirdAccountMapper.deleteSysUserThirdAccountByIds(ids);
    }

    /**
     * 删除第三方账户绑定信息
     *
     * @param id 第三方账户绑定主键
     * @return 结果
     */
    @Override
    public int deleteSysUserThirdAccountById(Long id) {
        return sysUserThirdAccountMapper.deleteSysUserThirdAccountById(id);
    }
}
