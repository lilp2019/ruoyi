package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ChatRequireSummaryMapper;
import com.ruoyi.system.domain.ChatRequireSummary;
import com.ruoyi.system.service.IChatRequireSummaryService;

/**
 * 需求总结方案信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
@Service
public class ChatRequireSummaryServiceImpl implements IChatRequireSummaryService 
{
    @Autowired
    private ChatRequireSummaryMapper chatRequireSummaryMapper;

    /**
     * 查询需求总结方案信息
     * 
     * @param id 需求总结方案信息主键
     * @return 需求总结方案信息
     */
    @Override
    public ChatRequireSummary selectChatRequireSummaryById(Integer id)
    {
        return chatRequireSummaryMapper.selectChatRequireSummaryById(id);
    }

    /**
     * 查询需求总结方案信息列表
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 需求总结方案信息
     */
    @Override
    public List<ChatRequireSummary> selectChatRequireSummaryList(ChatRequireSummary chatRequireSummary)
    {
        return chatRequireSummaryMapper.selectChatRequireSummaryList(chatRequireSummary);
    }

    /**
     * 新增需求总结方案信息
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 结果
     */
    @Override
    public int insertChatRequireSummary(ChatRequireSummary chatRequireSummary)
    {
        chatRequireSummary.setCreateTime(DateUtils.getNowDate());
        return chatRequireSummaryMapper.insertChatRequireSummary(chatRequireSummary);
    }

    /**
     * 修改需求总结方案信息
     * 
     * @param chatRequireSummary 需求总结方案信息
     * @return 结果
     */
    @Override
    public int updateChatRequireSummary(ChatRequireSummary chatRequireSummary)
    {
        chatRequireSummary.setUpdateTime(DateUtils.getNowDate());
        return chatRequireSummaryMapper.updateChatRequireSummary(chatRequireSummary);
    }

    /**
     * 批量删除需求总结方案信息
     * 
     * @param ids 需要删除的需求总结方案信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequireSummaryByIds(Integer[] ids)
    {
        return chatRequireSummaryMapper.deleteChatRequireSummaryByIds(ids);
    }

    /**
     * 删除需求总结方案信息信息
     * 
     * @param id 需求总结方案信息主键
     * @return 结果
     */
    @Override
    public int deleteChatRequireSummaryById(Integer id)
    {
        return chatRequireSummaryMapper.deleteChatRequireSummaryById(id);
    }
}
