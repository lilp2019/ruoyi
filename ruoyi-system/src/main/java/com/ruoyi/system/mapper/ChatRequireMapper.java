package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ChatRequire;

/**
 * 用户需求信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
public interface ChatRequireMapper 
{
    /**
     * 查询用户需求信息
     * 
     * @param id 用户需求信息主键
     * @return 用户需求信息
     */
    public ChatRequire selectChatRequireById(Integer id);

    /**
     * 查询用户需求信息列表
     * 
     * @param chatRequire 用户需求信息
     * @return 用户需求信息集合
     */
    public List<ChatRequire> selectChatRequireList(ChatRequire chatRequire);

    /**
     * 新增用户需求信息
     * 
     * @param chatRequire 用户需求信息
     * @return 结果
     */
    public int insertChatRequire(ChatRequire chatRequire);

    /**
     * 修改用户需求信息
     * 
     * @param chatRequire 用户需求信息
     * @return 结果
     */
    public int updateChatRequire(ChatRequire chatRequire);

    /**
     * 删除用户需求信息
     * 
     * @param id 用户需求信息主键
     * @return 结果
     */
    public int deleteChatRequireById(Integer id);

    /**
     * 批量删除用户需求信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatRequireByIds(Integer[] ids);
}
