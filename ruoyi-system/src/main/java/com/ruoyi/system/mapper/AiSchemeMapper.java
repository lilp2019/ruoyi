package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiScheme;

/**
 * 用户方案信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface AiSchemeMapper 
{
    /**
     * 查询用户方案信息
     * 
     * @param id 用户方案信息主键
     * @return 用户方案信息
     */
    public AiScheme selectAiSchemeById(Long id);

    /**
     * 查询用户方案信息列表
     * 
     * @param aiScheme 用户方案信息
     * @return 用户方案信息集合
     */
    public List<AiScheme> selectAiSchemeList(AiScheme aiScheme);

    /**
     * 新增用户方案信息
     * 
     * @param aiScheme 用户方案信息
     * @return 结果
     */
    public int insertAiScheme(AiScheme aiScheme);

    /**
     * 修改用户方案信息
     * 
     * @param aiScheme 用户方案信息
     * @return 结果
     */
    public int updateAiScheme(AiScheme aiScheme);

    /**
     * 删除用户方案信息
     * 
     * @param id 用户方案信息主键
     * @return 结果
     */
    public int deleteAiSchemeById(Long id);

    /**
     * 批量删除用户方案信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiSchemeByIds(Long[] ids);
}
