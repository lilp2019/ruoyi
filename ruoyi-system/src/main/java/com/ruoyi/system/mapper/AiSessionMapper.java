package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiSession;

/**
 * 用户对话Session信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface AiSessionMapper 
{
    /**
     * 查询用户对话Session信息
     * 
     * @param id 用户对话Session信息主键
     * @return 用户对话Session信息
     */
    public AiSession selectAiSessionById(String id);

    /**
     * 查询用户对话Session信息列表
     * 
     * @param aiSession 用户对话Session信息
     * @return 用户对话Session信息集合
     */
    public List<AiSession> selectAiSessionList(AiSession aiSession);

    /**
     * 新增用户对话Session信息
     * 
     * @param aiSession 用户对话Session信息
     * @return 结果
     */
    public int insertAiSession(AiSession aiSession);

    /**
     * 修改用户对话Session信息
     * 
     * @param aiSession 用户对话Session信息
     * @return 结果
     */
    public int updateAiSession(AiSession aiSession);

    /**
     * 删除用户对话Session信息
     * 
     * @param id 用户对话Session信息主键
     * @return 结果
     */
    public int deleteAiSessionById(String id);

    /**
     * 批量删除用户对话Session信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiSessionByIds(String[] ids);
}
