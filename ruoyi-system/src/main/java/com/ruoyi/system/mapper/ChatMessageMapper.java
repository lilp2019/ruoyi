package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ChatMessage;

import java.util.List;

/**
 * 【BankChat会话消息】Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-25
 */
public interface ChatMessageMapper {
    /**
     * 查询【BankChat会话消息】
     *
     * @param id 【BankChat会话消息】主键
     * @return 【BankChat会话消息】
     */
    public ChatMessage selectChatMessageById(String id);

    /**
     * 查询【BankChat会话消息】
     *
     * @param parentId 【BankChat会话消息】主键
     * @return 【BankChat会话消息】
     */
    public ChatMessage selectChatMessageByParentId(String parentId);

    /**
     * 查询【BankChat会话消息】列表
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 【BankChat会话消息】集合
     */
    public List<ChatMessage> selectChatMessageList(ChatMessage chatMessage);

    /**
     * 新增【BankChat会话消息】
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 结果
     */
    public int insertChatMessage(ChatMessage chatMessage);

    /**
     * 修改【BankChat会话消息】
     *
     * @param chatMessage 【BankChat会话消息】
     * @return 结果
     */
    public int updateChatMessage(ChatMessage chatMessage);

    /**
     * 删除【BankChat会话消息】
     *
     * @param id 【BankChat会话消息】主键
     * @return 结果
     */
    public int deleteChatMessageById(String id);

    /**
     * 批量删除【BankChat会话消息】
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatMessageByIds(String[] ids);
}
