package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiRequirePlan;

/**
 * 用户商机意向合作信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
public interface AiRequirePlanMapper 
{
    /**
     * 查询用户商机意向合作信息
     * 
     * @param id 用户商机意向合作信息主键
     * @return 用户商机意向合作信息
     */
    public AiRequirePlan selectAiRequirePlanById(Long id);

    /**
     * 查询用户商机意向合作信息列表
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 用户商机意向合作信息集合
     */
    public List<AiRequirePlan> selectAiRequirePlanList(AiRequirePlan aiRequirePlan);

    /**
     * 新增用户商机意向合作信息
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 结果
     */
    public int insertAiRequirePlan(AiRequirePlan aiRequirePlan);

    /**
     * 修改用户商机意向合作信息
     * 
     * @param aiRequirePlan 用户商机意向合作信息
     * @return 结果
     */
    public int updateAiRequirePlan(AiRequirePlan aiRequirePlan);

    /**
     * 删除用户商机意向合作信息
     * 
     * @param id 用户商机意向合作信息主键
     * @return 结果
     */
    public int deleteAiRequirePlanById(Long id);

    /**
     * 批量删除用户商机意向合作信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiRequirePlanByIds(Long[] ids);
}
