package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ChatRequirePlan;

/**
 * 需求方案信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
public interface ChatRequirePlanMapper 
{
    /**
     * 查询需求方案信息
     * 
     * @param id 需求方案信息主键
     * @return 需求方案信息
     */
    public ChatRequirePlan selectChatRequirePlanById(Integer id);

    /**
     * 查询需求方案信息列表
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 需求方案信息集合
     */
    public List<ChatRequirePlan> selectChatRequirePlanList(ChatRequirePlan chatRequirePlan);

    /**
     * 新增需求方案信息
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 结果
     */
    public int insertChatRequirePlan(ChatRequirePlan chatRequirePlan);

    /**
     * 修改需求方案信息
     * 
     * @param chatRequirePlan 需求方案信息
     * @return 结果
     */
    public int updateChatRequirePlan(ChatRequirePlan chatRequirePlan);

    /**
     * 删除需求方案信息
     * 
     * @param id 需求方案信息主键
     * @return 结果
     */
    public int deleteChatRequirePlanById(Integer id);

    /**
     * 批量删除需求方案信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatRequirePlanByIds(Integer[] ids);
}
