package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiAgentCfg;

/**
 * 不同类型对话Agent配置信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface AiAgentCfgMapper 
{
    /**
     * 查询不同类型对话Agent配置信息
     * 
     * @param id 不同类型对话Agent配置信息主键
     * @return 不同类型对话Agent配置信息
     */
    public AiAgentCfg selectAiAgentCfgById(Long id);

    /**
     * 查询不同类型对话Agent配置信息列表
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 不同类型对话Agent配置信息集合
     */
    public List<AiAgentCfg> selectAiAgentCfgList(AiAgentCfg aiAgentCfg);

    /**
     * 新增不同类型对话Agent配置信息
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 结果
     */
    public int insertAiAgentCfg(AiAgentCfg aiAgentCfg);

    /**
     * 修改不同类型对话Agent配置信息
     * 
     * @param aiAgentCfg 不同类型对话Agent配置信息
     * @return 结果
     */
    public int updateAiAgentCfg(AiAgentCfg aiAgentCfg);

    /**
     * 删除不同类型对话Agent配置信息
     * 
     * @param id 不同类型对话Agent配置信息主键
     * @return 结果
     */
    public int deleteAiAgentCfgById(Long id);

    /**
     * 批量删除不同类型对话Agent配置信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiAgentCfgByIds(Long[] ids);
}
