package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ChatCase;

/**
 * 案例信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-08
 */
public interface ChatCaseMapper 
{
    /**
     * 查询案例信息
     * 
     * @param id 案例信息主键
     * @return 案例信息
     */
    public ChatCase selectChatCaseById(Integer id);

    /**
     * 查询案例信息列表
     * 
     * @param chatCase 案例信息
     * @return 案例信息集合
     */
    public List<ChatCase> selectChatCaseList(ChatCase chatCase);

    /**
     * 新增案例信息
     * 
     * @param chatCase 案例信息
     * @return 结果
     */
    public int insertChatCase(ChatCase chatCase);

    /**
     * 修改案例信息
     * 
     * @param chatCase 案例信息
     * @return 结果
     */
    public int updateChatCase(ChatCase chatCase);

    /**
     * 删除案例信息
     * 
     * @param id 案例信息主键
     * @return 结果
     */
    public int deleteChatCaseById(Integer id);

    /**
     * 批量删除案例信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteChatCaseByIds(Integer[] ids);
}
