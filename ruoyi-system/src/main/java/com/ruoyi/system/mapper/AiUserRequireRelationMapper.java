package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiUserRequireRelation;

/**
 * 用户需求商机信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-15
 */
public interface AiUserRequireRelationMapper 
{
    /**
     * 查询用户需求商机信息
     * 
     * @param id 用户需求商机信息主键
     * @return 用户需求商机信息
     */
    public AiUserRequireRelation selectAiUserRequireRelationById(Long id);

    /**
     * 查询用户需求商机信息列表
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 用户需求商机信息集合
     */
    public List<AiUserRequireRelation> selectAiUserRequireRelationList(AiUserRequireRelation aiUserRequireRelation);

    /**
     * 新增用户需求商机信息
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 结果
     */
    public int insertAiUserRequireRelation(AiUserRequireRelation aiUserRequireRelation);

    /**
     * 修改用户需求商机信息
     * 
     * @param aiUserRequireRelation 用户需求商机信息
     * @return 结果
     */
    public int updateAiUserRequireRelation(AiUserRequireRelation aiUserRequireRelation);

    /**
     * 删除用户需求商机信息
     * 
     * @param id 用户需求商机信息主键
     * @return 结果
     */
    public int deleteAiUserRequireRelationById(Long id);

    /**
     * 批量删除用户需求商机信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiUserRequireRelationByIds(Long[] ids);
}
