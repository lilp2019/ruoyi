package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.AiTopic;

/**
 * 用户会话topic信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-01
 */
public interface AiTopicMapper 
{
    /**
     * 查询用户会话topic信息
     * 
     * @param id 用户会话topic信息主键
     * @return 用户会话topic信息
     */
    public AiTopic selectAiTopicById(String id);

    /**
     * 查询用户会话topic信息列表
     * 
     * @param aiTopic 用户会话topic信息
     * @return 用户会话topic信息集合
     */
    public List<AiTopic> selectAiTopicList(AiTopic aiTopic);

    /**
     * 新增用户会话topic信息
     * 
     * @param aiTopic 用户会话topic信息
     * @return 结果
     */
    public int insertAiTopic(AiTopic aiTopic);

    /**
     * 修改用户会话topic信息
     * 
     * @param aiTopic 用户会话topic信息
     * @return 结果
     */
    public int updateAiTopic(AiTopic aiTopic);

    /**
     * 删除用户会话topic信息
     * 
     * @param id 用户会话topic信息主键
     * @return 结果
     */
    public int deleteAiTopicById(String id);

    /**
     * 批量删除用户会话topic信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAiTopicByIds(String[] ids);
}
