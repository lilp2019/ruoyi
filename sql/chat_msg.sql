ALTER TABLE `ry`.`chat_message`
  ADD COLUMN `msg_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键' FIRST,
  CHANGE `id` `id` VARCHAR(31)  NULL COMMENT '消息ID',
  CHANGE `parent_id` `parent_id` VARCHAR(31)  NULL COMMENT '消息父ID',
  CHANGE `session_id` `session_id` VARCHAR(64)  NULL COMMENT '会话标识',
  CHANGE `topic_id` `topic_id` VARCHAR(31)  NULL COMMENT '主题',
  CHANGE `role` `role` VARCHAR(31)  NULL COMMENT '角色',
  CHANGE `content` `content` TEXT  NULL COMMENT '消息内容',
  CHANGE `model` `model` VARCHAR(31)  NULL COMMENT 'AI模型',
  CHANGE `create_time` `create_time` DATETIME NULL COMMENT '创建时间',
  CHANGE `update_time` `update_time` DATETIME NULL COMMENT '更新时间',
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`msg_id`);
