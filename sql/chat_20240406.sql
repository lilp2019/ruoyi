USE `ry`;

ALTER TABLE `ry`.`chat_message`
  ADD COLUMN `user_id` BIGINT(20) NULL COMMENT '用户ID' AFTER `msg_id`,
  ADD COLUMN `create_by` VARCHAR(64) DEFAULT '' NULL COMMENT '创建者' AFTER `model`,
COMMENT='用户会话信息表';


/*Table structure for table `chat_require` */

DROP TABLE IF EXISTS `chat_require`;

CREATE TABLE `chat_require` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '需求方用户ID',
  `session_id` varchar(64) DEFAULT NULL COMMENT '会话标识ID',
  `topic_id` varchar(31) DEFAULT NULL COMMENT '主题ID',
  `title` varchar(50) DEFAULT NULL COMMENT '需求标题',
  `content` text COMMENT '需求内容',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户需求信息表';

/*Table structure for table `chat_require_plan` */

DROP TABLE IF EXISTS `chat_require_plan`;

CREATE TABLE `chat_require_plan` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `require_id` int(4) NOT NULL COMMENT '需求ID',
  `user_id` bigint(20) NOT NULL COMMENT '方案提供方用户ID',
  `title` varchar(50) NOT NULL COMMENT '方案标题',
  `content` text COMMENT '方案内容',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需求方案信息表';

/*Table structure for table `chat_require_summary` */

DROP TABLE IF EXISTS `chat_require_summary`;

CREATE TABLE `chat_require_summary` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `require_id` int(4) NOT NULL COMMENT '需求ID',
  `user_id` bigint(20) NOT NULL COMMENT '方案提供方用户ID',
  `recommend_type` int(4) NOT NULL DEFAULT '0' COMMENT '推荐类型（1-AI推荐 2-人气最高）',
  `main_image` varchar(255) DEFAULT NULL COMMENT '方案主图URL',
  `content` text COMMENT '方案总结内容',
  `pre_price` decimal(10,0) DEFAULT '0' COMMENT '预估价格',
  `company_name` varchar(64) DEFAULT NULL COMMENT '方案提供方公司名称',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需求总结方案信息表';