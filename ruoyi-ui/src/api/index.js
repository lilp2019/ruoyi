import request from '@/utils/request'

// 获取首页总数统计信息
export function getTotalStatistics() {
  return request({
    url: '/total/statistics',
    method: 'get'
  })
}

// 获取首页7天内每天的统计信息
export function getStatistics() {
  return request({
    url: '/statistics',
    method: 'get'
  })
}
