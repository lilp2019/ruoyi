import request from '@/utils/request'

// 查询案例信息列表
export function listCase(query) {
  return request({
    url: '/system/case/list',
    method: 'get',
    params: query
  })
}

// 查询案例信息详细
export function getCase(id) {
  return request({
    url: '/system/case/' + id,
    method: 'get'
  })
}

// 新增案例信息
export function addCase(data) {
  return request({
    url: '/system/case',
    method: 'post',
    data: data
  })
}

// 修改案例信息
export function updateCase(data) {
  return request({
    url: '/system/case',
    method: 'put',
    data: data
  })
}

// 状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/case',
    method: 'put',
    data: data
  })
}

// 删除案例信息
export function delCase(id) {
  return request({
    url: '/system/case/' + id,
    method: 'delete'
  })
}
