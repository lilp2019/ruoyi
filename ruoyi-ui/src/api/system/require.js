import request from '@/utils/request'

// 查询用户需求信息列表
export function listRequire(query) {
  return request({
    url: '/system/require/list',
    method: 'get',
    params: query
  })
}

// 查询用户需求信息详细
export function getRequire(id) {
  return request({
    url: '/system/require/' + id,
    method: 'get'
  })
}

// 新增用户需求信息
export function addRequire(data) {
  return request({
    url: '/system/require',
    method: 'post',
    data: data
  })
}

// 修改用户需求信息
export function updateRequire(data) {
  return request({
    url: '/system/require',
    method: 'put',
    data: data
  })
}

// 删除用户需求信息
export function delRequire(id) {
  return request({
    url: '/system/require/' + id,
    method: 'delete'
  })
}

// 状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/require',
    method: 'put',
    data: data
  })
}
