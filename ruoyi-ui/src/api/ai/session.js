import request from '@/utils/request'

// 查询用户对话Session信息列表
export function listSession(query) {
  return request({
    url: '/ai/session/list',
    method: 'get',
    params: query
  })
}

// 查询用户对话Session信息详细
export function getSession(id) {
  return request({
    url: '/ai/session/' + id,
    method: 'get'
  })
}

// 新增用户对话Session信息
export function addSession(data) {
  return request({
    url: '/ai/session',
    method: 'post',
    data: data
  })
}

// 修改用户对话Session信息
export function updateSession(data) {
  return request({
    url: '/ai/session',
    method: 'put',
    data: data
  })
}

// 删除用户对话Session信息
export function delSession(id) {
  return request({
    url: '/ai/session/' + id,
    method: 'delete'
  })
}
