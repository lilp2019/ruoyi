import request from '@/utils/request'

// 查询用户会话信息列表
export function listMessage(query) {
  return request({
    url: '/ai/message/list',
    method: 'get',
    params: query
  })
}

// 查询用户会话信息详细
export function getMessage(id) {
  return request({
    url: '/ai/message/' + id,
    method: 'get'
  })
}

// 新增用户会话信息
export function addMessage(data) {
  return request({
    url: '/ai/message',
    method: 'post',
    data: data
  })
}

// 修改用户会话信息
export function updateMessage(data) {
  return request({
    url: '/ai/message',
    method: 'put',
    data: data
  })
}

// 删除用户会话信息
export function delMessage(id) {
  return request({
    url: '/ai/message/' + id,
    method: 'delete'
  })
}
