import request from '@/utils/request'

// 查询用户意见反馈信息列表
export function listFeedback(query) {
  return request({
    url: '/ai/feedback/list',
    method: 'get',
    params: query
  })
}

// 查询用户意见反馈信息详细
export function getFeedback(id) {
  return request({
    url: '/ai/feedback/' + id,
    method: 'get'
  })
}

// 新增用户意见反馈信息
export function addFeedback(data) {
  return request({
    url: '/ai/feedback',
    method: 'post',
    data: data
  })
}

// 修改用户意见反馈信息
export function updateFeedback(data) {
  return request({
    url: '/ai/feedback',
    method: 'put',
    data: data
  })
}

// 删除用户意见反馈信息
export function delFeedback(id) {
  return request({
    url: '/ai/feedback/' + id,
    method: 'delete'
  })
}
