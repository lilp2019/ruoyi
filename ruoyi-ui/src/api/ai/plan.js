import request from '@/utils/request'

// 查询用户商机意向合作信息列表
export function listPlan(query) {
  return request({
    url: '/ai/plan/list',
    method: 'get',
    params: query
  })
}

// 查询用户商机意向合作信息详细
export function getPlan(id) {
  return request({
    url: '/ai/plan/' + id,
    method: 'get'
  })
}

// 新增用户商机意向合作信息
export function addPlan(data) {
  return request({
    url: '/ai/plan',
    method: 'post',
    data: data
  })
}

// 修改用户商机意向合作信息
export function updatePlan(data) {
  return request({
    url: '/ai/plan',
    method: 'put',
    data: data
  })
}

// 删除用户商机意向合作信息
export function delPlan(id) {
  return request({
    url: '/ai/plan/' + id,
    method: 'delete'
  })
}
