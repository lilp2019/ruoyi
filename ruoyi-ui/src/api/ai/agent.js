import request from '@/utils/request'

// 查询用户对话Agent配置信息列表
export function listAgent(query) {
  return request({
    url: '/ai/agent/list',
    method: 'get',
    params: query
  })
}

// 查询用户对话Agent配置信息详细
export function getAgent(id) {
  return request({
    url: '/ai/agent/' + id,
    method: 'get'
  })
}

// 新增用户对话Agent配置信息
export function addAgent(data) {
  return request({
    url: '/ai/agent',
    method: 'post',
    data: data
  })
}

// 修改用户对话Agent配置信息
export function updateAgent(data) {
  return request({
    url: '/ai/agent',
    method: 'put',
    data: data
  })
}

// 删除用户对话Agent配置信息
export function delAgent(id) {
  return request({
    url: '/ai/agent/' + id,
    method: 'delete'
  })
}
