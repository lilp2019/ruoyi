import request from '@/utils/request'

// 查询用户方案信息列表
export function listScheme(query) {
  return request({
    url: '/ai/scheme/list',
    method: 'get',
    params: query
  })
}

// 查询用户方案信息详细
export function getScheme(id) {
  return request({
    url: '/ai/scheme/' + id,
    method: 'get'
  })
}

// 新增用户方案信息
export function addScheme(data) {
  return request({
    url: '/ai/scheme',
    method: 'post',
    data: data
  })
}

// 修改用户方案信息
export function updateScheme(data) {
  return request({
    url: '/ai/scheme',
    method: 'put',
    data: data
  })
}

// 删除用户方案信息
export function delScheme(id) {
  return request({
    url: '/ai/scheme/' + id,
    method: 'delete'
  })
}
