import request from '@/utils/request'

// 查询用户需求商机信息列表
export function listRequire(query) {
  return request({
    url: '/ai/require/list',
    method: 'get',
    params: query
  })
}

// 查询用户需求商机信息详细
export function getRequire(id) {
  return request({
    url: '/ai/require/' + id,
    method: 'get'
  })
}

// 新增用户需求商机信息
export function addRequire(data) {
  return request({
    url: '/ai/require',
    method: 'post',
    data: data
  })
}

// 修改用户需求商机信息
export function updateRequire(data) {
  return request({
    url: '/ai/require',
    method: 'put',
    data: data
  })
}

// 状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/ai/require',
    method: 'put',
    data: data
  })
}

// 删除用户需求商机信息
export function delRequire(id) {
  return request({
    url: '/ai/require/' + id,
    method: 'delete'
  })
}
