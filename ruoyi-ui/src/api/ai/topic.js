import request from '@/utils/request'

// 查询用户会话topic信息列表
export function listTopic(query) {
  return request({
    url: '/ai/topic/list',
    method: 'get',
    params: query
  })
}

// 查询用户会话topic信息详细
export function getTopic(id) {
  return request({
    url: '/ai/topic/' + id,
    method: 'get'
  })
}

// 新增用户会话topic信息
export function addTopic(data) {
  return request({
    url: '/ai/topic',
    method: 'post',
    data: data
  })
}

// 修改用户会话topic信息
export function updateTopic(data) {
  return request({
    url: '/ai/topic',
    method: 'put',
    data: data
  })
}

// 删除用户会话topic信息
export function delTopic(id) {
  return request({
    url: '/ai/topic/' + id,
    method: 'delete'
  })
}
