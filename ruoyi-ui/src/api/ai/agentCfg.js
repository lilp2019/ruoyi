import request from '@/utils/request'

// 查询不同类型对话Agent配置信息列表
export function listAgentCfg(query) {
  return request({
    url: '/ai/agent/cfg/list',
    method: 'get',
    params: query
  })
}

// 查询不同类型对话Agent配置信息详细
export function getAgentCfg(id) {
  return request({
    url: '/ai/agent/cfg/' + id,
    method: 'get'
  })
}

// 新增不同类型对话Agent配置信息
export function addAgentCfg(data) {
  return request({
    url: '/ai/agent/cfg',
    method: 'post',
    data: data
  })
}

// 修改不同类型对话Agent配置信息
export function updateAgentCfg(data) {
  return request({
    url: '/ai/agent/cfg',
    method: 'put',
    data: data
  })
}

// 状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/ai/agent/cfg',
    method: 'put',
    data: data
  })
}

// 删除不同类型对话Agent配置信息
export function delAgentCfg(id) {
  return request({
    url: '/ai/agent/cfg/' + id,
    method: 'delete'
  })
}
